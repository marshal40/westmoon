
#include "stdafx.h"
#include "DataManager.h"
#include "Texture.h"
#include "TextureManager.h"

#include <Windows.h>																// WARNING

template class DataManager<Texture2D, Texture2DManager>;

// Singleton initialization.  At first, there is no object created.						
template <typename T, typename C>
C *DataManager<T, C>::_singleton = NULL;
 
template <typename T, typename C>
inline T *DataManager<T, C>::request (const string &name)
{
	typename DataMap::iterator itor;
	itor = _registry.find (name);


	if (itor != _registry.end ())	// The object has been found
	{

		return itor->second;
	}
	else
	{

		return NULL;
	}

	

}