
#include "stdafx.h"
#include "LiveObject.h"

#include <vector>
#include <utility>

std::string LiveObject::GetAnimation()
{
	return this->character->currentAnim();
}

LiveObject::LiveObject(Md2Player *character, 
		Vector3 position, 
		Vector3 rotation, 
		float linearSpeed, 
		float angularSpeed,
		float armor,
		BoundingBox boundingBox,
		Environment* environment,
		float frameRate,
		float scale)
{
	this->unlim = true;
	this->environment = environment;

	this->character = character;
	this->character->setScale(scale);
	this->position = position;								
	this->position.y += this->environment->Topo(this->position);		// Set the object over the terrain

	this->rotation = rotation;
	this->linearSpeed = linearSpeed;
	this->angularSpeed = angularSpeed;
	this->_templateBB = boundingBox;
	this->UpdateBoundingBox();
		
	this->life = 100.0f;
	this->frameRate = frameRate;

	this->armor = armor;

	this->environment->liveObjectsInfos.push_back(LiveObjectInfo(&this->boundingBox, &this->life, &this->armor));

	this->isTree = false;
	this->isManWithRiffle = false;
	this->doNotAninate = false;
}

void LiveObject::Draw(Camera *camera, bool animated)
{
	if(camera->IsVisible(&this->boundingBox))							//Draw the object if is visible
	{
		glPushMatrix();
		glTranslatef(position.x, position.y, position.z);
		glRotatef(this->rotation.y, 0.0f, 1.0f, 0.0f);
		this->character->drawPlayerItp(true, static_cast<Md2Object::Md2RenderMode>(1));
		glPopMatrix();
	}

	/*
	if(this->isTree)
	{
		fstream file("falledChecker.txt", ios::app);
		file << this->GetAnimation() <<"\n";
		file.close();
	}
	if(this->isManWithRiffle)
	{
		fstream file("diedChecker.txt", ios::app);
		file << this->GetAnimation() <<"\n";
		file.close();
	}
	*/
	if(animated)
		if(this->character->animate (this->frameRate, this->unlim) && !this->unlim)
		{
			this->AnimStoped();
			if(this->isTree && !this->GetAnimation().compare("fall"))
			{
				this->SetAnimation("felt");
			}
			else if(this->isManWithRiffle && !this->GetAnimation().compare("die"))
			{
				fstream file("diedChecker.txt", ios::app);

				file << "reached 1 "<< this->GetAnimation() <<"\n";

				this->SetAnimation("death");
				
				file << "reached 2 "<< this->GetAnimation() <<"\n";
				file.close();
			}
			//this->unlim = true;
		}//Animate the object

	
	//this->boundingBox.Draw();
}

void LiveObject::UpdateBoundingBox()
{
	this->boundingBox = this->_templateBB;
	this->boundingBox.Translate(this->position);
}

bool LiveObject::SetAnimation(char* animation)
{
	//if(this->isTree && !this->GetAnimation().compare("felt"));
	if(this->isManWithRiffle && !this->GetAnimation().compare("death"))
		return false;
	else this->character->setAnim(animation);
	return true;
}

void LiveObject::Heal(float value) 
{
	this->life += value;								// Modify the life value
	if(this->life > 100.0f) this->life = 100.0f;		// Check if the life value is in its bounds
	else if(this->life < 0.0f) this->life = 0.0f;
}

void LiveObject::SetPosition(Vector3 position) 
{ 
	this->position = position; 
	this->UpdateBoundingBox();
}

void LiveObject::Move(Vector3 newPosition)
{
	float topo = this->environment->Topo(newPosition);
	if(topo != Environment::UNDEFINED)
	{
		if(topo -this->position.y < 5.0f && topo > 0.0f)
		{
			Vector3 lastPosition = this->position;								// Save the object's last position
			BoundingBox lastBB = this->boundingBox;
			this->SetPosition(Vector3(newPosition.x, topo, newPosition.z));

			if(!this->environment->isLegal(this->boundingBox))					// If the new position is illegal in the environment
			{
				this->position = lastPosition;									// return the last position 
				this->boundingBox = lastBB;
			}
		}
	}
	else this->SetPosition(newPosition);
}

void LiveObject::TurnLeft(float angle)
{
	this->rotation.y += angle;
	if(this->rotation.y > 360) this->rotation.y -= 360;
}
void LiveObject::TurnRight(float angle)
{
	this->rotation.y -= angle;
	if(this->rotation.y < 0) this->rotation.y += 360;
}
	
void LiveObject::MoveForward()
{
	VectorMath::Vector3 translation(0.0f, 0.0f, this->linearSpeed);		// build velocity vector
	translation.RotateAroundYAxis(this->rotation.y);					// direct the velocity vector forward
	this->Move(this->position + translation);							// apply velocity
}
void LiveObject::MoveLeft()
{
	VectorMath::Vector3 translation(this->linearSpeed/2, 0.0f, 0.0f);	// build velocity vector
	translation.RotateAroundYAxis(this->rotation.y);					// direct the velocity vector left
	this->Move(this->position + translation);							// apply velocity
}
void LiveObject::MoveRight()
{
	VectorMath::Vector3 translation(-this->linearSpeed/2, 0.0f, 0.0f);  // build velocity vector
	translation.RotateAroundYAxis(this->rotation.y);					// direct the velocity vector right
	this->Move(this->position + translation);							// apply velocity
}
void LiveObject::MoveBack()
{
	VectorMath::Vector3 translation(0.0f, 0.0f, -this->linearSpeed/2);  // build velocity vector
	translation.RotateAroundYAxis(this->rotation.y);					// direct the velocity vector backward
	this->Move(this->position + translation);							// apply velocity
}