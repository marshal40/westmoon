// Environment.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "Environment.h"
#include <string>
#include "../reCoIl/reCoIl.h"

const float Environment::UNDEFINED = -12345.0f;

Environment::Environment() 
{
	this->showBBs = false;
	this->mosquito = NULL;
}

Environment::~Environment()
{
	delete mosquito;
}

void Environment::PushBack(Terrain *terrain) 
{ 
	if(!this->terrains.PushElement(terrain))
		ThrowError(TEXT("Defined more terrains than declared."));			// Chack for out of array bounds
}

void Environment::LoadEnvironment(char* filename)
{
	fstream file;
	file.open(filename, ios::in);
	if(!file.is_open())		// Throw error if the file coulf not be opened
	{
		ThrowError(TEXT("Cannnot open evironment descibrion file."));
		exit(0);
	}
	else					// if the file is opened successfully
	{
		char operatorE[256];

		Texture2DManager *texMgr;
		texMgr = Texture2DManager::getInstance();

		if(reCoIl::GoToBlock(&file, "ENVIRONMENT"))//if(reCoIl::FindOperator(&file, "BEGIN") && reCoIl::IsNextOperator(&file, "ENVIRONMENT"))
		{
			while(file.good())
			{
				strcpy(operatorE, reCoIl::GetNextOperator(&file));
				//file >> operatorE;									// get the current operator
				if(!strcmp(operatorE, "TERRAIN_NUM"))
				{
					unsigned terrainNum;
					file >> terrainNum;
					this->ReserveTerrains(terrainNum);
				}
				else if(!strcmp(operatorE, "TERRAIN"))
				{
					char terrainMeshFilename[256];
					unsigned smoothLevel;
					int mapX, mapY, precision;
					Vector3 position, scale;
					bool compress;

					file >> terrainMeshFilename;
					file >> smoothLevel;
					file >> mapX >> mapY >> precision;
					file >> position.x >> position.y >> position.z;
					file >> scale.x >> scale.y >> scale.z;
					file >> compress;
					this->PushBack(new Terrain(this->terrains.GetNumElements(), terrainMeshFilename, smoothLevel, mapX, mapY, precision, position, scale, compress));
				}
				else if(!strcmp(operatorE, "COMPILE_TERRAINS"))
				{
					this->Compile();
				}
				else if(!strcmp(operatorE, "SUN"))
				{
					char sunTextureFilename[256];
					Vector3 position;
					float size;
					Vector3 rotationAxis;
					float rotationSpeed_degs;
					file >> sunTextureFilename;
					file >> position.x >> position.y >> position.z;
					file >> size;
					file >> rotationAxis.x >> rotationAxis.y >> rotationAxis.z >> rotationSpeed_degs;
					this->SetSun(new Sun(texMgr->load(sunTextureFilename), position, size, rotationAxis, rotationSpeed_degs));
				}
				else if(!strcmp(operatorE, "MOSQUITO"))
				{
					char soundFilename[256];
					file >> soundFilename;
					WCHAR wFilename[256];
					MultiByteToWideChar(0, 0, soundFilename, 256, wFilename, 256);
					this->SetMosquito(new Mosquito(wFilename));
				}
				else if(!strcmp(operatorE, "PLANT_MAPS_NUM"))
				{
					unsigned plantsNum;
					file >> plantsNum;
					this->plants.DeclareElements(plantsNum);
				}
				else if(!strcmp(operatorE, "PLANT_MAP"))
				{
					char plantMapFile[256];
					file >> plantMapFile;
					this->LoadPlantMap(plantMapFile);
				}
				else if(!strcmp(operatorE, "BIRDS_NUM"))
				{
					unsigned birdsNum;
					file >> birdsNum;
					this->birds.DeclareElements(birdsNum);
				}
				else if(!strcmp(operatorE, "BIRD"))
				{
					char birdFilename[256];
					file >> birdFilename;
					this->LoadBird(birdFilename);
				}
				else if(!strcmp(operatorE, "OBJECT"))
				{
					this->plants.PushElement(new PlantMap(&file, &this->bodies));
				}
				else if(!strcmp(operatorE, "END"))
				{
					file.close();
					return;
				}
			}
		}
		else
		{
			ThrowError(L"reCoIl: Invalid ENVIRONMENT block.");
			return;
		}
	}
}

void Environment::LoadPlantMap(char* filename) 
{ 
	if(this->plants.PushElement(new PlantMap(filename, &this->bodies))) //++this->lastPlantIndex < this->plantsNum)					// Check for the array bounds
	{
		unsigned plantMapIndex = this->plants.GetNumElements()-1;
		unsigned plantsNum = this->plants[plantMapIndex]->GetPlantsNum();
		for(unsigned i = 0; i < plantsNum; i++)
		{
			this->plants[plantMapIndex]->SetPlantHeight(i, this->Topo(this->plants[plantMapIndex]->GetPlantPosition(i)));
		}
	}
	else
	{
		ThrowError(TEXT("Defined more plant maps than declared!"));
	}
}

void Environment::LoadBird(char* filename)
{
	if(!this->birds.PushElement(new Bird(filename, &this->liveObjectsInfos)))
		ThrowError(TEXT("Defined more terrains than declared."));				// chech for out of array bounds
}

void Environment::SetSun(Sun *sun) { this->sky.SetSun(sun); }
Sun* Environment::GetSun() { return this->sky.GetSun(); }
void Environment::SetMosquito(Mosquito *mosquito) { this->mosquito = mosquito; }

void Environment::Update(Mouse *mouse, Keyboard *keyboard)
{
	this->eventsMask = 0;						// Clear the eventsMask
	
	if(keyboard->GetButtonState('B'))
		this->showBBs = !this->showBBs;


	// Update birds
	for(int i = 0; i < this->birds.GetNumElements(); i++)
		if(this->birds[i]->IsAlive())
			this->birds[i]->Update();
		else
		{
			this->eventsMask |= this->BIRD_IS_DEAD;
			this->birds.EraseElement(i);
			i--;
			if(i+1 == this->birds.GetNumElements()) 
				break;
		}
	
	// Update mosquito
	this->mosquito->Update();

	// Update sky objects
	this->sky.Update();
}

void Environment::Compile()
{
	//Compile the terrains
	for(unsigned i = 0; i < this->terrains.GetNumElements(); i++)
		this->terrains[i]->Compile();
}

void Environment::Draw(Camera *camera) 
{
	glEnable(GL_TEXTURE_2D);
	//Draw terrains
	for(unsigned i = 0; i < this->terrains.GetNumElements(); i++)
		this->terrains[i]->Draw(camera);

	//Draw plants
	for(unsigned i = 0; i < this->plants.GetNumElements(); i++)
		this->plants[i]->Draw(camera);

	//Draw birds
	for(unsigned i = 0; i < this->birds.GetNumElements(); i++)
		this->birds[i]->Draw(camera);

	if(this->showBBs)
	{
		for(unsigned i = 0; i < this->bodies.size(); i++)
			this->bodies[i]->Draw();
	}
}

void Environment::DrawSky(Camera *camera)
{
	glEnable(GL_TEXTURE_2D);
	sky.Draw(camera);
}

void Environment::ReserveTerrains(int numTerrains)			// TODO: do not use this function
{ 
	this->terrains.DeclareElements(numTerrains);
}

float Environment::Topo(Vector3 point)								// Get the highest point of the environment at specific point
{
	for(unsigned i = 0; i < this->terrains.GetNumElements(); i++)
		if(terrains[i]->IsPointInside(point))						// Find the terrain which contains the point
			return terrains[i]->Topo(point.x, point.z);				// Get the altitude of the point in the terrain
		
	return Environment::UNDEFINED;									// if the point is outside this environment return undefined value
}

bool Environment::isLegal(BoundingBox box)
{
	for(unsigned i = 0; i < this->bodies.size(); i++)
		if(box.isColliding(this->bodies[i])) 
			return false;

	for(unsigned i = 0; i< this->liveObjectsInfos.size(); i++)
		if(box.isColliding(this->liveObjectsInfos[i].bb) && !box.IsEqual(this->liveObjectsInfos[i].bb))
			return false;

	return true;
}

void Environment::Damage(Vector3 point, float damage)
{
	for(unsigned i = 0; i < this->liveObjectsInfos.size(); i++)
		if(this->liveObjectsInfos[i].bb->IsContaining(point))								// Get the object which contains the damaged point
			*this->liveObjectsInfos[i].life -= damage - *this->liveObjectsInfos[i].armor;	// Damage the object
}

bool Environment::CheckEvent(Events _event)							// If event is happened
{
	return (this->eventsMask & _event) == _event;
}