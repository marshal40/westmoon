
#include "stdafx.h"
#include "Bird.h"
#include "ThrowError.h"

Bird::Bird(char* filename, vector<LiveObjectInfo>* liveObjectsInfos) 
{
	this->Load(filename, liveObjectsInfos);
}

void Bird::Load(char* filename, vector<LiveObjectInfo>* liveObjectsInfos)			// Load the bird info from file
{
	fstream file;
	file.open(filename, ios::in);
	if(file.is_open())							
	{
		char meshFilename[256], textureFilename[256];
		file.getline(meshFilename, 256);
		file.getline(textureFilename, 256);
		this->LoadModel(meshFilename, textureFilename);
		
		// Read the scale and the frame rate
		float scale;
		file >> scale;
		this->model->setScale(scale);

		file >> this->frameRate;

		// Read the information describing the flying area
		file >> this->center.x >> this->center.y >> this->center.z;
		file >> this->radius; 
		
		// Read invormation about the dynamics of the bird
		float velocityF;
		file >> velocityF;
		this->velocity = Vector3(0.0f, 0.0f, velocityF);
		file >> this->maxAngle;

		// Read bounding box
		file >> this->_templateBB.center.x >> this->_templateBB.center.y >> this->_templateBB.center.z;
		file >> this->_templateBB.dimensions.x >> this->_templateBB.dimensions.y >> this->_templateBB.dimensions.z;

		this->position = this->center;
		this->UpdateBB();
		this->isInside = true;
		this->lastIsInside = true;
		this->rotationY = 0.0f;

		this->life = 100.0f;
		this->armor = 0.0f;

		// Register the bounding box
		if(liveObjectsInfos != NULL && this->_templateBB.IsReal())
			liveObjectsInfos->push_back(LiveObjectInfo(&this->bb, &this->life, &this->armor));
	}
	else			// if the file does not exist throw error
	{
		ThrowError(TEXT("Cannot open file to initialize Bird"));
	}
	file.close();
}

void Bird::UpdateBB()																	// Update the bounding box
{
	this->bb = this->_templateBB;
	this->bb.Translate(this->position);
}

void Bird::LoadModel(char* meshFilename, char* textureFilename)							// Load the model info from file
{
	this->model = new Md2Player(meshFilename, textureFilename); 
	this->model->setAnim("frame");
}

bool Bird::IsAlive()
{
	return (this->life > 0.0f);
}

void Bird::Draw(Camera *camera)
{
	glPushMatrix();
	glTranslatef(this->position.x, this->position.y, this->position.z);					// Translate to bird's position
	glRotatef(this->rotationY, 0.0f, 1.0f, 0.0f);
	this->model->drawPlayerItp(true, static_cast<Md2Object::Md2RenderMode>(1));			// Render the model
	this->model->animate(this->frameRate, true);										// Animate the model
	glPopMatrix();

	//this->bb.Draw();
} 

void Bird::Update()
{
	this->lastIsInside = this->isInside;
	if((this->position - this->center).GetSqLength() < this->radius*this->radius)				// If the bird is inside the allowed area
	{
		this->isInside = true;
		if(rand()%100 == 0) this->Rotate(((rand()%2)*2 - 1)*(this->maxAngle));			// if the bird "decided" to turn
	}		
	else																						// If the bird is outside the allowed area
	{
		this->isInside = false;
		float distance = DistanceFromPointToLineInXZ(this->position + this->velocity, this->position, this->center); 

		// if when the bird goes straight it goes in the legal area
		if(distance >= radius || Vector3::DotProduct(this->velocity, this->center - this->position) < 0)	
		{
			if(this->lastIsInside)
				this->angle = ((rand()%2)*2 - 1)*(this->maxAngle);
			this->Rotate(angle);
		}
		else
		{
			if(rand()%100 == 0) this->Rotate(((rand()%2)*2 - 1)*(this->maxAngle));		// if the bird "decided" to turn
		}
	}

	this->MoveForward();	
}

void Bird::MoveForward()
{
	this->position += this->velocity;
	this->UpdateBB();
}

void Bird::Rotate(float angle)
{
	this->rotationY += angle;
	this->velocity.RotateAroundYAxis(angle);
}