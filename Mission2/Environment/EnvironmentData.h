
#ifndef EVIRONMENTDATA_H
#define EVIRONMENTDATA_H

#ifdef ENVIRONMENT_EXPORTS
#define ENVIRONMENT_API __declspec(dllexport)
#else
#define ENVIRONMENT_API __declspec(dllimport)
#endif

#include "../Md2Model/Texture.h"

struct ENVIRONMENT_API EnvironmentData			// same data used from Environment class
{
	static Texture2D* textureWater;
	static Texture2D* textureGrass;
	static Texture2D* textureSoil;
	static Texture2D* textureRock;
};

#endif