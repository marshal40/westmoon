
#ifndef TS_SUN_H
#define TS_SUN_H

#ifdef ENVIRONMENT_EXPORTS
#define ENVIRONMENT_API __declspec(dllexport)
#else
#define ENVIRONMENT_API __declspec(dllimport)
#endif

#include "SkyObject.h"
#include "../Geometry/Geometry.h"
#include "../Camera/Camera.h"
#include "../Md2Model/Texture.h"
using namespace VectorMath;

/* This class describes a sun. It is a sky object.
 * Sun's main property is to move around the world
 * and simulate day and night, dusk and twilight
 */
class ENVIRONMENT_API Sun : public SkyObject
{
private:	
	// Rotation around the globe
	Vector3 rotationAxis;	
	float rotatingSpeed_degs;
	float rotation_degs;				
public:
	Sun(Texture2D *texture, Vector3 position, float size = 50.0f, 
		Vector3 rotationAxis = Vector3(-1.0f, 0.0f, 0.0f),
		float rotationSpeed_degs = 0.2f);
	
	Vector3 GetPosition();
	float GetRotationAroundGlobe();
	void SetRotation(float angle);

	void Draw(Camera *camera);
	void Update();
};

#endif