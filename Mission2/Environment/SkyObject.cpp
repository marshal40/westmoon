
#include "stdafx.h"
#include "SkyObject.h"


SkyObject::SkyObject(Texture2D *texture, Vector3 position, float size)
{
	this->texture = texture;
	this->position = position;
	this->size = size;
	this->CalculateRotation();
}

void SkyObject::SetPosition(Vector3 position) 
{ 
	this->position = position; 
	this->CalculateRotation();
}

void SkyObject::CalculateRotation()
{
	Vector3 v = (this->position*(-1)).Normal();						// center of the scene-sun vector
	Vector3 fwdVector = Vector3(0.0f, 0.0f, 1.0f);					// forward vector of the sun when it is not rotated
	this->rotationV = CrossProduct(fwdVector, v).Normal();			// the rotation axis is the normal of the plane where lies v and fwdVector
	float angleCosine = Vector3::DotProduct(fwdVector, v);			// get the cosine of the angle of rotation
	this->angle = acos(Vector3::DotProduct(fwdVector, v))*180/PI;	// get the angle of rotation
}

void SkyObject::Draw(Camera *camera)
{
	glPushMatrix();	
	Vector3 cameraPosition = camera->GetPosition();

	// Draw the object at specific distansce from the camera
	glTranslatef(cameraPosition.x + this->position.x, 
		cameraPosition.y + this->position.y, 
		cameraPosition.z + this->position.z);
		
	// Rotate the sky object so it looks at the camera
	glRotatef(this->angle, this->rotationV.x, this->rotationV.y, this->rotationV.z);	

	// draw the sky object
	this->texture->bind();
	glColor3f(1.0f, 1.0f, 1.0f);
	glDisable(GL_LIGHTING);
	glScalef(this->size, this->size, this->size);
		
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.0f); glVertex3f( 1.0f,  -1.0f, 0.0f);
	glTexCoord2f(1.0f, 1.0f); glVertex3f( 1.0f,  1.0f, 0.0f);
	glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f,  1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f, -1.0f, 0.0f);		
	glEnd();

	glEnable(GL_LIGHTING);
	glPopMatrix();
}

Vector3 SkyObject::GetPosition()
{
	return this->position;
}
