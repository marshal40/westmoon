#ifndef TS_BOUNDINGBOX_H
#define TS_BOUNDINGBOX_H

#ifdef ENVIRONMENT_EXPORTS
#define ENVIRONMENT_API __declspec(dllexport)
#else
#define ENVIRONMENT_API __declspec(dllimport)
#endif

#include "../Geometry/Geometry.h"
#include <gl/glew.h>
using namespace VectorMath;

/* Box represented by its center and dimensions.
 * Bounsing boxes are used mostly for collision detection
 */
struct ENVIRONMENT_API BoundingBox
{
	Vector3 center;
	Vector3 dimensions;
	
	BoundingBox(Vector3 center = Vector3(), Vector3 dimensions = Vector3());	
	void Set(Vector3 center = Vector3(), Vector3 dimensions = Vector3());		
	bool isColliding(BoundingBox* box);											// if this box and other box are colliding
	void Draw();																// Draw the box. This functions is used mostly for debug and testing
	void Translate(Vector3 translation);										// Transletes the box
	bool IsContaining(Vector3 point);											// If point is inside the box
	bool IsReal();																// If the box has positive dimensions
	bool IsEqual(BoundingBox* box);
};

#endif