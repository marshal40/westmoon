
#include "stdafx.h"
#include "Terrain.h"
#include "EnvironmentData.h"
#include "ThrowError.h"

Terrain::Terrain(int _ID, char* fileName, unsigned smoothLevel,
		int mapX, int mapZ, int precision,
		Vector3 position, 
		Vector3 scale,
		bool compress)
{
	this->_ID = _ID;
	
	this->smoothLevel = smoothLevel;

	this->mapX = mapX;
	this->mapZ = mapZ;
	this->precision = precision;
	this->position = position;

	std::fstream file;
	file.open(fileName, std::ios::in);
	if(!file.good())
	{
		ThrowError(TEXT("Cannot load terrain mesh. Invalid filename."));
		exit(1);
	}
	int _mapX, _mapZ;
	file >> _mapX >> _mapZ;	

	if(_mapX < this->mapX || _mapX%this->mapX ||
		_mapZ < this->mapZ || _mapZ%this->mapZ)
	{
		ThrowError(TEXT("The real dimensions and the needed dimensions of terrain mesh are not in the correct format!"));
		exit(1);
	}
	
	int** _map;

	_map = new int* [_mapX];
	for(int i = 0; i < _mapX; i++)
		_map[i] = new int[_mapZ];

	for(int i = 0; i < _mapX; i++)
		for(int j = 0; j < _mapZ; j++)
			file >> _map[i][j];

	file.close();


	// Compress the vertexheights map
	this->vertexHeights = new int*[this->mapX];
	for(int i = 0; i < this->mapX; i++)
		this->vertexHeights[i] = new int[this->mapZ];

	if(compress)
	{
		int compressX = _mapX / this->mapX;
		int compressZ = _mapZ / this->mapZ;
		int sum;

		for(int s, t, j, i = 0; i < _mapX; i += compressX)
			for(j = 0; j < _mapZ; j += compressZ)
			{
				sum  = 0;
				for(t = i; t < i + compressX; t++)
					for(s = j; s < j + compressZ; s++)
						sum += _map[i][j];
				this->vertexHeights[i/compressX][j/compressZ] = sum / (compressX * compressZ);
			}
	}
	else
	{
		for(int i = 0; i < _mapX; i++)
			for(int j = 0; j < _mapZ; j++)
				this->vertexHeights[i][j] = _map[i][j];
	}
	// ---------------------------------------------

	this->vertexCount = this->mapX * this->mapZ;
	this->triangleCount = (this->mapX-1) * (this->mapZ - 1) * 2;			

	// Create vertices array
	this->vertices = new Vector3*[this->mapX];
	for(int i = 0; i < this->mapX; i++)
		this->vertices[i] = new Vector3[this->mapZ];

	// Create the triangles array
	this->triangles = new Triangle**[this->mapX-1];
	for(int i = 0; i < this->mapX-1; i++)
	{
		this->triangles[i] = new Triangle*[this->mapZ-1];
		for(int j = 0; j < this->mapZ-1; j++)
			this->triangles[i][j] = new Triangle[2];
	}

	//Initialize, scale and translate the vertices
	for(int i = 0; i < this->mapX; i++)
		for(int j = 0; j < this->mapZ; j++)
		{
			this->vertices[i][j] =  Vector3((this->position.x + i*this->precision)*scale.x,
													(this->position.y + this->vertexHeights[i][j])*scale.y,
													(this->position.z + j*this->precision)*scale.z);
		}
	
	//Set triangles
	for(int triIndex = 0, i = 0; i < this->mapX - 1; i++)
		for(int j = 0; j < this->mapZ - 1; j++)
		{
			this->triangles[i][j][0].Set(i+1, j,   i,   j,   i,   j+1);
			this->triangles[i][j][1].Set(i,   j+1, i+1, j+1, i+1, j);
		}
	
	this->ManageWithFaceNormals();
	this->CalculateWaterPools();
}

void Terrain::PrintFaceNormalsToFile()
{
	char memFile[256];
	itoa(this->_ID, memFile, 10);
	strcat(memFile, "_terrainSmoothNormalsInfo");
	fstream file(memFile, ios::out | ios::binary);
	file.write((char*)&this->mapX, sizeof(this->mapX));
	file.write((char*)&this->mapZ, sizeof(this->mapZ));
	file.write((char*)&this->smoothLevel, sizeof(this->smoothLevel));
	for(int j, i = 0; i < this->mapX; i++)
		for(j = 0; j < this->mapZ; j++)
			file.write((char*)&this->vertices[i][j], sizeof(this->vertices[i][j]));
	
	for(int k, j, i = 0; i < this->mapX-1; i++)
				for(j = 0; j < this->mapZ-1; j++)
					for(k = 0; k < 2; k++)
						file.write((char*)&this->smoothNormals[i][j][k], sizeof(this->smoothNormals[i][j][k]));
	file.close();
}

void Terrain::CalculateFaceNormals()
{	
	Vector3 *a, *b, *c;

	for(int k, j, i = 0; i < this->mapX-1; i++)
		for(j = 0; j < this->mapZ-1; j++)
			for(k = 0; k < 2; k++)
			{
				a = &this->vertices[this->triangles[i][j][k].vertices[0][0]][this->triangles[i][j][k].vertices[0][1]];
				b = &this->vertices[this->triangles[i][j][k].vertices[1][0]][this->triangles[i][j][k].vertices[1][1]];
				c = &this->vertices[this->triangles[i][j][k].vertices[2][0]][this->triangles[i][j][k].vertices[2][1]];
				this->normals[i][j][k] = CrossProduct((*c-*b), (*a-*b));
				this->normals[i][j][k].Normalize();
			}

	if(this->smoothLevel == 0)
	{
		for(int k, j, i = 0; i < this->mapX-1; i++)
			for(j = 0; j < this->mapZ-1; j++)
				for(k = 0; k < 2; k++)
					this->smoothNormals[i][j][k] = this->normals[i][j][k];
	}
	else
	{
		// Caclulate the smooth normals as a avarage of the normals of the incident faces
		for(int currSmoothLevel = 0; currSmoothLevel < this->smoothLevel; currSmoothLevel++)
		{
			for(int k, j, i = 0; i < this->mapX-1; i++)
				for(j = 0; j < this->mapZ-1; j++)
					for(k = 0; k < 2; k++)
					{
						int incidentFaces = 1;
						this->smoothNormals[i][j][k] = this->normals[i][j][k];

						// Set the current normal to the sum of the incident faces of the mesh. 
						if(k == 1)
						{
							this->smoothNormals[i][j][k] += this->normals[i][j][0];
							incidentFaces++;
							if(i+1 < this->mapX-1)
							{
								this->smoothNormals[i][j][k] += this->normals[i+1][j][0];
								incidentFaces++;
							}
							if(j+1 < this->mapZ-1)
							{
								this->smoothNormals[i][j][k] += this->normals[i][j+1][0];
								incidentFaces++;
							}
						}
						else
						{
							this->smoothNormals[i][j][k] += this->normals[i][j][1];
							incidentFaces++;
							if(i-1 >= 0)
							{
								this->smoothNormals[i][j][k] += this->normals[i-1][j][1];
								incidentFaces++;
							}
							if(j-1 >= 0)
							{
								this->smoothNormals[i][j][k] += this->normals[i][j-1][1];
								incidentFaces++;
							}
						}
						this->smoothNormals[i][j][k] = this->smoothNormals[i][j][k] / incidentFaces;		// Calculate the average
					}
	
			// Override normals array with the current smoothed normals
			for(int k, j, i = 0; i < this->mapX-1; i++)
				for(j = 0; j < this->mapZ-1; j++)
					for(k = 0; k < 2; k++)
						this->normals[i][j][k] = this->smoothNormals[i][j][k];
		}
	}

	this->PrintFaceNormalsToFile();
}

void Terrain::ManageWithFaceNormals()
{
	// Create normals and smoothNormals arrays
	this->normals = new Vector3**[this->mapX-1];
	this->smoothNormals = new Vector3**[this->mapX-1];
	for(int j, i = 0; i < this->mapX-1; i++)
	{
		this->normals[i] = new Vector3*[this->mapZ-1];
		this->smoothNormals[i] = new Vector3*[this->mapZ-1];
		for(int j = 0; j < this->mapZ-1; j++)
		{
			this->normals[i][j] = new Vector3[2];
			this->smoothNormals[i][j] = new Vector3[2];
		}
	}

	// check if the normals for this terrain are already calculated
	char memFile[256];
	itoa(this->_ID, memFile, 10);
	strcat(memFile, "_terrainSmoothNormalsInfo");
	fstream file(memFile, ios::in | ios::binary);
	if(file.is_open())
	{
		int _mapX, _mapZ;
		unsigned _smoothLevel;
		file.read((char*)&_mapX, sizeof(_mapX));
		file.read((char*)&_mapZ, sizeof(_mapZ));
		file.read((char*)&_smoothLevel, sizeof(_smoothLevel));
		if(this->mapX != _mapX || this->mapZ != _mapZ || this->smoothLevel != _smoothLevel)
		{
			file.close();
			this->CalculateFaceNormals();
			return;
		}

		Vector3 currVertex;
		for(int j, i = 0; i < _mapX; i++)
			for(j = 0; j < _mapZ; j++)
			{
				file.read((char*)&currVertex, sizeof(currVertex));
				if(currVertex.x != this->vertices[i][j].x || 
					currVertex.y != this->vertices[i][j].y || 
					currVertex.z != this->vertices[i][j].z)
				{
					file.close();
					this->CalculateFaceNormals();
					return;
				}
			}
		
		// this is the right terrain so read the terrain face normals
		for(int k, j, i = 0; i < this->mapX-1; i++)
			for(j = 0; j < this->mapZ-1; j++)
				for(k = 0; k < 2; k++)
					file.read((char*)&this->smoothNormals[i][j][k], sizeof(this->smoothNormals[i][j][k]));

	}
	else
	{
		file.close();
		this->CalculateFaceNormals();
		return;
	}
}

inline void Terrain::GetPool(int i, int j, bool** visited, float* maxX, float*maxZ, float* minX, float* minZ)			// DFS
{
	visited[i][j] = true;
	
	if(this->vertices[i][j].x < *minX) *minX = this->vertices[i][j].x;
	if(this->vertices[i][j].x > *maxX) *maxX = this->vertices[i][j].x;
	if(this->vertices[i][j].z < *minZ) *minZ = this->vertices[i][j].z;
	if(this->vertices[i][j].z > *maxZ) *maxZ = this->vertices[i][j].z;
	
	if(i + 1 < this->mapX &&															// right vertex
		!visited[i+1][j] && 
		this->vertices[i+1][j].y < 0.0f) 
		this->GetPool(i+1, j, visited, maxX, maxZ, minX, minZ);
	if(i - 1 >= 0.0f &&																	// left vertex
		!visited[i-1][j] && 
		this->vertices[i-1][j].y < 0.0f) 
		this->GetPool(i-1, j, visited, maxX, maxZ, minX, minZ);
	if(j + 1 < this->mapZ &&															// up vertex
		!visited[i][j+1] && 
		this->vertices[i][j+1].y < 0.0f) 
		this->GetPool(i, j+1, visited, maxX, maxZ, minX, minZ);
	if(j - 1 >= 0 &&																	// down vertex
		!visited[i][j-1] && 
		this->vertices[i][j-1].y < 0.0f) 
		this->GetPool(i, j-1, visited, maxX, maxZ, minX, minZ);
}

void Terrain::CalculateWaterPools()
{
	// Calculates water pools using DFS algorithm
	bool** visited = new bool*[this->mapX];
	for(int i = 0; i < this->mapX; i++)
	{
		visited[i] = new bool[this->mapZ];
		memset(visited[i], false, sizeof(bool)*this->mapZ);
	}

	for(int j, i = 0; i < this->mapX; i++)
		for(j = 0; j < this->mapZ; j++)
		{
			if(visited[i][j] || this->vertices[i][j].y >= 0.0f)
			{
				visited[i][j] = true;
				continue;
			}
			else
			{
				float maxX = this->position.x - 1, 
					maxZ = this->position.z - 1, 
					minX = this->position.x + this->mapX*this->precision, 
					minZ = this->position.z + this->mapZ*this->precision;
				this->GetPool(i, j, visited, &maxX, &maxZ, &minX, &minZ);
				this->waterPools.push_back(Water(Vector3((float)(minX - this->precision), 0.0f, (float)(minZ - this->precision)), 
												 Vector3((float)(maxX + this->precision), 0.0f, (float)(maxZ + this->precision))));
			}
		}
}


void Terrain::Compile()
{
	//Compile the displayList
	this->displayList = glGenLists(1);
	glNewList(this->displayList, GL_COMPILE);
		this->Render();
	glEndList();
}

Terrain::~Terrain()
{
	glDeleteLists(this->displayList, 1);
}

void Terrain::Render()
{
	glEnable(GL_TEXTURE_2D);

	
	EnvironmentData::textureGrass->bind();
	//glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_TRIANGLES);


	Vector3 *a, *b, *c;	// triangle vertices

	for(int k, j, i = 0; i < this->mapX-1; i++)
		for(j = 0; j < this->mapZ-1; j++)
			for(k = 0; k < 2; k++)
			{
				a = &this->vertices[this->triangles[i][j][k].vertices[0][0]][this->triangles[i][j][k].vertices[0][1]];
				b = &this->vertices[this->triangles[i][j][k].vertices[1][0]][this->triangles[i][j][k].vertices[1][1]];
				c = &this->vertices[this->triangles[i][j][k].vertices[2][0]][this->triangles[i][j][k].vertices[2][1]];
			
				if(a->y < 0.0f && b->y < 0.0f && c->y < 0.0f)									// do not draw the field if it is under the sea level
					continue;

				if(a->y < 0.0f) a->y = -5.0f;
				else if(a->y - 0.1f < 0.0f) a->y += 5.0f;

				if(b->y < 0.0f) b->y = -5.0f;
				else if(b->y - 0.1f < 0.0f) b->y += 5.0f;
		
				if(c->y < 0.0f) c->y = -5.0f;
				else if(c->y - 0.1f < 0.0f) c->y += 5.0f;

				glNormal3f(this->smoothNormals[i][j][k].x, this->smoothNormals[i][j][k].y, this->smoothNormals[i][j][k].z);

				// Draw the current triangle
				glTexCoord2f(0.0f, 0.0f); glVertex3f(a->x, a->y, a->z);
				glTexCoord2f(1.0f, 0.0f); glVertex3f(b->x, b->y, b->z);
				glTexCoord2f(0.0f, 1.0f); glVertex3f(c->x, c->y, c->z);
			}

	glEnd();

	//Draw water pools
	for(unsigned i = 0; i < this->waterPools.size(); i++)
		this->waterPools[i].Render();	
}

void Terrain::Draw(Camera *camera)
{
	// Do not draw the terrain when it is behind the camera
	if(camera->IsBehind(this->position) &&																					
		camera->IsBehind(this->position + Vector3(this->precision*this->mapX, 0.0f, 0.0f)) &&		
		camera->IsBehind(this->position + Vector3(this->precision*this->mapX, 0.0f, this->precision*this->mapZ)) &&
		camera->IsBehind(this->position + Vector3(0.0f, 0.0f, this->precision*this->mapZ)))
		return;
	else
	{
		glCallList(this->displayList);
	}
}


float Terrain::Topo(float x, float z)
{
	Vector3 A, B, C;					// triangle vertices

	// Calculate the index of the triangle which contains the point
	int i = (int)((x - this->position.x)/this->precision);
	int j = (int)((z - this->position.z)/this->precision);
	
	if(i < 0 || i >= this->mapX -1 || j < 0 || j >= this->mapZ-1)		// throw error if the requested point is outside the terrain
		ThrowError(TEXT("Point passed to topo is out of the game field!"));
	
	int half = 0;

	A = this->vertices[this->triangles[i][j][half].vertices[0][0]][this->triangles[i][j][half].vertices[0][1]];
	B = this->vertices[this->triangles[i][j][half].vertices[1][0]][this->triangles[i][j][half].vertices[1][1]];
	C = this->vertices[this->triangles[i][j][half].vertices[2][0]][this->triangles[i][j][half].vertices[2][1]];

	if(abs(x - B.x) + abs(z - B.z) > this->precision)
	{
		// if so replace A, B, C
		half = 1;
		A = this->vertices[this->triangles[i][j][half].vertices[0][0]][this->triangles[i][j][half].vertices[0][1]];
		B = this->vertices[this->triangles[i][j][half].vertices[1][0]][this->triangles[i][j][half].vertices[1][1]];
		C = this->vertices[this->triangles[i][j][half].vertices[2][0]][this->triangles[i][j][half].vertices[2][1]];
	}

	float a = (abs(x - B.x))/this->precision;
	float b = (abs(z - B.z))/this->precision;
	
	Vector3 resultV = B + (A - B)*a + (C - B)*b;
	return resultV.y;
}



bool Terrain::IsPointInside(VectorMath::Vector3 point)				// If the point is inside the terrain
{
	return (this->position.x < point.x) && (point.x < this->position.x + this->precision*(this->mapX-1)) &&		// point.x is inside and
			(this->position.z < point.z) && (point.z < this->position.z + this->precision*(this->mapZ-1));		// point.y is inside
}