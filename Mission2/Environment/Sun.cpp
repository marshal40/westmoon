
#include "stdafx.h"
#include "Sun.h"

Sun::Sun(Texture2D *texture, Vector3 position, 
	float size, Vector3 rotationAxis, float rotationSpeed_degs) : SkyObject(texture, position, size)
{
	this->rotationAxis = rotationAxis;
	this->rotatingSpeed_degs = rotationSpeed_degs;
	this->rotation_degs = 0.0f;
}

void Sun::Update()
{
	this->rotation_degs += this->rotatingSpeed_degs;
	if(this->rotation_degs >= 360.0f) this->rotation_degs -= 360.0f;
	else if(this->rotation_degs < 0.0f) this->rotation_degs += 360.0f;
}

void Sun::SetRotation(float angle)
{
	this->rotation_degs = angle;
	if(this->rotation_degs >= 360.0f) this->rotation_degs -= 360.0f;
	else if(this->rotation_degs < 0.0f) this->rotation_degs += 360.0f;
}

void Sun::Draw(Camera *camera)
{
	glPushMatrix();	
	Vector3 cameraPosition = camera->GetPosition();
	
	//Rotate to simulate rotation around the globe
	glRotatef(this->rotation_degs, this->rotationAxis.x, this->rotationAxis.y, this->rotationAxis.z);

	// Draw the sun at specific distansce from the camera
	glTranslatef(cameraPosition.x + this->position.x, 
		cameraPosition.y + this->position.y, 
		cameraPosition.z + this->position.z);
		
	// Rotate the sun so it looks at the camera
	glRotatef(this->angle, this->rotationV.x, this->rotationV.y, this->rotationV.z);	

	// draw the sun
	this->texture->bind();
	glColor3f(1.0f, 1.0f, 1.0f);
	glDisable(GL_LIGHTING);
	glScalef(this->size, this->size, this->size);
		
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.0f); glVertex3f( 1.0f,  -1.0f, 0.0f);
	glTexCoord2f(1.0f, 1.0f); glVertex3f( 1.0f,  1.0f, 0.0f);
	glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f,  1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f, -1.0f, 0.0f);		
	glEnd();

	glEnable(GL_LIGHTING);
	glPopMatrix();
}

Vector3 Sun::GetPosition()
{
	if(this->rotationAxis.x == -1.0f && 
		this->rotationAxis.y == 0.0f && 
		this->rotationAxis.z == 0.0f)
	{
		//if(this->rotation_degs > 100.0f && this->rotation_degs < 260.0f) return Vector3(0.0f, -10000.0f, 0.0f);
		Vector3 resultV = this->position;
		resultV.RotateAroundXAxis(-this->rotation_degs);
		return resultV;
	}
	else return this->position;
}

float Sun::GetRotationAroundGlobe()
{
	return this->rotation_degs;
}