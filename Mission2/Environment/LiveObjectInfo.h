
#ifndef TS_LIVEOBJECTINFO_H
#define TS_LIVEOBJECTINFO_H

#ifdef ENVIRONMENT_EXPORTS
#define ENVIRONMENT_API __declspec(dllexport)
#else
#define ENVIRONMENT_API __declspec(dllimport)
#endif

#include "BoundingBox.h"

/* Stores the most imortand info for the live object
 */
struct ENVIRONMENT_API LiveObjectInfo				
{
	BoundingBox *bb;
	float *life;
	float *armor;

	LiveObjectInfo();
	LiveObjectInfo(BoundingBox* bb, float *life, float *armor);
};

#endif