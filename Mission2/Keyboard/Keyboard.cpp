
#include "stdafx.h"
#include "Keyboard.h"

#include <cstring>

Keyboard::Keyboard()
{
	memset(this->keyStates, KEY_STATE_RELEASED, 256*sizeof(bool));
	memset(this->buttonStates,  KEY_STATE_RELEASED, 256*sizeof(bool));
	memset(this->lastKeyStates, KEY_STATE_RELEASED, 256*sizeof(bool));
}

void Keyboard::SetState (char key, bool state)
{
	this->lastKeyStates[key] = this->keyStates[key];		// override the last key state
	this->keyStates[key] = state;							// set the new state
		
	this->buttonStates[key] =								// the button is pressed only in the first moment in which the respective key is pressed
		this->lastKeyStates[key] == false &&
		this->keyStates[key] == true;		
}

void Keyboard::Update()
{
	for(short i = 0; i < 256; i++)
	{
		if(this->buttonStates[i]) 
			this->buttonStates[i] = false;
	}
}