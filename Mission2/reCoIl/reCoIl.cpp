// reCoIl.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "reCoIl.h"
#include "../GUI/GUI.h"

#include <fstream>
#include <cstring>

using namespace std;

namespace reCoIl
{
	void ReadCode(char* filename)
	{
		fstream file;
		file.open(filename, ios::in);
		if(file.is_open())
		{

		}
		else return;
	}

	bool FindOperator(fstream *file, char* _operator)
	{
		char currOperator[128];
		while(file->good())
		{
			(*file) >> currOperator;
			if(strcmp(currOperator, _operator) == 0) return true;
		}
		return false;
	}

	bool FindOperator(stringstream *code, char* _operator)
	{
		char currOperator[128];
		while(code->good())
		{
			(*code) >> currOperator;
			if(strcmp(currOperator, _operator) == 0) return true;
		}
		return false;
	}

	bool FindOperatorInThisBlock(fstream *file, char* _operator)
	{
		char currOperator[128];
		int beginEndBalance = 1;
		while(file->good() && beginEndBalance > 0)
		{
			(*file) >> currOperator;
			if(strcmp(currOperator, _operator) == 0) return true;

			if(strcmp(currOperator, "BEGIN")) beginEndBalance++;
			if(strcmp(currOperator, "END")) beginEndBalance--;
		}
		return false;
	}

	bool FindOperatorInThisBlock(stringstream *code, char* _operator)
	{
		char currOperator[128];
		int beginEndBalance = 1;
		while(code->good() && beginEndBalance > 0)
		{
			(*code) >> currOperator;
			if(strcmp(currOperator, _operator) == 0) return true;

			if(strcmp(currOperator, "BEGIN")) beginEndBalance++;
			if(strcmp(currOperator, "END")) beginEndBalance--;
		}
		return false;
	}

	bool IsNextOperator(fstream *file, char* _operator)
	{
		char currOperator[128];
		(*file) >> currOperator;

		if(strcmp(currOperator, _operator) == 0) return true;
		else return false;
	}

	bool IsNextOperator(stringstream *code, char* _operator)
	{
		char currOperator[128];
		(*code) >> currOperator;

		if(strcmp(currOperator, _operator) == 0) return true;
		else return false;
	}

	char* GetNextOperator(fstream *file)
	{
		char nextOperator[128];
		(*file) >> nextOperator;
		return nextOperator;
	}

	char* GetNextOperator(stringstream *code)
	{
		char nextOperator[128];
		(*code) >> nextOperator;
		return nextOperator;
	}

	bool GoToBlock(fstream *file, char* blockName)
	{
		while(FindOperator(file, "BEGIN"))
		{
			if(IsNextOperator(file, blockName)) return true;
		}

		return false;
	}

	bool GoToBlock(stringstream *code, char* blockName)
	{
		while(FindOperator(code, "BEGIN"))
		{
			if(IsNextOperator(code, blockName)) return true;
		}

		return false;
	}
	
	void GoToTheEndOfThisBlock(fstream *file)
	{
		int beginEndBalance = 1;
		char currOperator[128];
		
		while(beginEndBalance > 0 && file->good())
		{
			(*file) >> currOperator;
			if(strcmp(currOperator, "BEGIN") == 0) beginEndBalance++;
			else if(strcmp(currOperator, "END") == 0) beginEndBalance--;
		}
	}

	void GoToTheEndOfThisBlock(stringstream *code)
	{
		int beginEndBalance = 1;
		char currOperator[128];
		
		while(beginEndBalance > 0 && code->good())
		{
			(*code) >> currOperator;
			if(strcmp(currOperator, "BEGIN") == 0) beginEndBalance++;
			else if(strcmp(currOperator, "END") == 0) beginEndBalance--;
		}
	}

	/*
	void reCoIl::ReadDefinition(fstream file)
	{
		char type[128], varname[128];
		file >> type >> varname;
		if(strcmp(type, "MENU"))
		{
			
		}
	}
	*/
}