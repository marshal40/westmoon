
#ifndef TS_RECOIL_VARIABLESMAP_H
#define TS_RECOIL_VARIABLESMAP_H

#ifdef RECOIL_EXPORTS
#define RECOIL_API __declspec(dllexport)
#else
#define RECOIL_API __declspec(dllimport)
#endif

#include "Variable.h"
#include <vector>
#include <string>
using namespace std;

namespace reCoIl
{
	class RECOIL_API VariablesMap
	{
	private:
		vector<Variable*> vars;

	public:
		VariablesMap() {}
		void* GetVariable(string name);
		void SetVariable(Variable *var);
	};
}

#endif