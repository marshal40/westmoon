// SmartArray.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "SmartArray.h"

#include "../Environment/Terrain.h"
#include "../Environment/PlantMap.h"
#include "../Environment/Bird.h"
#include "../GUI/Window.h"
#include "../GUI/Button.h"
#include "../GUI/TextBox.h"
#include "../GUI/CompassPoint.h"
#include "../Geometry/Geometry.h"

template class SmartArray<Terrain*>;
template class SmartArray<Terrain>;
template class SmartArray<PlantMap*>;
template class SmartArray<Bird*>;
template class SmartArray<GUI::Window*>;
template class SmartArray<GUI::Button*>;
template class SmartArray<GUI::TextBox*>;
template class SmartArray<VectorMath::Vector3>;
template class SmartArray<GUI::CompassPoint*>;


template <class T>
SmartArray<T>::SmartArray()
{
	this->numElements = 0;
	this->nextElementIndex = 0;
	this->elements = NULL;
}

template <class T>
SmartArray<T>::~SmartArray()
{
	this->DeleteArray();
}

template <class T>
void SmartArray<T>::DeleteArray()
{
	delete this->elements;
	this->numElements = 0;
	this->nextElementIndex = 0;
}

template <class T>
unsigned SmartArray<T>::GetNumElements() const
{
	return this->nextElementIndex;
}

template <class T>
unsigned SmartArray<T>::GetCapacity() const
{
	return this->numElements;
}

template <class T>
void SmartArray<T>::DeclareElements(unsigned numberOfElements)
{
	this->numElements = numberOfElements;
	this->elements = new T[this->numElements];
}

template <class T>
bool SmartArray<T>::PushElement(T element)
{
	if(this->nextElementIndex < this->numElements)
	{
		this->elements[this->nextElementIndex++] = element;
		return true;
	}
	else return false;
}

template <class T>
void SmartArray<T>::EraseElement(unsigned elementIndex)
{
	if(elementIndex < this->numElements)
	{
		if(elementIndex+1 != this->nextElementIndex)
			this->elements[this->nextElementIndex-1] = this->elements[elementIndex];
		this->nextElementIndex--;
	}
}

template <class T>
T SmartArray<T>::operator[](unsigned elementIndex) const
{
	if(elementIndex < this->numElements)
		return this->elements[elementIndex];
}

template <class T>
T* SmartArray<T>::GetElementPtr(unsigned elementIndex) const
{
	if(elementIndex < this->numElements)
		return &this->elements[elementIndex];
	else return NULL;
}