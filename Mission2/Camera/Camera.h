// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the CAMERA_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// CAMERA_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef CAMERA_EXPORTS
#define CAMERA_API __declspec(dllexport)
#else
#define CAMERA_API __declspec(dllimport)
#endif

#pragma once

#include <gl\glew.h>
#include "..\Geometry\Geometry.h"
#include "..\Environment\BoundingBox.h"

#include <iostream>

class CAMERA_API Camera
{
private:
	VectorMath::Vector3 currentPos,
		currentEulerRotation_degs,
		fwdVector,
		upVector,
		rightVector;

public:
	Camera();

	void SetPosition (VectorMath::Vector3 newPosition) { this->currentPos = newPosition; }
	VectorMath::Vector3 GetPosition () { return this->currentPos; }

	VectorMath::Vector3 GetForwardVector() { return this->fwdVector; }

	void SetRotation (VectorMath::Vector3 newEulerRotation_degs);
	VectorMath::Vector3 GetRotation() { return this->currentEulerRotation_degs; }

	void ApplyOrientation();

	void DeapplyOrientation();

	void ApplyLook();

	void MoveForward (float units);

	void MoveRight (float units);

	void MoveUp (float units);
	void LookRight (float absDegrees);

	void LookLeft (float absDegrees);

	void LookUp (float absDegrees);

	void LookDown (float absDegrees);

	bool IsVisible(VectorMath::Vector3 point);	
	bool IsVisible(BoundingBox *box);
	bool IsBehind(VectorMath::Vector3 point);

	Vector3 *GetPositionPtr();
	Vector3 *GetRotationPtr();
};

