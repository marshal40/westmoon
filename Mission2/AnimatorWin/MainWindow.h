
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <fstream>
#include <cstring>
#include "ThrowError.h"
using namespace std;

struct MainWindow
{
	LPCWSTR name;
	short sizeW; 
	short sizeH; 
	std::pair<short, short> position;

	HDC			hDC;											// Private GDI Device Context
	HGLRC		hRC;											// Permanent Rendering Context
	HWND		hWnd;											// Holds Our Window Handle
	HINSTANCE	hInstance;										// Holds The Instance Of The Application

	bool fullscreen;
	bool active;

	MainWindow() { }
	MainWindow( LPCWSTR name, 
		const short &sizeW, const short &sizeH, 
		const short &posX, const short &posY,
		bool fullscreen);

	void LoadFromFile(char* filename);
	void SaveToFile(char* filename);

}; 

MainWindow::MainWindow( LPCWSTR name, 
		const short &sizeW, const short &sizeH, 
		const short &posX, const short &posY,
		bool fullscreen)
{
	this->name = name;
	this->sizeW = sizeW;
	this->sizeH = sizeH;
	this->position = std::pair<short, short>(posX, posY);
	this->hDC = NULL;
	this->hRC = NULL;
	this->hWnd = NULL;
	this->fullscreen = fullscreen;
	this->active = false;
}

void MainWindow::LoadFromFile(char* filename)
{
	fstream file;
	file.open(filename, ios::in);
	if(!file.is_open())
	{
		ThrowError(TEXT("Cannot find the window initialization file"));
		exit(0);							
	}
	char name_cstr [128];
	file.getline(name_cstr, 128);

	WCHAR *wstr = new WCHAR[strlen(name_cstr) + 1];
	MultiByteToWideChar( 0,0, name_cstr, strlen(name_cstr), wstr, strlen(name_cstr)+1);
	this->name = wstr;

	file >> this->sizeW >> this->sizeH;
	file >> this->position.first >> this->position.second;
	file >> this->fullscreen;

	this->hDC = NULL;
	this->hRC = NULL;
	this->hWnd = NULL;
	this->active = false;
}

void MainWindow::SaveToFile(char* filename)
{
	fstream file;
	file.open(filename, ios::out);
	file << this->name << endl;
	file << this->sizeW << " " << this->sizeH << endl;
	file << this->position.first << " " << this->position.second << endl;
	file << this->fullscreen << endl;
	file.close();
}

#endif