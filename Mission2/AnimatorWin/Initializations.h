
#ifdef INITIALIZATIONS_H
#error Initializations.h included twice
#else
#define INITIALIZATIONS_H

Vector3 CalibrateScreen()
{
	glClearColor (1.0, 1.0, 1.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	
	glDisable(GL_TEXTURE_2D);
	glColor3f(0.0f, 0.0f, 0.0f);
	glBegin(GL_QUADS);
	glVertex3f(+100.0f, -100.0f, -10.0f);
	glVertex3f(+100.0f, +100.0f, -10.0f);
	glVertex3f(-100.0f, +100.0f, -10.0f);
	glVertex3f(-100.0f, -100.0f, -10.0f);
	glEnd();

	SwapBuffers(mainWindow.hDC);

	mouse.SetCurrentPosition(VectorMath::Vector3((float)mainWindow.sizeW - 1 , 1.0f));
	VectorMath::Vector3 currOGLPosition = ScreenToOGLCoordinates((int)mouse.GetCurrentPosition().x,
																(int)mouse.GetCurrentPosition().y);

	guiDimensions.x = currOGLPosition.x;
	guiDimensions.y = currOGLPosition.y;
	return Vector3(currOGLPosition.x, currOGLPosition.y, -10.0f);
}

/*
void ShowLoadingScreen()
{
	Texture2DManager *texMgr;
	texMgr = Texture2DManager::getInstance();
	
	glClearColor (1.0, 1.0, 1.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	glEnable(GL_TEXTURE_2D);
	westMoonGUI->textureMenu->bind();
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 1.0f); glVertex3f(0, SCREEN_Y, -10.0f);
	glTexCoord2f(1.0f, 1.0f); glVertex3f(SCREEN_X, SCREEN_Y, -10.0f);
	glTexCoord2f(1.0f, 0.0f); glVertex3f(SCREEN_X, 0.0f, -10.0f);
	glTexCoord2f(0.0f, 0.0f); glVertex3f(0.0f, 0.0f, -10.0f);
	glEnd();

	SwapBuffers(mainWindow.hDC);
}*/

void LoadEnvironmentData()
{
	Texture2DManager *texMgr;
	texMgr = Texture2DManager::getInstance();

	EnvironmentData::textureGrass = texMgr->load("Data/Textures/grass.tga");
	EnvironmentData::textureWater = texMgr->load("Data/Textures/water.tga");
	EnvironmentData::textureSoil = texMgr->load("Data/Textures/soil.tga");
	EnvironmentData::textureRock = texMgr->load("Data/Textures/rock.tga");
}

void InitializeObjects()
{
	Vector3 guiDimensions = CalibrateScreen();
	westMoonGUI = new WestMoonGUI(guiDimensions);
	LoadEnvironmentData();
}

#endif