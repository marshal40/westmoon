
#ifndef HORSE_H
#define HORSE_H


class Horse
{
private:
	VectorMath::Vector3 position,
		rotation;

	Md2Player *horse;

	float linearSpeed;

	short counter;

public:

	enum HorseStatus{STAND, RUN} status;

	Horse(VectorMath::Vector3 position = VectorMath::Vector3(),
		VectorMath::Vector3 rotation = VectorMath::Vector3())
	{
		horse = new Md2Player("Data/Models/Horse/horse.md2", 
			"Data/Models/Horse/horse.pcx");

		this->position = position;
		this->rotation = rotation;

		this->linearSpeed = 30.0;

		this->status = STAND;

		this->counter = 0;
	}

	~Horse()
	{
		delete horse;
	}

	void Draw()
	{
		switch(this->status)
		{
		case STAND:
			glPushMatrix();
			glTranslatef(this->position.x, position.y, position.z);
			glRotatef(this->rotation.y, 0.0, 1.0, 0.0);	
			horse->drawPlayerFrame(0, static_cast<Md2Object::Md2RenderMode>(1));
			glPopMatrix();
			break;

		case RUN:
			//Draw
			glPushMatrix();
			glTranslatef(this->position.x, position.y, position.z);
			glRotatef(this->rotation.y, 0.0, 1.0, 0.0);	
			horse->drawPlayerItp(true, static_cast<Md2Object::Md2RenderMode>(1));
			glPopMatrix();

			//Animate
			horse->animate(Timer::frameRate*0.1f, true);
			break;
		}
		
	}

	void Update()
	{
		if(keyboard.GetButtonState(VK_RETURN))
		{
			switch(this->status)
			{
				case STAND: this->status = RUN; break;
				case RUN: this->status = STAND; break;
			}
		}

		switch(this->status)
		{
		case RUN:
			VectorMath::Vector3 translation(0.0, 0.0, this->linearSpeed);
			translation.RotateAroundYAxis(this->rotation.y);
			this->position = this->position + translation;

			if(this->counter >= 60)									//if is time for rotate
			{
				this->counter = 0;
				
				float newAngle = this->rotation.y - 90;
				if(newAngle < 0) newAngle += 360;
				this->rotation.y = newAngle;
			}
			this->counter++;
			break;
		}		
	}

	void SetPosition(VectorMath::Vector3 newPosition)
	{
		this->position = newPosition;
	}

	void SetRotation(VectorMath::Vector3 newRotation)
	{
		this->rotation = newRotation;
	}

	void SetStatus(HorseStatus status)
	{
		this->status = status;
	}

	HorseStatus GetStatus() 
	{
		return this->status;
	}

};


#endif