
#ifndef MISSIONSLOGIC_H
#define MISSIONSLOGIC_H

#ifndef NULL
#define NULL 0
#endif

#include "ThrowError.h"
#include "../GUI/GameMessage.h"
#include "../GUI/TextBox.h"
#include "../GUI/BitmapFont.h"
#include <string>
#include <cstring>
#include <fstream>

#define MESSAGE_LEN 128

class Objective
{
private:
	char todoMessage[MESSAGE_LEN];
	char doneMessage[MESSAGE_LEN];

public:
	Objective() {}		

	Objective(char todoMessage[MESSAGE_LEN], char doneMessage[MESSAGE_LEN])
	{
		strcpy_s(this->todoMessage, todoMessage);
		strcpy_s(this->doneMessage, doneMessage);	
	}

	char* GetTodoMessage() { return this->todoMessage; }
	char* GetDoneMessage() { return this->doneMessage; }
};

class Mission
{
private:
	unsigned objectivesNum;
	Objective* objectives;
	vector<unsigned> currObjectives;

	int currIndex;

	bool started;
	bool accomplished;

	GUI::GameMessageHandler *gmh;

public:

	Mission(GUI::GameMessageHandler *gmh) 
	{
		this->objectivesNum = 0;
		this->accomplished = false;
		this->currIndex = 0;
		this->gmh = gmh;
		this->started = false;
	}
	
	void LoadObjectiveList(char* filename)
	{
		fstream file;
		file.open(filename, ios::in);
		if(!file.is_open())
		{
			ThrowError(TEXT("Cannot open the file with the list of objectives."));
			exit(1);
		}
		else
		{
			file >> this->objectivesNum;
			this->objectives = new Objective[this->objectivesNum];

			char todoMessage[MESSAGE_LEN], doneMessage[MESSAGE_LEN];
			file.getline(todoMessage, MESSAGE_LEN);						// get the rest of the first line

			for(unsigned i = 0; i < this->objectivesNum; i++)
			{
				file.getline(todoMessage, MESSAGE_LEN);
				file.getline(doneMessage, MESSAGE_LEN);
				this->PushBack(Objective(todoMessage, doneMessage));
			}
		}
		file.close();
	}

	int PushBack(Objective obj)
	{
		if(this->currIndex == this->objectivesNum)
		{
			ThrowError(TEXT("Defined more objectives than declared"));
			exit(1);
		}
		this->objectives[this->currIndex++] = obj;
		return this->currIndex-1;
	}

	void SetObjectivesNum(int objectivesNum)
	{
		this->objectivesNum = objectivesNum;
		this->objectives = new Objective[this->objectivesNum];
	}

	void StartMission()
	{
		if(this->started == false)
		{
			this->started = true;
		}
	}

	bool IsStarted() { return this->started; }

	void PrintObjectives(GUI::Box gui, GUI::BitmapFont *font)
	{
		// Draw the "todo" objectives 
		if(this->accomplished == false)
		{
			for(unsigned i = 0; i < this->currObjectives.size(); i++)
			{
				GUI::TextBox tb(font, 0.25f, gui, 
									GUI::Box(VectorMath::Vector3(0.05f, 0.3f + i*0.05f), VectorMath::Vector3(0.65f, 0.325f + i*0.05f)),
									this->objectives[this->currObjectives[i]].GetTodoMessage(), 
									GUI::LEFT);
		
				tb.SetColor(Vector3(1.0f, 1.0f, 0.0f));
				tb.Draw();
			}
		}
	}

	bool IsAccomplished() { return this->accomplished; }

	void SetCurrObjective(int index, bool throwMessage)
	{
		this->currObjectives.push_back(index);

		if(throwMessage)
		{
			char message[128];
			strcpy_s(message, this->objectives[index].GetTodoMessage());

			this->gmh->PushBack(message);
		}
	}

	void RemoveCurrObjective(int index, bool throwMessage)
	{
		for(unsigned i = 0; i < this->currObjectives.size(); i++)
			if(this->currObjectives[i] == index)
			{
				if(throwMessage)
				{
					char message[128];
					message[0] = (char)(GUI::BitmapFont::TICK);
					message[1] = '\0';
					strcat_s(message, this->objectives[this->currObjectives[i]].GetDoneMessage());
					this->gmh->PushBack(message);
				}
				this->currObjectives.erase(this->currObjectives.begin() + i);
				break;
			}
	}

	
	void LoadFromFile(std::fstream *file)
	{
		while(this->currObjectives.size() > 0)
			this->RemoveCurrObjective(0, false);

		unsigned currObjectivesSize;
		file->read((char*)&currObjectivesSize, sizeof(currObjectivesSize));
		this->currObjectives.reserve(currObjectivesSize);
		for(unsigned i = 0; i < currObjectivesSize; i++)
		{	
			int currCurrObjective;
			file->read((char*)&currCurrObjective, sizeof(currCurrObjective));
			this->currObjectives.push_back(currCurrObjective);
		}
		file->read((char*)&this->started, sizeof(this->started));
		file->read((char*)&this->accomplished, sizeof(this->accomplished));
	}

	void SaveToFile(std::fstream *file)
	{
		unsigned currObjectivesNumber = this->currObjectives.size();
		file->write((char*)&currObjectivesNumber, sizeof(currObjectivesNumber));					// Save the number of the current objectives
		for(unsigned i = 0; i < this->currObjectives.size(); i++)
		{
			file->write((char*)&this->currObjectives[i], sizeof(this->currObjectives[i]));			// Save the current objectives indexes
		}
		file->write((char*)&this->started, sizeof(this->started));
		file->write((char*)&this->accomplished, sizeof(this->accomplished));
	}

	unsigned GetCurrObjectivesNum() { return this->currObjectives.size(); }
	unsigned GetCurrObjectiveIndex(unsigned index) { return this->currObjectives[index]; }
	Objective GetObjective(unsigned index) { return this->objectives[index]; }

	void Accomplish() { this->accomplished = true; }
};


#endif