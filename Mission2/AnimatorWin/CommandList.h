
#pragma once
#include <list>
using namespace std;

class CommandList
{
public:
	enum Commands{STAY, FORWARD, BACKWARD, ROTATE_LEFT, ROTATE_RIGHT, FORWARD_LEFT, FORWARD_RIGHT, BACKWARD_LEFT, BACKWARD_RIGHT, FIRE};
private:
	list<Commands> commands;
	list<Commands>::iterator currCommand;
public:
	void ReadFromFile(char* filename);
	Commands GetCurrCommand();

	CommandList(void);
	~CommandList(void);

	
};

#include <fstream>
#include "ThrowError.h"
#include "../reCoIl/reCoIl.h"

CommandList::CommandList(void)
{
}


CommandList::~CommandList(void)
{
}

void CommandList::ReadFromFile(char* filename)
{
	fstream log("log.txt", ios::out);
	fstream file(filename, ios::in);
	if(file.is_open())
	{
		char oper[256];
		while(file.good())
		{
			strcpy(oper, reCoIl::GetNextOperator(&file));

			if(!strcmp("REPEAT", oper))
			{
				//log << "REPEAT ";
				int numInterations;
				file >> numInterations;
				reCoIl::GetNextOperator(&file);		// Get FRAMES
				strcpy(oper, reCoIl::GetNextOperator(&file));
								
				if(!strcmp("FORWARD", oper))
					for(int i = 0; i < numInterations; i++)
						this->commands.push_back(Commands::FORWARD);
				else if(!strcmp("BACKWARD", oper))
					for(int i = 0; i < numInterations; i++)
						this->commands.push_back(Commands::BACKWARD);
				else if(!strcmp("ROTATE_LEFT", oper))
					for(int i = 0; i < numInterations; i++)
						this->commands.push_back(Commands::ROTATE_LEFT);
				else if(!strcmp("ROTATE_RIGHT", oper))
					for(int i = 0; i < numInterations; i++)
						this->commands.push_back(Commands::ROTATE_RIGHT);
				else if(!strcmp("FORWARD_RIGHT", oper))
					for(int i = 0; i < numInterations; i++)
						this->commands.push_back(Commands::FORWARD_RIGHT);
				else if(!strcmp("FORWARD_LEFT", oper))
					for(int i = 0; i < numInterations; i++)
						this->commands.push_back(Commands::FORWARD_LEFT);
				else if(!strcmp("BACKWARD_RIGHT", oper))
					for(int i = 0; i < numInterations; i++)
						this->commands.push_back(Commands::BACKWARD_RIGHT);
				else if(!strcmp("BACKWARD_LEFT", oper))
					for(int i = 0; i < numInterations; i++)
						this->commands.push_back(Commands::BACKWARD_LEFT);
				else if(!strcmp("FIRE", oper))
					for(int i = 0; i < numInterations; i++)
						this->commands.push_back(Commands::FIRE);
				else if(!strcmp("STAY", oper))
					for(int i = 0; i < numInterations; i++)
						this->commands.push_back(Commands::STAY);
			}
			else if(!strcmp("FORWARD", oper))
			{
				this->commands.push_back(Commands::FORWARD);
			}
			else if(!strcmp("BACKWARD", oper))
			{
				this->commands.push_back(Commands::BACKWARD);
			}
			else if(!strcmp("ROTATE_LEFT", oper))
			{
				this->commands.push_back(Commands::ROTATE_LEFT);
			}
			else if(!strcmp("ROTATE_RIGHT", oper))
			{
				this->commands.push_back(Commands::ROTATE_RIGHT);
			}
			else if(!strcmp("FORWARD_RIGHT", oper))
			{
				this->commands.push_back(Commands::FORWARD_RIGHT);
			}
			else if(!strcmp("FORWARD_LEFT", oper))
			{
				this->commands.push_back(Commands::FORWARD_LEFT);
			}
			else if(!strcmp("BACKWARD_RIGHT", oper))
			{
				this->commands.push_back(Commands::BACKWARD_RIGHT);
			}
			else if(!strcmp("BACKWARD_LEFT", oper))
			{
				this->commands.push_back(Commands::BACKWARD_LEFT);
			}
			else if(!strcmp("FIRE", oper))
			{
				this->commands.push_back(Commands::FIRE);
			}
			else if(!strcmp("STAY", oper))
			{
				this->commands.push_back(Commands::STAY);
			}
			else if(!strcmp("END", oper))
				break;
		}
		this->currCommand = this->commands.begin();
		for(list<Commands>::iterator it = this->commands.begin(); 
			it != this->commands.end(); it++)
		{
			switch(*it)
			{
			case STAY: log << "STAY\n"; break;
			case FORWARD: log << "FORWARD\n"; break;
			case BACKWARD: log << "BACKWARD\n"; break;
			case FIRE: log << "FIRE\n"; break;
			default : log << "def\n"; break;
			}
		}
	}
	else
	{
		ThrowError(L"Cannot open command list.");
		return;
	}
}


CommandList::Commands CommandList::GetCurrCommand()
{
	Commands result =  *this->currCommand;
	if(this->currCommand != this->commands.end())
	{
		this->currCommand++;
		return result;
	}
	else return Commands::STAY;
}