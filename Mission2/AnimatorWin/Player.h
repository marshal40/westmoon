

#ifndef PLAYER_H
#define PLAYER_H

#include <sstream>
#include <string>
#include <vector>

#include "Gun.h"
#include "../Environment/LiveObject.h"
#include "../Mouse/Mouse.h"
#include "../Keyboard/Keyboard.h"

class Player : public LiveObject
{
public:
	enum CameraMode { FIRST_PERSON, THIRD_PERSON};
private:	
	VectorMath::Vector3 cameraPosition;
	VectorMath::Vector3 cameraRotation;
	Camera camera;

	Camera firstPsn;
	Vector3 firstPsnC_position; //, firstPsnC_rotation;

	
	CameraMode cameraMode;
	enum PlayerState {STAND, RUN, ATTACK} state;
	bool updateCamera;

	bool tired;
public:
	Gun *leftGun, *rightGun;

	void AnimStoped();

	float tiredness;

	Player(Environment* environment);
	~Player();

	Camera* GetCamera(CameraMode mode) { return (mode)?(&(this->camera)):(&this->firstPsn); }

	void Update(Keyboard *keyboard, Mouse *mouse);
	void UpdateCamera();

	void SaveToFile(fstream *file);
	void LoadFromFile(fstream *file);

	bool IsMoving() { return this->state == RUN; } 


	Vector3* GetPositionPtr() { return &this->position; }
	Vector3* GetRotationPtr() { return &this->rotation; }

	void FireLeft();
	void FireRight();
	void DontFire();
private:	
	void MoveCameraDown(float angle);
	void MoveCameraUp(float angle);

	void KeyboardControl(Keyboard *keyboard);
	void MouseControl(Mouse *mouse);
};

Player::Player(Environment* environment) : LiveObject(new Md2Player("Data/Models/OldBounceHand/old_bouncehand.md2", 
																	"Data/Models/OldBounceHand/old_bouncehand.pcx",
																	"Data/Models/gun/guns.md2",
																	"Data/Models/gun/guns.pcx"),
																	Vector3(-676.0f, 0.0f, -366.0f), Vector3(0.0f, 150.0f, 0.0f), 
																	5.0f, 5.0f, 90.0f, 
																	BoundingBox(Vector3(0.0f, 25.0f, 0.0f), Vector3(10.0f, 25.0f, 10.0f)), 
																	environment,
																	30.0f, 1.0f)
{	
	character->setAnim("stand");
	camera = Camera();
	this->UpdateCamera();
	this->state = STAND;
	this->cameraRotation = Vector3(-12.0f, 0.0f, 0.0f);
	this->cameraPosition = Vector3(0.0f, 65.0f, -65.0f);
	this->tiredness = 0.0f;
	this->tired = false;

	this->firstPsn = Camera();
	this->firstPsnC_position = Vector3(0.0f, 65.0f, 0.0f);

	this->cameraMode = CameraMode::THIRD_PERSON;

	this->leftGun = new Gun(6, 6);
	this->rightGun = new Gun(6, 6);
}

Player::~Player() { delete character; }

void Player::MoveCameraDown(float angle)
{
	if(this->cameraRotation.x - angle > -60.0f)
	{
		this->cameraPosition.RotateAroundXAxis(angle);
		this->cameraRotation.x -= angle;

		if(this->cameraRotation.x < -90.0f) 
			this->cameraRotation.x = -90.0f;
	}

	//this->firstPsnC_rotation.x = this->cameraRotation.x + 12.0f;
}
void Player::MoveCameraUp(float angle)
{
	if(this->cameraRotation.x + angle < 70.0f)
	{
		this->cameraPosition.RotateAroundXAxis(-angle);
		this->cameraRotation.x += angle;

		if(this->cameraRotation.x > 90.0f) 
			this->cameraRotation.x = 90.0f;
	}
	//this->firstPsnC_rotation.x = this->cameraRotation.x + 12.0f;
}

void Player::KeyboardControl(Keyboard *keyboard)
{
	if(this->state != ATTACK)
		this->state = STAND;

	bool moving = false;

	if(this->tired)
	{
		if(this->tiredness < 60.0f)
			this->tired = false;
	}

	if((keyboard->GetKeyState(VK_UP) || keyboard->GetKeyState('W')) && !this->tired)
	{
		moving = true;

		if(this->character->currentAnim().compare("run") != 0)
		{
			this->character->setAnim("run");
		}
			
		this->MoveForward();				
		this->updateCamera = true;
		this->state = RUN;
	}

	if(keyboard->GetKeyState(VK_LEFT) || keyboard->GetKeyState('Q'))
	{
		this->updateCamera = true;
		this->TurnLeft(this->angularSpeed);
	}
		
	if(keyboard->GetKeyState(VK_RIGHT) || keyboard->GetKeyState('E'))
	{
		this->updateCamera = true;
		this->TurnRight(this->angularSpeed);
	}

	if(keyboard->GetKeyState('A')  && !this->tired)
	{
		moving = true;
		this->updateCamera = true;
		this->MoveLeft();
	}

	if(keyboard->GetKeyState('D') && !this->tired)
	{
		moving = true;
		this->updateCamera = true;
		this->MoveRight();
	}

	if((keyboard->GetKeyState('S') || keyboard->GetKeyState(VK_DOWN))  && !this->tired)
	{
		moving = true;
		this->updateCamera = true;
		this->MoveBack();
	}

	if(keyboard->GetKeyState('R'))
	{
		this->leftGun->Reload();
		this->rightGun->Reload();
	}

	if(keyboard->GetButtonState(VK_SPACE))
	{
		moving = true;
		this->character->setAnim("attack");
		this->state = ATTACK;
	}

	if(this->state == STAND && this->character->currentAnim().compare("stand") != 0)		//If current animation is "stand"
	{
		this->character->setAnim("stand");
	}

	if(moving)
	{
		if(this->character->currentAnim().compare("run") != 0)
			this->character->setAnim("run");

		if(this->tiredness < 99.8f)		
			this->tiredness += 0.3f;
		else this->tired = true;
	}
	else
	{
		if(this->tiredness > 0.2f)
			this->tiredness -= 0.3f;
	}
}
void Player::MouseControl(Mouse *mouse)
{
	if(currCamera == &this->camera || currCamera == &this->firstPsn)
	{
		// handle horizontal movements
		int rotationX = (int) (mouse->GetCurrentPosition().x - mouse->GetLastPosition().x);

		if(rotationX < 0)
		{
			this->updateCamera = true; 
			this->TurnLeft(-(float)rotationX);
		}
		else if(rotationX > 0)
		{
			this->updateCamera = true; 
			this->TurnRight((float)rotationX);
		}

		//handle verticle movements
		int rotationY = -(int)(mouse->GetCurrentPosition().y - mouse->GetLastPosition().y);

		if(rotationY < 0) 
		{
			this->updateCamera = true;
			this->MoveCameraDown(-(float)rotationY);
		}
		else if(rotationY > 0)
		{
			this->updateCamera = true;
			this->MoveCameraUp((float)rotationY);
		}
	}

	/*
	if(mouse->IsLeftButtonDown())
	{
		this->character->setAnim("attack");
		this->state = ATTACK;
		
	}
	
	if(mouse->IsRightButtonDown())
	{
		this->character->setAnim("attack");
		this->state = ATTACK;
		this->rightGun->Fire();	
	}
	*/
}

void Player::AnimStoped()
{
	this->SetAnimation("stand");
}

void Player::FireLeft()
{
	this->leftGun->Fire();
	this->SetAnimation("shoot_left");
	this->state = ATTACK;
	this->unlim = false;
}

void Player::FireRight()
{
	this->rightGun->Fire();	
	this->SetAnimation("shoot_right");
	this->state = ATTACK;
	this->unlim = false;
}

void Player::DontFire()
{
	//this->state = STAND;
}

void Player::Update(Keyboard *keyboard, Mouse *mouse)
{
	this->updateCamera = false;

	this->KeyboardControl(keyboard);
	this->MouseControl(mouse);

	//if(updateCamera && currCamera == &(this->camera)) 
	this->UpdateCamera();		
}

void Player::UpdateCamera()
{		
	float currCameraRotY = this->camera.GetRotation().y;
	this->camera.SetRotation(VectorMath::Vector3(-12.0, currCameraRotY, 0.0));

	this->camera.SetRotation(this->cameraRotation+VectorMath::Vector3(0.0f, this->rotation.y - 180.0f, 0.0f));

	VectorMath::Vector3 newCameraPosition = this->cameraPosition;
	newCameraPosition.RotateInZX(this->rotation.y);
	newCameraPosition = newCameraPosition + this->position;
	this->camera.SetPosition(newCameraPosition);
	this->firstPsn.SetPosition(this->firstPsnC_position + this->position);
	this->firstPsn.SetRotation(Vector3(this->camera.GetRotation()) + Vector3(12.0f, 0.0f, 0.0f));

	WarpPointer(mainWindow.sizeW/2, mainWindow.sizeH/2);
	mouse.OverrideLastPosition (VectorMath::Vector3((float)(mainWindow.sizeW/2), (float)(mainWindow.sizeH/2)));
}

void Player::SaveToFile(fstream *file)
{
	file->write((char*)&this->position, sizeof(this->position));
	file->write((char*)&this->rotation.y, sizeof(this->rotation.y));
	file->write((char*)&this->camera, sizeof(this->camera));
	file->write((char*)&this->state, sizeof(this->state));
	file->write((char*)&this->life, sizeof(this->life));
	file->write((char*)&this->tiredness, sizeof(this->tiredness));
}

void Player::LoadFromFile(fstream *file)
{
	file->read((char*)&this->position, sizeof(this->position));
	this->SetPosition(this->position);
	file->read((char*)&this->rotation.y, sizeof(this->rotation.y));
	file->read((char*)&this->camera, sizeof(this->camera));
	file->read((char*)&this->state, sizeof(this->state));
	file->read((char*)&this->life, sizeof(this->life));
	file->read((char*)&this->tiredness, sizeof(this->tiredness));
}

#endif