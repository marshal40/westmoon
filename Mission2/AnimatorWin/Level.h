
#ifndef LEVEL_H
#define LEVEL_H

#include "MissionsLogic.h"
#include "../Camera/Camera.h"
#include "../Mouse/Mouse.h"
#include "../Keyboard/Keyboard.h"
#include "../Environment/Environment.h"
#include "Player.h"

class Level
{	
public:
	Mission *currMission;

	virtual void Init() {}
	virtual Camera* GetPlayerCamera(Player::CameraMode mode) { return NULL; }
	virtual void UpdatePlayerCamera() {}
	virtual void Update(Keyboard *keyboard, Mouse *mouse) {}
	virtual void Draw(Camera *camera) {}
	virtual void LoadFromFile(const char* filename) { }
	virtual void SaveToFile(const char* filename) { }
	virtual Environment* GetEnvironment() { return NULL; }
	virtual void Draw2D(Camera *camera) { }
};

#endif