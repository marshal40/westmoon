
#ifdef TS_GUIDEFINITIONS_HPP
#error GUIDefinitions.hpp icluded twice
#endif
#define TS_GUIDEFINITIONS_HPP

#include "../reCoIl/reCoIl.h"

const int numberOfSaves = 10;
const char* saveInfoFilenames[] = {					// precomputed filenames
	"Saves/save01.tsi",
	"Saves/save02.tsi",
	"Saves/save03.tsi",
	"Saves/save04.tsi",
	"Saves/save05.tsi",
	"Saves/save06.tsi",
	"Saves/save07.tsi",
	"Saves/save08.tsi",
	"Saves/save09.tsi",
	"Saves/save10.tsi"}; 
const char* saveFilenames[] = {
	"Saves/save01.tss",
	"Saves/save02.tss",
	"Saves/save03.tss",
	"Saves/save04.tss",
	"Saves/save05.tss",
	"Saves/save06.tss",
	"Saves/save07.tss",
	"Saves/save08.tss", 
	"Saves/save09.tss",
	"Saves/save10.tss"}; 
GUI::Button *saves[numberOfSaves];
bool isSaveEmpty[numberOfSaves];

class WestMoonGUI
{
public:
	GUI::GUI* gui;								// GUI Space
	GUI::Window* currWindow;
	GUI::BitmapFont* lucidaConsole;				// Font
	Texture2D* textureMenu;						// The background of the menu
	Texture2D* textureMenuBig;

	// Menus
	//GUI::Window* pauseMenu;														// TODO: rename variables
	//GUI::Window* creditsMenu;
	//GUI::Window* gameMenu;
	//GUI::Window* loadingWindow;
	//GUI::Window* byeWindow;
	//GUI::Window* objectiveMenu;
	//GUI::Window* saveGameMenu;
	//GUI::Window* loadGameMenu;

	// MESSAGE HANDLER
	GUI::GameMessageHandler *messageHandler;

	Vector3 guiDimensions;
private:
	void InitGameMenu();
	void InitByeWindow();
	void InitLoadingWindow();
	void InitObjectiveWindow();
	void InitMainMenu();
	void InitCreditsMenu();
	void InitLoadGameMenu();
	void InitSaveGameMenu();
	void SaveSytemTime(const char* filename);

public:
	WestMoonGUI(Vector3 guiDimensions) {this->InitGUI(guiDimensions); }// this->InitFromFile(guiDimensions); } //

	bool InitGUI(Vector3 guiDimenstions);
	void InitFromFile();

	void UpdateGUI();

	void LoadSavesButtons();
	void DeleteSavesButtons();

	Vector3 GetDimensions() const { return this->guiDimensions; }
} *westMoonGUI;

void WestMoonGUI::InitGameMenu()
{
	//gameMenu = 
	gui->AddWindow(new GUI::Window("gameMenu", gui->box, Vector3(.0f, .0f), Vector3(1.0f, 1.0f), textureMenu));
	gui->GetWindow("gameMenu")->ReserveButtons(4);			

	gui->GetWindow("gameMenu")->AddButton(new GUI::Button("StartGame", lucidaConsole, 
										0.5f,
										gui->GetWindow("gameMenu")->GetBox(), 
										GUI::Box(VectorMath::Vector3(0.0f, 0.25f), VectorMath::Vector3(0.7f, 0.3f)),
										"������"));
	gui->GetWindow("gameMenu")->AddButton(new GUI::Button("LoadGame", lucidaConsole, 
										0.5f,
										gui->GetWindow("gameMenu")->GetBox(), 
										GUI::Box(VectorMath::Vector3(0.0f, 0.35f), VectorMath::Vector3(0.7f, 0.4f)),
										"������"));
	gui->GetWindow("gameMenu")->AddButton(new GUI::Button("Credits", lucidaConsole, 
										0.5f,
										gui->GetWindow("gameMenu")->GetBox(), 
										GUI::Box(VectorMath::Vector3(0.0f, 0.45f), VectorMath::Vector3(0.7f, 0.5f)),
										"�����"));
	gui->GetWindow("gameMenu")->AddButton(new GUI::Button("Exit", lucidaConsole, 
										0.5f,
										gui->GetWindow("gameMenu")->GetBox(), 
										GUI::Box(VectorMath::Vector3(0.0f, 0.55f), VectorMath::Vector3(0.7f, 0.6f)),
										"�����"));
}

void WestMoonGUI::InitByeWindow()
{
	//byeWindow = 
	gui->AddWindow(new GUI::Window("byeWindow", gui->box, Vector3(.0f, .0f), Vector3(1.0f, 1.0f), textureMenu));
	gui->GetWindow("byeWindow")->ReserveTextBoxes(1);

	gui->GetWindow("byeWindow")->AddTextBox(new GUI::TextBox(lucidaConsole, 
										0.8f,
										gui->GetWindow("byeWindow")->GetBox(), 
										GUI::Box(VectorMath::Vector3(0.0f, 0.0f), VectorMath::Vector3(0.7f, 0.9f)),
										"��� :)"));
}

void WestMoonGUI::InitLoadingWindow()
{
	//loadingWindow = 
	gui->AddWindow(new GUI::Window("loadingWindow", gui->box, Vector3(.0f, .0f), Vector3(1.0f, 1.0f), textureMenu));
	gui->GetWindow("loadingWindow")->ReserveTextBoxes(1);

	gui->GetWindow("loadingWindow")->AddTextBox(new GUI::TextBox(lucidaConsole, 
										0.7f,
										gui->GetWindow("loadingWindow")->GetBox(), 
										GUI::Box(VectorMath::Vector3(0.0f, 0.0f), VectorMath::Vector3(0.7f, 0.9f)),
										"���������..."));
}

void WestMoonGUI::InitObjectiveWindow()
{
	//objectiveMenu = 
	gui->AddWindow(new GUI::Window("objectiveMenu", gui->box, Vector3(.0f, .0f), Vector3(1.0f, 1.0f), textureMenu));
	gui->GetWindow("objectiveMenu")->ReserveTextBoxes(1);
	gui->GetWindow("objectiveMenu")->ReserveButtons(1);

	gui->GetWindow("objectiveMenu")->SetTitleBox(new GUI::TextBox(lucidaConsole, 
										0.5f,
										gui->GetWindow("objectiveMenu")->GetBox(), 
										GUI::Box(VectorMath::Vector3(0.0f, 0.0f), VectorMath::Vector3(0.6f, 0.3f)),
										"������"));

	gui->GetWindow("objectiveMenu")->AddButton(new GUI::Button("Back", lucidaConsole, 
										0.5f,
										gui->GetWindow("objectiveMenu")->GetBox(), 
										GUI::Box(VectorMath::Vector3(0.0f, 0.85f), VectorMath::Vector3(0.1f, 0.9f)),
										"<")); 
}

void WestMoonGUI::InitMainMenu()
{
	//pauseMenu = 
	gui->AddWindow(new GUI::Window("mainMenu", gui->box, Vector3(0.0f, 0.0f), Vector3(1.0f, 1.0f), textureMenu));
	gui->GetWindow("mainMenu")->ReserveButtons(6);


	gui->GetWindow("mainMenu")->AddButton(new GUI::Button("Resume", lucidaConsole, 
									0.5f,
									gui->GetWindow("mainMenu")->GetBox(), 
									GUI::Box(VectorMath::Vector3(0.0f, 0.2f), VectorMath::Vector3(0.7f, 0.25f)),
									"�������"));
	gui->GetWindow("mainMenu")->AddButton(new GUI::Button("Objectives", lucidaConsole, 
									0.5f,
									gui->GetWindow("mainMenu")->GetBox(), 
									GUI::Box(VectorMath::Vector3(0.0f, 0.3f), VectorMath::Vector3(0.7f, 0.35f)),
									"������"));
	gui->GetWindow("mainMenu")->AddButton(new GUI::Button("SaveGame", lucidaConsole, 
									0.5f,
									gui->GetWindow("mainMenu")->GetBox(), 
									GUI::Box(VectorMath::Vector3(0.0f, 0.4f), VectorMath::Vector3(0.7f, 0.45f)),
									"������"));
	gui->GetWindow("mainMenu")->AddButton(new GUI::Button("LoadGame", lucidaConsole, 
									0.5f,
									gui->GetWindow("mainMenu")->GetBox(), 
									GUI::Box(VectorMath::Vector3(0.0f, 0.5f), VectorMath::Vector3(0.7f, 0.55f)),
									"������"));
	gui->GetWindow("mainMenu")->AddButton(new GUI::Button("Credits", lucidaConsole, 
									0.5f,
									gui->GetWindow("mainMenu")->GetBox(), 
									GUI::Box(VectorMath::Vector3(0.0f, 0.6f), VectorMath::Vector3(0.7f, 0.65f)),
									"�����"));
	gui->GetWindow("mainMenu")->AddButton(new GUI::Button("Exit", lucidaConsole, 
									0.5f,
									gui->GetWindow("mainMenu")->GetBox(), 
									GUI::Box(VectorMath::Vector3(0.0f, 0.7f), VectorMath::Vector3(0.7f, 0.75f)),
									"�����"));
}

void WestMoonGUI::InitCreditsMenu()
{
	//creditsMenu = 
	gui->AddWindow(new GUI::Window("creditsMenu",  gui->box, Vector3(.0f, .0f), Vector3(1.0f, 1.0f), textureMenu));
	gui->GetWindow("creditsMenu")->ReserveTextBoxes(13);
	gui->GetWindow("creditsMenu")->ReserveButtons(1);

	gui->GetWindow("creditsMenu")->AddButton(new GUI::Button("Back", lucidaConsole, 
											0.5f, 
											gui->GetWindow("creditsMenu")->GetBox(), 
											GUI::Box(VectorMath::Vector3(0.0f, 0.85f), VectorMath::Vector3(0.1f, 0.9f)), 
											"<"));
	gui->GetWindow("creditsMenu")->SetTitleBox(new GUI::TextBox(lucidaConsole, 
											0.75f,
											gui->GetWindow("creditsMenu")->GetBox(),
											GUI::Box(VectorMath::Vector3(0.05f, 0.0f), VectorMath::Vector3(0.65f, 0.3f)),
											"�����"));

	gui->GetWindow("creditsMenu")->AddTextBox(new GUI::TextBox(lucidaConsole, 
											0.25f,
											gui->GetWindow("creditsMenu")->GetBox(),
											GUI::Box(VectorMath::Vector3(0.05f, 0.3f), VectorMath::Vector3(0.65f, 0.325f)),
											"��������: ",
											GUI::LEFT));
	gui->GetWindow("creditsMenu")->AddTextBox(new GUI::TextBox(lucidaConsole, 
											0.25f,
											gui->GetWindow("creditsMenu")->GetBox(),
											GUI::Box(VectorMath::Vector3(0.05f, 0.3f), VectorMath::Vector3(0.65f, 0.325f)),
											"WASD ��� ���������",
											GUI::RIGHT));

	gui->GetWindow("creditsMenu")->AddTextBox(new GUI::TextBox(lucidaConsole, 
											0.25f,
											gui->GetWindow("creditsMenu")->GetBox(),
											GUI::Box(VectorMath::Vector3(0.05f, 0.35f), VectorMath::Vector3(0.65f, 0.375f)),
											"��������:",
											GUI::LEFT));	
	gui->GetWindow("creditsMenu")->AddTextBox(new GUI::TextBox(lucidaConsole, 
											0.25f,
											gui->GetWindow("creditsMenu")->GetBox(),
											GUI::Box(VectorMath::Vector3(0.05f, 0.35f), VectorMath::Vector3(0.65f, 0.375f)),
											"�������� �� �������",
											GUI::RIGHT));

	gui->GetWindow("creditsMenu")->AddTextBox(new GUI::TextBox(lucidaConsole, 
											0.25f,
											gui->GetWindow("creditsMenu")->GetBox(),
											GUI::Box(VectorMath::Vector3(0.05f, 0.4f), VectorMath::Vector3(0.65f, 0.425f)),
											"������������:",
											GUI::LEFT));	
	gui->GetWindow("creditsMenu")->AddTextBox(new GUI::TextBox(lucidaConsole, 
											0.25f,
											gui->GetWindow("creditsMenu")->GetBox(),
											GUI::Box(VectorMath::Vector3(0.05f, 0.4f), VectorMath::Vector3(0.65f, 0.425f)),
											"R",
											GUI::RIGHT));

	gui->GetWindow("creditsMenu")->AddTextBox(new GUI::TextBox(lucidaConsole, 
											0.25f,
											gui->GetWindow("creditsMenu")->GetBox(),
											GUI::Box(VectorMath::Vector3(0.05f, 0.45f), VectorMath::Vector3(0.65f, 0.475f)),
											"�����/����:",
											GUI::LEFT));	
	gui->GetWindow("creditsMenu")->AddTextBox(new GUI::TextBox(lucidaConsole, 
											0.25f,
											gui->GetWindow("creditsMenu")->GetBox(),
											GUI::Box(VectorMath::Vector3(0.05f, 0.45f), VectorMath::Vector3(0.65f, 0.475f)),
											"ESC",
											GUI::RIGHT));

	gui->GetWindow("creditsMenu")->AddTextBox(new GUI::TextBox(lucidaConsole, 
											0.25f,
											gui->GetWindow("creditsMenu")->GetBox(),
											GUI::Box(VectorMath::Vector3(0.05f, 0.5f), VectorMath::Vector3(0.65f, 0.525f)),
											"����� �� �������",
											GUI::LEFT));	
	gui->GetWindow("creditsMenu")->AddTextBox(new GUI::TextBox(lucidaConsole, 
											0.25f,
											gui->GetWindow("creditsMenu")->GetBox(),
											GUI::Box(VectorMath::Vector3(0.05f, 0.5f), VectorMath::Vector3(0.65f, 0.525f)),
											"C",
											GUI::RIGHT));



}

void WestMoonGUI::InitLoadGameMenu()
{
	//loadGameMenu = 
	gui->AddWindow(new GUI::Window("loadGameMenu", gui->box, Vector3(.0f, .0f), Vector3(1.0f, 1.0f), textureMenu));
	gui->GetWindow("loadGameMenu")->ReserveButtons(1);
	gui->GetWindow("loadGameMenu")->SetTitleBox(new GUI::TextBox(lucidaConsole, 
										0.5f,
										gui->GetWindow("loadGameMenu")->GetBox(), 
										GUI::Box(VectorMath::Vector3(0.0f, 0.0f), VectorMath::Vector3(0.6f, 0.2f)),
										"������ ����"));

	gui->GetWindow("loadGameMenu")->AddButton(new GUI::Button("Back", lucidaConsole, 
										0.5f,
										gui->GetWindow("loadGameMenu")->GetBox(), 
										GUI::Box(VectorMath::Vector3(0.0f, 0.85f), VectorMath::Vector3(0.1f, 0.9f)),
										"<"));
}

void WestMoonGUI::InitSaveGameMenu()
{
	//saveGameMenu = 
	gui->AddWindow(new GUI::Window("saveGameMenu", gui->box, Vector3(.0f, .0f), Vector3(1.0f, 1.0f), textureMenu));
	gui->GetWindow("saveGameMenu")->ReserveButtons(1);
	gui->GetWindow("saveGameMenu")->SetTitleBox(new GUI::TextBox(lucidaConsole, 
										0.5f,
										gui->GetWindow("saveGameMenu")->GetBox(), 
										GUI::Box(VectorMath::Vector3(0.0f, 0.0f), VectorMath::Vector3(0.6f, 0.2f)),
										"������ ����"));
	gui->GetWindow("saveGameMenu")->AddButton(new GUI::Button("Back", lucidaConsole, 
										0.5f,
										gui->GetWindow("saveGameMenu")->GetBox(), 
										GUI::Box(VectorMath::Vector3(0.0f, 0.85f), VectorMath::Vector3(0.1f, 0.9f)),
										"<"));
}

void WestMoonGUI::SaveSytemTime(const char* filename)
{
	fstream file;
	file.open(filename, ios::out);
	SYSTEMTIME systemTime;
	GetSystemTime(&systemTime);
	file << systemTime.wDay << "/" << systemTime.wMonth << "/" << systemTime.wYear << " at " 
		<< (systemTime.wHour + 2) % 24									// Convert the current time into GMT + 2.0h  (Bulgarian time)
		<< ":" << systemTime.wMinute << ":" << systemTime.wSecond << endl;	

	file.close();
}

void WestMoonGUI::LoadSavesButtons()
{
	fstream file;
	SYSTEMTIME systemTime;
	char lastModified[128];

	for(int i = 0; i < numberOfSaves; i++)
	{
		file.open(saveInfoFilenames[i], ios::in | ios::binary);
		if(file.is_open())
		{
			file.getline(lastModified, 128);

			saves[i] = new GUI::Button("saveRecord", lucidaConsole, 
										0.4f, 
										gui->GetWindow("loadGameMenu")->GetBox(),
										GUI::Box(Vector3(0.1f, 0.2f + i*0.06f), Vector3(0.7f, 0.24f + i*0.06f)),
										lastModified,
										GUI::LEFT);
			isSaveEmpty[i] = false;
		}
		else
		{
			saves[i] = new GUI::Button("saveRecord", lucidaConsole, 
										0.4f,
										gui->GetWindow("loadGameMenu")->GetBox(), 
										GUI::Box(Vector3(0.1f, 0.2f + i*0.06f), Vector3(0.7f, 0.24f + i*0.06f)),
										"������",
										GUI::LEFT);
			isSaveEmpty[i] = true;
		}

		file.close();
	}
}

void WestMoonGUI::DeleteSavesButtons()
{
	for(int i = 0; i < 8; i++)
		delete saves[i];
}

void WestMoonGUI::InitFromFile()
{
	//fstream actLog("activityLog.txt", ios::out);
	fstream file;
	file.open("Data/WestMoon.rci", ios::in);
	if(reCoIl::GoToBlock(&file, "GUI") && reCoIl::GoToBlock(&file, "INIT"))
	{
		char currOperator[256];
		while(file.good())
		{
			strcpy(currOperator, reCoIl::GetNextOperator(&file));
			if(strcmp(currOperator, "WINDOW_NUM") == 0)
			{
				int windowNum;
				file >> windowNum;
				gui->ReserveWindows(windowNum);
			}
			else if(strcmp(currOperator, "ADDMENU") == 0)
			{
				char size[64];
				char windowName[64];
				file >> windowName >> size;
				if(!strcmp(size, "big"))
					gui->AddWindow(new GUI::Window(windowName, gui->box, Vector3(.0f, .0f), Vector3(1.0f, 1.0f), textureMenuBig));
				else if(!strcmp(size, "small"))
					gui->AddWindow(new GUI::Window(windowName, gui->box, Vector3(.0f, .0f), Vector3(1.0f, 1.0f), textureMenu));

				//actLog << "ADDMENU " << windowName << endl;
			}
			else if(strcmp(currOperator, "ADDBUTTON") == 0)
			{
				char parentWindowName[64], buttonName[64], buttonTextValue[64];
				GUI::Box position;
				file >> parentWindowName >> buttonName >> buttonTextValue;
				file >> position.upperLeft.x >> position.upperLeft.y >> position.lowerRight.x >> position.lowerRight.y;

				
				gui->GetWindow(parentWindowName)->AddButton(new GUI::Button(buttonName, lucidaConsole, 
										0.5f,
										gui->GetWindow(parentWindowName)->GetBox(), 
										position,
										buttonTextValue));

				//actLog << "ADDBUTTON " << parentWindowName << " " << buttonName << " " << buttonTextValue << endl;
				//actLog << position.upperLeft.x << " " << position.upperLeft.y << " " << position.lowerRight.x << " " << position.lowerRight.y << endl;
			}
			else if(strcmp(currOperator, "ADDTEXTBOX") == 0)
			{
				char parentWindowName[64], textboxTextValue[64];
				GUI::Box position;
				float size;
				int aligment;

				file >> parentWindowName >> textboxTextValue;
				file >> size;
				file >> position.upperLeft.x >> position.upperLeft.y >> position.lowerRight.x >> position.lowerRight.y;
				file >> aligment;

				
				gui->GetWindow(parentWindowName)->AddTextBox(new GUI::TextBox(lucidaConsole, 
										size,
										gui->GetWindow(parentWindowName)->GetBox(), 
										position,
										textboxTextValue, 
										(GUI::Aligment)aligment));

				//actLog << "ADDTEXTBOX " << parentWindowName << " " << textboxTextValue << endl;
			}
			else if(strcmp(currOperator, "RESERVEBUTTONS") == 0)
			{
				char menuName[64];
				int numButtons;
				file >> menuName >> numButtons;
				gui->GetWindow(menuName)->ReserveButtons(numButtons);
			}
			else if(strcmp(currOperator, "RESERVETEXTBOXES") == 0)
			{
				char menuName[64];
				int numTextBoxes;
				file >> menuName >> numTextBoxes;
				gui->GetWindow(menuName)->ReserveTextBoxes(numTextBoxes);
			}
			else if(strcmp(currOperator, "SETMENU") == 0)
			{
				char menuName[64];
				file >> menuName;
				currWindow = gui->GetWindow(menuName);
			}
			else if(strcmp(currOperator, "END") == 0)
			{
				break;
			}
			else
			{
				ThrowError(L"Unknown operator used in GUI initialization");
			}
		}
	}

}


bool WestMoonGUI::InitGUI(Vector3 guiDimensions)
{
	Texture2DManager *texMgr;
	texMgr = Texture2DManager::getInstance();

	textureMenu = texMgr->load("Data/Interface/menu_background.tga");
	textureMenuBig = texMgr->load("Data/Interface/menu_background_more_info.tga");
	lucidaConsole = new GUI::BitmapFont(texMgr->load("Data/Fonts/LucidaConsole2.tga"));

	gui = new GUI::GUI(-guiDimensions.x, guiDimensions.y, guiDimensions.z, 0.4f);

	this->InitFromFile();
	
	/*
	gui->ReserveWindows(8);

	this->InitGameMenu();
	this->InitByeWindow();
	this->InitLoadingWindow();
	this->InitObjectiveWindow();
	this->InitMainMenu();
	this->InitCreditsMenu();
	this->InitLoadGameMenu();
	this->InitSaveGameMenu();
	
	currWindow = gui->GetWindow("gameMenu");
	*/
	
	pauseMode = true;
	ShowPointer();
	
	messageHandler = new GUI::GameMessageHandler(gui->box, lucidaConsole, 0.2f, 0.5f);

	return true;			//Everything is OK
}

void WestMoonGUI::UpdateGUI()
	{
		if(currWindow == gui->GetWindow("byeWindow"))
		{
			Sleep(600);
			exit(0);
		}
		else if(currWindow == gui->GetWindow("loadingWindow"))
		{
			currLevel = new Level01(messageHandler, gui);
			pauseMode = false;
			HidePointer();
			currCamera = currLevel->GetPlayerCamera(Player::THIRD_PERSON);
		}

		if(keyboard.GetButtonState(VK_ESCAPE) && currLevel != NULL)			// If ESC is pressed and there is a level loaded
		{
			pauseMode = false;
			HidePointer();
			currCamera = currLevel->GetPlayerCamera(Player::THIRD_PERSON);
		}

		bool isButtonPressed = currWindow->IsAnyButtonPressed();

		if(currWindow == gui->GetWindow("loadGameMenu"))
			for(int i = 0; i < numberOfSaves; i++)
				isButtonPressed = isButtonPressed || (saves[i]->IsPressed() && !isSaveEmpty[i]);
		else if(currWindow == gui->GetWindow("saveGameMenu"))
		{
			for(int i = 0; i < numberOfSaves; i++)
				isButtonPressed = isButtonPressed || saves[i]->IsPressed();
		}

		if(isButtonPressed)
		{
			mouse.ReleaseLeftButton();						// "One mouse click - one input". Else we obtain a lot of inputs if we hold the mouse button

			if(!PlaySound(TEXT("Data/Sounds/MachineGun.wav"), 0, 1))
				ThrowError(TEXT("Cannot play MachineGun.wav"));

			if(currWindow == gui->GetWindow("gameMenu"))
			{	
				if(gui->GetWindow("gameMenu")->IsButtonPressed("StartGame")) currWindow = gui->GetWindow("loadingWindow");
				
				if(gui->GetWindow("gameMenu")->IsButtonPressed("LoadGame"))
				{
					LoadSavesButtons();
					currWindow = gui->GetWindow("loadGameMenu");
				}
				
				if(gui->GetWindow("gameMenu")->IsButtonPressed("Credits")) currWindow = gui->GetWindow("creditsMenu");
				if(gui->GetWindow("gameMenu")->IsButtonPressed("Exit")) currWindow = gui->GetWindow("byeWindow");
			}
			else if(currWindow == gui->GetWindow("mainMenu"))
			{
				if(gui->GetWindow("mainMenu")->IsButtonPressed("Resume"))
				{
					pauseMode = false;
					HidePointer();
					currCamera = currLevel->GetPlayerCamera(Player::THIRD_PERSON);
				}

				if(gui->GetWindow("mainMenu")->IsButtonPressed("SaveGame"))
				{
					LoadSavesButtons();
					currWindow = gui->GetWindow("saveGameMenu");
				}

				if(gui->GetWindow("mainMenu")->IsButtonPressed("LoadGame"))
				{
					LoadSavesButtons();
					currWindow = gui->GetWindow("loadGameMenu");
				}

				if(gui->GetWindow("mainMenu")->IsButtonPressed("Exit")) currWindow = gui->GetWindow("byeWindow");
				if(gui->GetWindow("mainMenu")->IsButtonPressed("Objectives")) currWindow = gui->GetWindow("objectiveMenu");
				if(gui->GetWindow("mainMenu")->IsButtonPressed("Credits")) currWindow = gui->GetWindow("creditsMenu");
			}
			else if(currWindow == gui->GetWindow("creditsMenu"))
			{
				if(gui->GetWindow("creditsMenu")->IsButtonPressed("Back"))				
					if(currLevel == NULL) currWindow = gui->GetWindow("gameMenu");
					else currWindow = gui->GetWindow("mainMenu");
			}
			else if(currWindow == gui->GetWindow("objectiveMenu"))
			{
				if(gui->GetWindow("objectiveMenu")->IsButtonPressed("Back"))
					currWindow = gui->GetWindow("mainMenu");
			}
			else if(currWindow == gui->GetWindow("loadGameMenu"))
			{
				for(int i = 0; i < numberOfSaves; i++)
					if(saves[i]->IsPressed())
					{
						currLevel = new Level01(messageHandler, gui);
						currLevel->LoadFromFile(saveFilenames[i]);
						pauseMode = false;
						HidePointer();
						break;
					}

				if(gui->GetWindow("loadGameMenu")->IsButtonPressed("Back"))
				{
					DeleteSavesButtons();
					if(currLevel == NULL) currWindow = gui->GetWindow("gameMenu");
					else currWindow = gui->GetWindow("mainMenu");
				}
			}
			else if(currWindow == gui->GetWindow("saveGameMenu"))
			{
				for(int i = 0; i < numberOfSaves; i++)
					if(saves[i]->IsPressed())
					{
						currLevel->SaveToFile(saveFilenames[i]);
						SaveSytemTime(saveInfoFilenames[i]);
					
						DeleteSavesButtons();						// Refresh the saves list
						LoadSavesButtons();

						break;	
					}

				if(gui->GetWindow("saveGameMenu")->IsButtonPressed("Back"))
				{
					DeleteSavesButtons();
					currWindow = gui->GetWindow("mainMenu");
				}
			}
		}

	
		currWindow->Update(&mouse);
		if(currWindow == gui->GetWindow("loadGameMenu") || currWindow == gui->GetWindow("saveGameMenu"))
		{
			for(int i = 0; i < numberOfSaves; i++)
				saves[i]->Update(&mouse);
		}
	}