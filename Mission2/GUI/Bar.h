
#ifndef BAR_H
#define BAR_H

#ifdef GUI_EXPORTS
#define GUI_API __declspec(dllexport)
#else
#define GUI_API __declspec(dllimport)
#endif

#include "Box.h"
#include "../Md2Model/Texture.h"

using namespace VectorMath;

namespace GUI
{
	class GUI_API Bar
	{
	private:
		Box box;													// Box where the bar is situated
		Texture2D* textureFull;										// The skin of the bar when is full
		Texture2D* textureEmpty;									// The skin of the bar when is empty
		Texture2D* iconTexture;
		
		Box window;
		Box iconBox;												// Bar's icon location
		Box full, empty;											// The box of the full and empty part of the bar

		float minValue, maxValue;									// min and max of the value which the bar represents
		float currValue;											// the value which the bar represents in case of INTERNAL_VALUE
		float *currValuep;											// pointer to the value in case of EXTERNAL_VALUE

		float percent;												// procent of "fullness" of the bar - 0 = empty; 100 = full

		Vector3 color;												// color in wich the bar will be drawn

		enum Type {EXTERNAL_VALUE, INTERNAL_VALUE} type;			// is the bar is going to use a pointer to an external variable or internal variable


	public:
		Bar(Box window, Box box, Texture2D *textureFull, Texture2D *textureEmpty, 
			Vector3 color = Vector3(1.0f, 1.0f, 1.0f), 
			float minValue = 0.0f, float maxValue = 100.0f);

		void SetIcon(Texture2D *iconTexure, Box iconBox);

		void SetPCurrValue(float *currValuep);
		void DeletePCurrValue();

		float GetValue();
		float GetPercent() { return this->percent; }
		void SetCurrValue(float currValue);
	
		float GetCurrValue() { return this->currValue; }
		void IncreaseCurrValue(float value);

		void SetColor(Vector3 color) { this->color = color; }
		Vector3 GetColor() { return this->color; }

		void Update();

		void Draw(Vector3 color);									// Draw the bar in custom color
		void Draw();												// Draw the bar in the defalut color defined in this class

	private:
		void DrawFullPart();
		void DrawEmptyPart();
	};
}

#endif