
#include "stdafx.h"
#include "TextBox.h"

namespace GUI
{
	TextBox::TextBox(BitmapFont* font, float textSize, Box window, Box box, char textValue[MAX_TEXT_SIZE], Aligment al)	
	{	
		this->font = font;
		this->textSize = textSize;
		strcpy_s(this->textValue, textValue);
		
		this->box = CalculatePercent(window, box);
		
		if(al == CENTERED)
		{
			this->box.upperLeft.y = (this->box.upperLeft.y + this->box.lowerRight.y + this->textSize)/2;			// Calculate center aligment
			this->box.upperLeft.x = (this->box.upperLeft.x + this->box.lowerRight.x - this->textSize*(strlen(this->textValue)))/2;
		}
		else if(al == LEFT)
		{
			this->box.upperLeft.y = (this->box.upperLeft.y + this->box.lowerRight.y + this->textSize)/2;			// Calculate left aligment
		}
		else if(al == RIGHT)
		{
			this->box.upperLeft.y = (this->box.upperLeft.y + this->box.lowerRight.y + this->textSize)/2;			// Calculate right aligment
			this->box.upperLeft.x = this->box.lowerRight.x - this->textSize*strlen(this->textValue);	
		}
		
		this->box.lowerRight = this->box.upperLeft + VectorMath::Vector3(this->textSize*(strlen(this->textValue)), -this->textSize);
		this->lowerLeft = this->box.upperLeft+VectorMath::Vector3(0.0f, -this->textSize);

		this->color = VectorMath::Vector3(1.0f, 1.0f, 1.0f);
	}

	void TextBox::Draw(VectorMath::Vector3 color)						// draw the textbox in custom color
	{
		glColor3f(color.x, color.y, color.z);
		font->Printf(this->textSize, this->lowerLeft, textValue);
	}
		
	void TextBox::Draw()												// draw the textbox in default color
	{
		glColor3f(this->color.x, this->color.y, this->color.z);
		font->Printf(this->textSize, this->lowerLeft, textValue);
	}
}