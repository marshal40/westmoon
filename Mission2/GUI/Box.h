#ifndef BOX_H
#define BOX_H

#ifdef GUI_EXPORTS
#define GUI_API __declspec(dllexport)
#else
#define GUI_API __declspec(dllimport)
#endif

#include "../Geometry/Geometry.h"

using namespace VectorMath;

namespace GUI
{
	struct GUI_API Box
	{
	public:
		Vector3 upperLeft, lowerRight;						// coordinates of the upper left and the lower right corner of the box

		Box() {}											// default constructor
		Box(Vector3 upperLeft, Vector3 lowerRight)
		{
			this->upperLeft = upperLeft;
			this->lowerRight = lowerRight;
		}

		bool Contain(Vector3 point);						// is point is in the box
		Vector3 GetCenter();
		Vector3 GetDimensions();
		void Draw(float rotation_degs = 0.0f);
	};

	GUI_API inline Vector3 CalculatePercent(Box box, VectorMath::Vector3 procent);	// returns the percent in which a point divides a box
	GUI_API Box CalculatePercent(Box bigBox, Box littleBox);	// returns the box of the percents in which the vertices of a box divides another box
}

#endif