
#include "stdafx.h"
#include "Window.h"

#include <cstring>

namespace GUI
{
	Window::Window(char* name, Box gui, 
		VectorMath::Vector3 procentUpperLeft, 
		VectorMath::Vector3 procentLowerRight,
		Texture2D* skin)
	{
		strcpy(this->name, name);
		this->box = CalculatePercent(gui, Box(procentUpperLeft, procentLowerRight));
			
		this->skin = skin;

		this->mouseOverAButton = false;
		this->buttonPressed = false;

		this->titleBox = NULL;
	}
		
	void Window::ReserveButtons(int buttons)			// Declare the number of the button
	{
		this->buttons.DeclareElements(buttons);
	}

	void Window::ReserveTextBoxes(int textBoxes)		// Declare the number of the textboxes
	{
		this->textBoxes.DeclareElements(textBoxes);
	}

	void Window::Update(Mouse *mouse)
	{
		this->mouseOverAButton = false;
		this->buttonPressed = false;

		for(unsigned i = 0; i < this->buttons.GetNumElements(); i++)
			this->buttons[i]->Update(mouse);

		for(unsigned i = 0; i < this->buttons.GetNumElements(); i++)
		{
			if(this->buttons[i]->IsMouseOver()) this->mouseOverAButton = true;
			if(this->buttons[i]->IsPressed()) this->buttonPressed = true;
		}
	}
		
	void Window::Draw()														
	{
		glPushMatrix();
		this->skin->bind();	
	
		glBegin(GL_QUADS);
		glColor3f(1.0f, 1.0f, 1.0f);
		glTexCoord2i(0, 0);
		glVertex3f(this->box.upperLeft.x, 
			this->box.lowerRight.y, 
			this->box.upperLeft.z);

		glTexCoord2i(1, 0);
		glVertex3f(this->box.lowerRight.x, 
			this->box.lowerRight.y, 
			this->box.lowerRight.z);

		glTexCoord2i(1, 1);
		glVertex3f(this->box.lowerRight.x, 
			this->box.upperLeft.y, 
			this->box.lowerRight.z);

		glTexCoord2i(0, 1);
		glVertex3f(this->box.upperLeft.x, 
			this->box.upperLeft.y, 
			this->box.upperLeft.z);
		
		glEnd();
			
		for(unsigned i = 0; i < this->buttons.GetNumElements(); i++)
			this->buttons[i]->Draw();

		for(unsigned i = 0; i < this->textBoxes.GetNumElements(); i++)
			this->textBoxes[i]->Draw();

		if(this->titleBox != NULL)
			this->titleBox->Draw();
			
		glPopMatrix();
	}
	
	Button* Window::AddButton(Button *newButton)
	{
		if(this->buttons.PushElement(newButton))
			return this->buttons[this->buttons.GetNumElements()-1];
		else return NULL;
	}

	TextBox* Window::AddTextBox(TextBox *newTextBox)
	{
		if(this->textBoxes.PushElement(newTextBox))
			return this->textBoxes[this->textBoxes.GetNumElements()-1];
		else return NULL;
	}

	void Window::SetTitleBox(TextBox *titleBox) { this->titleBox = titleBox; }

	void Window::RemoveTitleBox()									// Remove the window's title
	{ 
		delete this->titleBox; 
		this->titleBox = NULL;
	}

	void Window::RemoveButton(char *buttonName)						// Remove a button
	{
		for(unsigned i = 0; i < this->buttons.GetNumElements(); i++)
			if(strcmp(buttonName, this->buttons[i]->GetName())==0)		// find the button in the vector
				this->buttons.EraseElement(i);		// and delete it
	}

	/*
	void Window::RemoveTextBox(char *textboxName)					// Remove a textbox
	{
		for(unsigned i = 0; i < this->textBoxes.GetNumElements(); i++)
			if(strcmp(textboxName, this->textBoxes[i]->Get)						// find the textbox in the vector
				this->textBoxes.EraseElement(i);	// and delete it
	}
	*/

	bool Window::IsMouseOverButton(char* buttonName)
	{
		for(unsigned i = 0; i < this->buttons.GetNumElements(); i++)			
			if(strcmp(this->buttons[i]->GetName(), buttonName) == 0)			// find the button with specified name in the vector
				return this->buttons[i]->IsMouseOver();
		return false;														// if the button is not find return false
	}

	bool Window::IsButtonPressed(char* buttonName)
	{
		for(unsigned i = 0; i < this->buttons.GetNumElements(); i++)			
			if(strcmp(this->buttons[i]->GetName(), buttonName) == 0)			// find the button with specified name in the vector
				return this->buttons[i]->IsPressed();
		return false;														// if the button is not find return false
	}
}