// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the GUI_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// GUI_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.

#ifndef GUI_H
#define GUI_H

#ifdef GUI_EXPORTS
#define GUI_API __declspec(dllexport)
#else
#define GUI_API __declspec(dllimport)
#endif

/*  Scheme

   --Box--
		^--GUI--
			^--Window--
		^		^--Button--
					 ^--TextBox--
	--Pointer--
		^--TargetPointer--

	--GameMessage--
		^--GameMessageHandler--
*/

#include <cmath>
#include <vector>
#include <gl/glew.h>
#include "../Geometry/Geometry.h"
#include "../Mouse/Mouse.h"
#include "../SmartArray/SmartArray.h"

#include "Box.h"
#include "GameMessage.h"
#include "Window.h"
#include "Pointer.h"
#include "TargetPointer.h"
#include "Bar.h"
#include "CompassPoint.h"
#include "Compass.h"

#include "../Md2Model/Texture.h"
#include "../Md2Model/TextureManager.h"
#include "../Md2Model/Image.h"

namespace GUI
{
	class GUI_API GUI
	{
	private:
		SmartArray<Window*> windows;
	public:
		Box box;						// GUI Space

		GUI(float posX, float posY, float posZ, float depth);
		void ReserveWindows(int numWindows);
		Window* AddWindow(Window* window);
		void Update(Mouse *mouse);
		void Draw();
		
		Window* GetWindow(char* name);
	};
}

#endif