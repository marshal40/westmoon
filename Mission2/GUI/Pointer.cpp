
#include "stdafx.h"
#include "Pointer.h"

namespace GUI
{
	Pointer::Pointer(Box window, Vector3 position, Texture2D* skin, float size = 0.03f)
	{
		this->size = abs(CalculatePercent(window, Vector3(size, size)).x - window.upperLeft.x);			// calculate the real coordinated of the pointer
		this->skin = skin;
		this->SetPosition(window, position);
		this->targetPosition = Vector3();
	}

	void Pointer::SetPosition(Box window, Vector3 position)
	{
		if(Box(Vector3(0.0f, 0.0f), Vector3(1.0f, 1.0f)).Contain(position))
		{
			this->position = position;
			this->box = Box(CalculatePercent(window, position) - Vector3(this->size, -this->size),		// calculate the real coordinated of the pointer
							CalculatePercent(window, position) + Vector3(this->size, -this->size));
		}
	}

	Vector3 Pointer::GetScreenPosition(int screenX, int screenY)
	{ 
		return CalculatePercent(Box(Vector3(0.0f, 0.0f), Vector3((float)screenX, (float)screenY)), this->position);
	}

	void Pointer::Draw()
	{
		glPushMatrix();
		this->skin->bind();	
	
		glBegin(GL_QUADS);
		glColor3f(1.0f, 1.0f, 1.0f);
		glTexCoord2i(0, 0);
		glVertex3f(this->box.upperLeft.x, 
			this->box.lowerRight.y, 
			this->box.upperLeft.z);

		glTexCoord2i(1, 0);
		glVertex3f(this->box.lowerRight.x, 
			this->box.lowerRight.y, 
			this->box.lowerRight.z);

		glTexCoord2i(1, 1);
		glVertex3f(this->box.lowerRight.x, 
			this->box.upperLeft.y, 
			this->box.lowerRight.z);

		glTexCoord2i(0, 1);
		glVertex3f(this->box.upperLeft.x, 
			this->box.upperLeft.y, 
			this->box.upperLeft.z);
		
		glEnd();
		glPopMatrix();
	}
}