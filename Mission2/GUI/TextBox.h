
#ifndef TEXTBOX_H
#define TEXTBOX_H

#ifdef GUI_EXPORTS
#define GUI_API __declspec(dllexport)
#else
#define GUI_API __declspec(dllimport)
#endif

#include <string.h>
#include <math.h>
#include "BitmapFont.h"
#include "Box.h"
#include "../Geometry/Geometry.h"

namespace GUI
{
	#define MAX_TEXT_SIZE 256

	enum Aligment { LEFT, RIGHT, CENTERED };

	class GUI_API TextBox
	{
	private:
		BitmapFont* font;													// font of the text
		Box box;															// location of the textbox in OGL coordinateds
		VectorMath::Vector3 lowerLeft;
		float textSize;														// size of the text
		char textValue[MAX_TEXT_SIZE];										// text value

		VectorMath::Vector3 color;											// The color of the text

	public:
		TextBox(BitmapFont* font, float textSize, Box window, Box box, char textValue[MAX_TEXT_SIZE], Aligment al = CENTERED);
		void Draw(VectorMath::Vector3 color);								// draw the textbox in custom color
		void Draw();														// draw the textbox in the default color

		void SetColor(VectorMath::Vector3 color) { this->color = color; }	// set default color for this box
		Box GetBox() { return this->box; }
	};
}

#endif