
#ifndef BITMAPFONT_H
#define BITMAPFONT_H

#ifdef GUI_EXPORTS
#define GUI_API __declspec(dllexport)
#else
#define GUI_API __declspec(dllimport)
#endif

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "../Geometry/Geometry.h"
#include "../Md2Model/Texture.h"
#include "../Md2Model/TextureManager.h"

namespace GUI
{
	const float letterRange = 1/16.0f;

	class GUI_API BitmapFont
	{
	private:
		Texture2D* texRegular;																// texture of the 16x16 bitmap of the font
		unsigned regular;																	// first of 16x16 = 256 display lists
		
	public:
		static enum Symbol { TICK = 16 , BULLET = 164};										// some special symbols
		BitmapFont(Texture2D* texRegular);
		~BitmapFont();
		void PrintChar(char ch);															// prints single character 
		void Printf(float size, VectorMath::Vector3 position, char* formated, ...);			// prints a formated string
	};
}


#endif BITMAPFONT_H