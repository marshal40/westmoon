
#include "stdafx.h"
#include "BitmapFont.h"

#include <fstream>
using namespace std;


namespace GUI
{
	BitmapFont::BitmapFont(Texture2D* texRegular)
	{
		this->texRegular  = texRegular;
		this->regular = glGenLists(256);

		for(unsigned i = 0; i < 256; i++)
		{
			glBegin(GL_QUADS);

			glNewList(this->regular + i, GL_COMPILE);			//Compile the display lists
			glColor3f(1.0f, 1.0f, 1.0f);
			glTexCoord2d(0, 0); glVertex3i(0, 0, 0);
			glTexCoord2d(0, 1); glVertex3i(0, 1, 0);
			glTexCoord2d(1, 1); glVertex3i(1, 1, 0);
			glTexCoord2d(1, 0); glVertex3i(1, 0, 0);
				
			glEndList();

			glEnd();
		}
	}

	BitmapFont::~BitmapFont()
	{
		glDeleteLists(regular, 256);
	}

	void BitmapFont::PrintChar(char ch)
	{
		//fstream file("chars", ios::out | ios::app);
		//file << ch << " ";
		//file.close();
		ch = ch % 256;
		if(ch >= '�' && ch <= '�') 
		{
			if(ch)
				ch = ch;
		}
		unsigned char mChar;
		if (ch >= 0) mChar = ch;
		else mChar = 256 + ch; 
		//if(ch < 0) ch = 256 - ch;
		
		/*
		if(ch >= '�' && ch <= '�') ch = 127; //ch += 192 - 'A' + 129;
		if(ch >= '�' && ch <= '�') 
		{
			//if(ch)
			//	ch = ch;
			//ch += 224 - '�';// + 129;
		}
		*/
		float y = 1 - letterRange*(int)(mChar/16),										// column and
			x = letterRange*(int)(mChar%16);											// row of the character in the map

		glTexCoord2d(x				, y				 ); glVertex3i(0, 1, 0);		// Render the character
		glTexCoord2d(x 				, y	- letterRange); glVertex3i(0, 0, 0);
		glTexCoord2d(x + letterRange, y - letterRange); glVertex3i(1, 0, 0);
		glTexCoord2d(x + letterRange, y 			 ); glVertex3i(1, 1, 0);
	}

	void BitmapFont::Printf(float size, VectorMath::Vector3 position, char* formated, ...)
	{
		if(formated == NULL) return;						// stop if there is nothing to print
			
		char text[256];
		va_list ap;

		va_start(ap, formated);	
			vsprintf_s(text, formated, ap);					// Get the formated string
		va_end(ap);


		glEnable(GL_TEXTURE_2D);
		this->texRegular->bind();

		glPushMatrix();
		glTranslatef(position.x, position.y, position.z);
			
		for(unsigned i = 0; i < strlen(text); i++)			//Optimization
		{			
			glPushMatrix();
			glTranslatef(size*i, 0.0f, 0.0f);				// translate to the current's character position
			glScalef(size, size, size);						// Scale to the specific size
			glBegin(GL_QUADS);				
			this->PrintChar(text[i]);						// Print the current character
			glEnd();
			glPopMatrix();
		}
			
		glPopMatrix();
	}
}