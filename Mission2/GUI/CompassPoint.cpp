
#include "stdafx.h"
#include "CompassPoint.h"
#include <cstring>

namespace GUI
{
	CompassPoint::CompassPoint(char* name, Vector3 *worldPosition)
	{
		strcpy_s(this->name, name);
		this->worldPosition = worldPosition;
	}
}