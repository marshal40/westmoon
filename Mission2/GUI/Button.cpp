
#include "stdafx.h"
#include "Button.h"
#include <cstring>

namespace GUI
{
	Button::Button(char* name, BitmapFont* font, float textSize, Box window, Box box, char textValue[MAX_TEXT_SIZE], Aligment al)
	{
		strcpy_s(this->name, name);
		this->label = new TextBox(font, textSize, window, box, textValue, al);
		this->location = this->label->GetBox();

		this->pressed = false;
		this->mouseOver = false;
		this->lastMouseOver = false;
	}
	
	void Button::Update(Mouse* mouse)
	{
		this->lastMouseOver = this->mouseOver;						// override the lastMouseOver

		Vector3 mouseCoords = mouse->Get2DPosition();				// get the new position of the mouse

		this->mouseOver = location.upperLeft.x < mouseCoords.x  &&	// is the mouse pointer inside the button
			mouseCoords.x < location.lowerRight.x &&
			location.upperLeft.y > mouseCoords.y && 
			mouseCoords.y > location.lowerRight.y;
			
		this->pressed = this->mouseOver && mouse->IsLeftButtonDown();	// the button is pressed when the mouse is 
																		//over the button and the left mouse button is pressed
	}

	void Button::Draw()
	{
		if(this->mouseOver)														// if the mouse is over the button
			this->label->Draw(VectorMath::Vector3(1.0f, 1.0f, 0.0f));			// draw the button in yellow 
		else
			this->label->Draw();												// else draw it in its default color
	}
}