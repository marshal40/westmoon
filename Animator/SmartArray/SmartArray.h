// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the SMARTARRAY_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// SMARTARRAY_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifndef TS_SMARTARRAY_H
#define TS_SMARTARRAY_H

#ifdef SMARTARRAY_EXPORTS
#define SMARTARRAY_API __declspec(dllexport)
#else
#define SMARTARRAY_API __declspec(dllimport)
#endif

template <class T>
class SMARTARRAY_API SmartArray
{
private:
	unsigned numElements;
	T* elements;
	unsigned nextElementIndex;

public:
	SmartArray();
	~SmartArray();
	void DeleteArray();
	void DeclareElements(unsigned numberOfElements);
	bool PushElement(T element);
	void EraseElement(unsigned elementIndex);
	unsigned GetNumElements() const;
	unsigned GetCapacity() const;
	T operator[](unsigned elementIndex) const;
	T* GetElementPtr(unsigned elementIndex) const;
};

#endif

