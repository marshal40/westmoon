
#include "stdafx.h"
#include "VariablesMap.h"

namespace reCoIl
{
	void* VariablesMap::GetVariable(string name)
	{
		for(unsigned i = 0; i < this->vars.size(); i++)
		{
			if(name.compare(this->vars[i]->name) == 0) return this->vars[i]->object;
		}
	}

	void VariablesMap::SetVariable(Variable *var)
	{
		this->vars.push_back(var);
	}
}