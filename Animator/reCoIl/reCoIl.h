// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the RECOIL_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// RECOIL_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef RECOIL_EXPORTS
#define RECOIL_API __declspec(dllexport)
#else
#define RECOIL_API __declspec(dllimport)
#endif

#include "Variable.h"
#include "VariablesMap.h"

#include <fstream>
#include <sstream>
using namespace std;

namespace reCoIl
{
	/*
	class reCoIl
	{
	public:
		static VariablesMap vars;
		
		static void ReadDefinition(fstream file);
		
	}
	*/

	RECOIL_API void ReadCode(char* filename);

	RECOIL_API bool FindOperator(fstream *file, char* _operator);
	RECOIL_API bool FindOperator(stringstream *code, char* _operator);

	RECOIL_API bool FindOperatorInThisBlock(fstream *file, char* _operator);
	RECOIL_API bool FindOperatorInThisBlock(stringstream *code, char* _operator);

	RECOIL_API bool IsNextOperator(fstream *file, char* _operator);
	RECOIL_API bool IsNextOperator(stringstream *code, char* _operator);

	RECOIL_API char* GetNextOperator(fstream *file);
	RECOIL_API char* GetNextOperator(stringstream *code);

	RECOIL_API bool GoToBlock(fstream *file, char* blockName);
	RECOIL_API bool GoToBlock(stringstream *code, char* blockName);

	RECOIL_API void GoToTheEndOfThisBlock(fstream *file);
	RECOIL_API void GoToTheEndOfThisBlock(stringstream *code);
}
