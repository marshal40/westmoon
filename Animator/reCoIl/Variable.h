
#ifndef TS_RECOIL_VARIABLE_H
#define TS_RECOIL_VARIABLE_H

#ifdef RECOIL_EXPORTS
#define RECOIL_API __declspec(dllexport)
#else
#define RECOIL_API __declspec(dllimport)
#endif

#include <string>
using namespace std;

namespace reCoIl
{
	class RECOIL_API Variable
	{
	public:
		string type;
		string name;
		void* object;

		Variable(string type, string name, void* object);
		void Set(string type, string name, void* object);

	};
}
#endif