// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the GEOMETRY_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// GEOMETRY_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef GEOMETRY_EXPORTS
#define GEOMETRY_API __declspec(dllexport)
#else
#define GEOMETRY_API __declspec(dllimport)
#endif

#pragma once

#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <math.h>
#include <fstream>

#define PI 3.14159265f
#define INF 100000.0f

namespace VectorMath
{
	class GEOMETRY_API Vector3
	{
	public:
		float x, y, z;
	
		Vector3(float x = 0, float y = 0, float z = 0)
		{
			this->x = x;
			this->y = y;
			this->z = z;
		}

		Vector3 operator + (const Vector3 &other)
		{
			return Vector3(this->x + other.x,
				this->y + other.y,
				this->z + other.z);
		}

		Vector3 operator - (const Vector3 &other)
		{
			return Vector3(this->x - other.x,
				this->y - other.y,
				this->z - other.z);
		}

		Vector3 operator * (const float &coef)
		{
			return Vector3(this->x * coef,
				this->y * coef,
				this->z * coef);
		}

		Vector3 operator / (const float &coef)
		{
			return Vector3(this->x / coef,
				this->y / coef,
				this->z / coef);
		}

		void operator += (const Vector3 &other)
		{
			this->x += other.x;
			this->y += other.y;
			this->z += other.z;
		}

		void operator -= (const Vector3 &other)
		{
			this->x -= other.x;
			this->y -= other.y;
			this->z -= other.z;
		}

		bool operator == (const Vector3 &other)
		{
			return (this->x == other.x) && 
					(this->y == other.y) &&
					(this->z == other.z); 
		}

		bool operator != (const Vector3 &other)
		{
			return (this->x != other.x) || 
					(this->y != other.y) ||
					(this->z != other.z); 
		}
		
		static float DotProduct (Vector3 A, Vector3 B)
		{
			return A.x * B.x + A.y * B.y + A.z * B.z;
		}

		float GetSqLength()
		{
			return DotProduct(*this, *this);
		}

		float GetLength()
		{
			return sqrt(this->GetSqLength());
		}

		Vector3 Normal()
		{
			return (*this) / this->GetLength();
		}

		void Normalize()
		{
			(*this) = (*this) / (this->GetLength());
		}

		void RotateInZX (float angleInDegrees)
		{
			float alpha = angleInDegrees * PI/180;
			float resultZ = this->z * cos(alpha) - this->x * sin(alpha);
			float resultX = this->z * sin(alpha) + this->x * cos(alpha);
			this->z = resultZ;
			this->x = resultX;
		}

		void RotateInXY (float angleInDegrees)
		{
			float alpha = angleInDegrees * PI/180;
			float resultX = this->x * cos(alpha) - this->y * sin(alpha);
			float resultY = this->x * sin(alpha) + this->y * cos(alpha);
			this->x = resultX;
			this->y = resultY;
		}

		void RotateInYZ (float angleInDegrees)
		{
			float alpha = angleInDegrees * PI/180;
			float resultY = this->y * cos(alpha) - this->z * sin(alpha);
			float resultZ = this->y * sin(alpha) + this->z * cos(alpha);
			this->y = resultY;
			this->z = resultZ;
		}

		void RotateAroundXAxis (float angleInDegrees)
		{
			this->RotateInYZ (angleInDegrees);
		}

		void RotateAroundYAxis (float angleInDegrees)
		{
			this->RotateInZX (angleInDegrees);
		}

		void RotateAroundZAxis (float angleInDegrees)
		{
			this->RotateInXY (angleInDegrees);
		}
	};

	GEOMETRY_API float CrossProductXY (Vector3 A, Vector3 B);
	GEOMETRY_API Vector3 CrossProduct (Vector3 A, Vector3 B);
	GEOMETRY_API float DistanceFromPointToLineInXZ(Vector3 X, Vector3 Y, Vector3 P);
	GEOMETRY_API float TriangleArea (Vector3 A, Vector3 B, Vector3 C);
	GEOMETRY_API float TriangleXYArea (Vector3 A, Vector3 B, Vector3 C);
	GEOMETRY_API float GetAngle_rads (Vector3 A, Vector3 B);
	GEOMETRY_API float GetAngle_degs (Vector3 A, Vector3 B);
	GEOMETRY_API float GetProjectionLength (Vector3 A, Vector3 base);
	GEOMETRY_API Vector3 PointLineProjection (Vector3 point, Vector3 lineA, Vector3 lineB);
}

#endif
