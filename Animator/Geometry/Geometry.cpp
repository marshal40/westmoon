
#include "stdafx.h"
#include "Geometry.h"

namespace VectorMath
{
	float CrossProductXY (Vector3 A, Vector3 B)
	{
		float result = A.x * B.y - A.y * B.x;
		return result;
	}


	Vector3 CrossProduct (Vector3 A, Vector3 B)
	{
		Vector3 result;
		result.x = A.y*B.z - B.y*A.z;
		result.y = A.z*B.x - B.z*A.x;
		result.z = A.x*B.y - B.x*A.y;
		return result;
	}

	float DistanceFromPointToLineInXZ(Vector3 X, Vector3 Y, Vector3 P)
	{
		// Ax + Bz + C = 0 is the equation of the line XY
		float A = X.z - Y.z;
		float B = Y.x - X.x;
		float C = X.x * Y.z - Y.x * X.z;
		return abs((A*P.x + B*P.z + C) / sqrt(A*A + B*B));
	}
	
	float TriangleArea (Vector3 A, Vector3 B, Vector3 C)
	{
		Vector3 cpResult = CrossProduct ( B-A, C-A );
		return cpResult.GetLength() / 2;
	}

	float TriangleXYArea (Vector3 A, Vector3 B, Vector3 C)
	{
		Vector3 AB = B - A,
			AC = C - A;
		float area = fabs (CrossProductXY (AC, AB));
		area = area / 2;
		return area;
	}

	float GetAngle_rads (Vector3 A, Vector3 B)
	{
		A.Normalize();
		B.Normalize();
		float cosAngle = Vector3::DotProduct(A, B);
		float angle_rads = acos(cosAngle);
		return angle_rads;
	}

	float GetAngle_degs (Vector3 A, Vector3 B)
	{
		A.Normalize();
		B.Normalize();
		float cosAngle = Vector3::DotProduct(A, B);
		float angle_rads = acos(cosAngle);
		float angle_degs = angle_rads * 180 / PI;
		return angle_degs;
	}

	float GetProjectionLength (Vector3 A, Vector3 base)
	{
		float dpResult = Vector3::DotProduct(A, base);
		float projectionLength = dpResult / base.GetLength();
		return projectionLength;
	}

	Vector3 PointLineProjection (Vector3 point, Vector3 lineA, Vector3 lineB)
	{
		Vector3 result;
		return result;
	}
}