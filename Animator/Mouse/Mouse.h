// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the MOUSE_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// MOUSE_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.

#ifdef MOUSE_EXPORTS
#define MOUSE_API __declspec(dllexport)
#else
#define MOUSE_API __declspec(dllimport)
#endif

#ifndef TS_MOUSE_H
#define TS_MOUSE_H

#include "..\Geometry\Geometry.h"

class MOUSE_API Mouse
{
private:
	VectorMath::Vector3 currentPos,					// the current and the last position of the mouse pointer
		lastPos;

	VectorMath::Vector3 ogl3dPosition,				// The point in the 3D game space where the pointer points to
						ogl2dPosition;				// The point of the the GUI Space where the pointer points to

	bool stateLeftButton,							// the states (pressed or relleased) of the mouse buttons
		stateRightButton;
public:
	Mouse()
	{
		this->currentPos = VectorMath::Vector3();
		this->lastPos = VectorMath::Vector3();
		this->stateLeftButton = false;
		this->stateRightButton = false;
	}

	bool IsLeftButtonDown () { return this->stateLeftButton; }
	bool IsRightButtonDown () {	return stateRightButton; }
	void PressLeftButton () { this->stateLeftButton = true; }
	void ReleaseLeftButton () {	this->stateLeftButton = false; }
	void PressRightButton () { this->stateRightButton = true; }
	void ReleaseRightButton () { this->stateRightButton = false; }

	void SetCurrentPosition (VectorMath::Vector3 newPosition)
	{
		this->lastPos = this->currentPos;
		this->currentPos = newPosition;
	}

	void SetCurrent3DPosition(VectorMath::Vector3 ogl3dPosition) { this->ogl3dPosition = ogl3dPosition; }
	void SetCurrent2DPosition(VectorMath::Vector3 ogl2dPosition) { this->ogl2dPosition = ogl2dPosition; }
	void OverrideLastPosition (VectorMath::Vector3 lastPosition) { this->lastPos = lastPosition; }
	VectorMath::Vector3 GetCurrentPosition() { return this->currentPos; }
	VectorMath::Vector3 GetLastPosition() {	return this->lastPos; }
	VectorMath::Vector3 Get3DPosition() { return this->ogl3dPosition; }
	VectorMath::Vector3 Get2DPosition() { return this->ogl2dPosition; }
};

#endif