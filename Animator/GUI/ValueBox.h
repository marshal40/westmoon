
#ifndef VALUEBOX_H
#define VALUEBOX_H

#ifdef GUI_EXPORTS
#define GUI_API __declspec(dllexport)
#else
#define GUI_API __declspec(dllimport)
#endif

#include <string>
#include <vector>
#include "Button.h"
#include "TextBox.h"
#include "BitmapFont.h"

using namespace std;

namespace GUI
{
	class GUI_API ValueBox
	{
	private:
		vector<string> values;
		Button *leftButton, *rightButton;
		TextBox *value;
		short currValue;

	public:
		ValueBox(int numValues, BitmapFont* font, float textSize, Box window, Box box);
		void PushValue(string newValue);
		string GetCurrentValue();
		void Update();
		void Draw();
	};
}

#endif VALUEBOX_H