
#include "stdafx.h"
#include "ValueBox.h"

namespace GUI
{
	ValueBox::ValueBox(int numValues, BitmapFont* font, float textSize, Box window, Box box)
	{
		this->values.reserve(numValues);
		this->leftButton = new Button("vb_left", font, textSize, window, box, "<");
	}

	void ValueBox::PushValue(string newValue) 
	{
		this->values.push_back(newValue);
	}

	string ValueBox::GetCurrentValue() 
	{
		return this->values[currValue];
	}

	void ValueBox::Update()		
	{
		if(rightButton->IsPressed())
		{
			this->currValue++;
			if(this->currValue >= this->values.size())
				this->currValue = 0;
		}
		else if(leftButton->IsPressed())
		{
			this->currValue--;
			if(this->currValue < 0)
				this->currValue = this->values.size() - 1;
		}
	}

	void ValueBox::Draw()
	{
		this->rightButton->Draw();
		this->leftButton->Draw();
		this->value->Draw();
	}
}