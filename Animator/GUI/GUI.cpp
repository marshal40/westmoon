// GUI.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "GUI.h"

namespace GUI
{
	GUI::GUI(float posX, float posY, float posZ, float depth) 
	{
		this->box.upperLeft = VectorMath::Vector3(posX, posY, posZ);
		this->box.lowerRight = VectorMath::Vector3(-posX, -posY, posZ - depth);
	}

	void GUI::ReserveWindows(int numWindows) 
	{ 
		this->windows.DeclareElements(numWindows);
	}

	Window* GUI::AddWindow(Window* window)
	{
		if(this->windows.PushElement(window)) 
			return this->windows[this->windows.GetNumElements() - 1];
		else return NULL;
	}

	void GUI::Update(Mouse* mouse)
	{
		for(unsigned i = 0; i < this->windows.GetNumElements(); i++)
			this->windows[i]->Update(mouse);
	}

	void GUI::Draw()
	{
		for(unsigned i = 0; i < this->windows.GetNumElements(); i++)
			this->windows[i]->Draw();
	}

	Window* GUI::GetWindow(char* name)
	{
		unsigned numElements = this->windows.GetNumElements();
		for(unsigned i = 0; i < numElements; i++)
		{

			char *windowName = this->windows[i]->GetName();
			if(strcmp(name, windowName) == 0)
				return this->windows[i];
		}

		exit(0);			// WARNING
		return NULL;
	}
}