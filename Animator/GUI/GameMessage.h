
#ifndef GAMEMESSAGE_H
#define GAMEMESSAGE_H

#ifdef GUI_EXPORTS
#define GUI_API __declspec(dllexport)
#else
#define GUI_API __declspec(dllimport)
#endif

#include <vector>
#include <string>
#include <cstring>

#include "BitmapFont.h"
#include "TextBox.h"

using namespace std;
using namespace VectorMath;

namespace GUI
{
	class GUI_API GameMessage
	{
	private:
		char message[128];										// text value 
		unsigned life_frames;									// how many frames this message will be visible

	public:
		GameMessage(char *message, unsigned life_frames = 60);
		void Update();
		bool IsAlive();
		char* GetMessage() { return this->message; }
	};

	class GUI_API GameMessageHandler
	{
	private:
		vector<GameMessage> messages;
		Box gui;						// GUI Space where the messages will be showed
		float textSize;					// size of the text ot the game messages
		BitmapFont *font;				// font of the text

		struct
		{
			TextBox *message;			// text value
			int alive;					// is there a message to be shown
			float textSize;				// the size of the text
		} fullScreenMessage;

	public:
		GameMessageHandler(Box gui, BitmapFont *font, float textSize = 0.02f, float fullScrMessageSize = 0.05f);
		void PushBack(char *message);						// Add new Game Message
		void SetFullScreenMessage(char *message);			// Set a full screen message
		void Update();
		void Draw();
	};
}
#endif