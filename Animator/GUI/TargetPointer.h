
#ifndef TARGETPOINTER_H
#define TARGETPOINTER_H

#include "../Geometry/Geometry.h"
#include "Pointer.h"

using namespace std;

namespace GUI
{
	class GUI_API TargetPointer : public Pointer
	{
	private:
		Vector3 equilibriumPosition;				// the equilibrium (central) position
		Box window;
		float centripetalQueff;						// the magnitude of the centripetal force
		float *tirednessValue;						// value that represents the tiredness of the player

	public:
		TargetPointer(Box window, Vector3 position, Texture2D* skin, float *tirednessValue, float size = 0.03f);
		void ApplyForce(Vector3 force);
		void Update();

	private:
		void ApplyCentripetalForce();				// the force which drag the pointer to its equilibrium position
		int timeCounter;
		void ApplyTirednessEffect();				// the effect of the tremble hand of the player
	};
}

#endif 