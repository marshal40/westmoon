
#include "stdafx.h"
#include "Compass.h"
#include <cmath>
#include <cstring>

namespace GUI
{
	Compass::Compass(Box window, Box box, 
		Texture2D *movablePart, Texture2D *staticPart, Texture2D *pointTexture, 
		Vector3 *worldPosition, Vector3 *rotationPtr, Vector3 northVector)
	{
		this->guiLocation = CalculatePercent(window, box);
		this->guiLocation.lowerRight.z = -10.0f; //window.lowerRight.z;
		this->guiLocation.upperLeft.z = -10.0f; //window.upperLeft.z;
		float height = abs(this->guiLocation.lowerRight.y - this->guiLocation.upperLeft.y);
		this->guiLocation.lowerRight.x = this->guiLocation.upperLeft.x + height;			// make square
		
		this->movablePart = movablePart;
		this->staticPart = staticPart;
		this->pointTexture = pointTexture;

		this->northVector = northVector;

		this->Set(worldPosition, rotationPtr);

		this->points.DeclareElements(10);
	}

	/*
	void Compass::Update(Vector3 worldPosition, Vector3 fwdVector)
	{
		this->worldPosition = worldPosition;
		this->fwdVector = fwdVector;

		float angleSine = VectorMath::CrossProduct(this->fwdVector, this->northVector).GetLength() / this->fwdVector.GetLength() * this->northVector.GetLength();
		float angleCosine = Vector3::DotProduct(this->fwdVector, this->northVector) / this->fwdVector.GetLength() * this->northVector.GetLength();
		this->rotationY = atan(angleSine / angleCosine) * PI / 180;
	}

	void Compass::Update(Vector3 worldPosition, float rotationY)
	{
		this->worldPosition = worldPosition;
		this->rotationY = rotationY;
		this->fwdVector = this->northVector;
		this->fwdVector.RotateInXY(this->rotationY);
	}*/

	void Compass::Set(Vector3 *worldPosition, Vector3 *rotationPtr)
	{
		this->worldPosition = worldPosition;
		this->rotationPtr = rotationPtr;
	}

	void Compass::Draw(Camera *camera)
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		this->staticPart->bind();
		this->guiLocation.Draw();
		
		this->movablePart->bind();
		this->guiLocation.Draw(this->rotationPtr->y + 90.0f);

		//this->DrawPoints();
	}

	void Compass::PushPoint(CompassPoint *point)
	{
		this->points.PushElement(point);
	}

	void Compass::DeletePoint(char* name)
	{
		for(unsigned i = 0; i < this->points.GetNumElements(); i++)
			if(strcmp(this->points[i]->name, name) == 0)
			{
				this->points.EraseElement(i);
				return;
			}
	}

	void Compass::DrawPoints()
	{
		glPushMatrix();
		for(unsigned i = 0; i < this->points.GetNumElements(); i++)
		{
			glPushMatrix();
			Vector3 fwdVector = this->northVector; //(0.0f, 0.0f, 1.0f);
			fwdVector.RotateAroundYAxis(this->rotationPtr->y);
			Vector3 pointPosV = *this->points[i]->worldPosition - *this->worldPosition;
			float dotProduct = Vector3::DotProduct(fwdVector, pointPosV);
			float angleCosine = dotProduct / (fwdVector.GetLength()*pointPosV.GetLength());
			float angle = acos(angleCosine)*180.0f/PI;
			
			this->pointTexture->bind();
			this->guiLocation.Draw(angle);

			glPopMatrix();
		}
		glPopMatrix();
	}
}