// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the GUI_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// GUI_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.

#ifndef WINDOW_H
#define WINDOW_H

#ifdef GUI_EXPORTS
#define GUI_API __declspec(dllexport)
#else
#define GUI_API __declspec(dllimport)
#endif

#include <cmath>
#include <vector>

#include "../Geometry/Geometry.h"
#include "../Md2Model/Texture.h"
#include "../Md2Model/TextureManager.h"
#include "../Md2Model/Image.h"

#include "Box.h"
#include "TextBox.h"
#include "Button.h"
#include "../SmartArray/SmartArray.h"

namespace GUI
{
	class GUI_API Window
	{
	private:
		char name[64];

		Box box;									// location in the GUI Space
		Texture2D* skin;					

		SmartArray<Button*> buttons;
		SmartArray<TextBox*> textBoxes;
		//vector<Button> buttons;						// all buttons situated in the window
		//vector<TextBox> textBoxes;					// all textboxes situated in the window
			
		bool mouseOverAButton;						// if the mouse is over any button
		bool buttonPressed;							// if any of the the buttons is pressed

		TextBox *titleBox;							// title of the window

	public:
		Window(char* name, Box gui,  
			VectorMath::Vector3 procentUpperLeft, 
			VectorMath::Vector3 procentLowerRight,
			Texture2D* skin);
		
		void ReserveButtons(int buttons);							// Declare the number of the button
		void ReserveTextBoxes(int textBoxes);						// Declare the number of the textboxes
		
		void Update(Mouse *mouse);
		void Draw();

		Button* AddButton(Button *newButton);						// Add new button
		void RemoveButton(char *buttonName);							// Remove a button

		TextBox* AddTextBox(TextBox *newTextBox);					// Add new textbox
		//void RemoveTextBox(char *textboxName);						// Remove a textbox

		void SetTitleBox(TextBox *titleBox);						// Set title of the window
		void RemoveTitleBox();										// Romeve the window's title

		Box GetBox() { return this->box; }
		bool IsMouseOverAButton() { return this->mouseOverAButton; }
		bool IsAnyButtonPressed() { return this->buttonPressed; }

		bool IsMouseOverButton(char* buttonName);
		bool IsButtonPressed(char* buttonName);
		
		char* GetName() { return this->name; }
	};
}

#endif