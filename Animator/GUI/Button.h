#ifndef BUTTON_H
#define BUTTON_H

#ifdef GUI_EXPORTS
#define GUI_API __declspec(dllexport)
#else
#define GUI_API __declspec(dllimport)
#endif

#include "Box.h"
#include "TextBox.h"
#include "BitmapFont.h"
#include "../Mouse/Mouse.h"

namespace GUI
{	
	#define MAX_TEXT_SIZE 256
	
	class GUI_API Button
	{
	private:
		bool pressed;								// if the button is pressed
		bool mouseOver;								// if the mouse is over the buton
		bool lastMouseOver;							// is in the last moment (frame) the mouse was over the button

		TextBox* label;								// the text value of the button
		Box location;								// location of the button

		char name[64];

	public:
		Button(char* name, BitmapFont* font, float textSize, Box window, Box box, char textValue[MAX_TEXT_SIZE], Aligment al = CENTERED);

		void Update(Mouse *mouse);
		void Draw();

		bool IsPressed() { return this->pressed; }
		bool IsMouseOver() { return this->mouseOver; }
		void SetMouseOver(bool isMouseOver) { this->mouseOver = isMouseOver; }
		Box GetLocation() { return this->location; }
		char* GetName() { return this->name; }
	};
}


#endif