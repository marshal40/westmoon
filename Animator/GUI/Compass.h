
#ifndef TS_COMPASS_H
#define TS_COMPASS_H

#ifdef GUI_EXPORTS
#define GUI_API __declspec(dllexport)
#else
#define GUI_API __declspec(dllimport)
#endif


#include "Box.h"
#include "CompassPoint.h"
#include "../Geometry/Geometry.h"
#include "../Camera/Camera.h"
#include "../SmartArray/SmartArray.h"
#include "../Md2Model/Texture.h"

namespace GUI
{
	class GUI_API Compass
	{
	private:
		Box guiLocation;

		Texture2D* movablePart, *staticPart;
		Texture2D* pointTexture;

		//Vector3 worldPosition;
		//Vector3 fwdVector;
		Vector3 northVector;
		//float rotationY;
		Vector3 *rotationPtr;
		Vector3 *worldPosition;

		//SmartArray<Vector3> points;
		SmartArray<CompassPoint*>points;
	public:

		Compass(Box window, Box box, 
			Texture2D *movablePart, Texture2D *staticPart, Texture2D *pointTexture,
			Vector3 *worldPosition, Vector3 *rotationPtr, Vector3 northVector);
		//void Update(Vector3 worldPosition, Vector3 fwdVector); 
		//void Update(Vector3 worldPosition, float rotationY); 
		void Set(Vector3 *worldPosition, Vector3 *rotationPtr);

		void Draw(Camera *camera);

		void PushPoint(CompassPoint* point);
		void DeletePoint(char* name);
	private:
		//void DrawMovablePart();
		//void DrawStaticPart();
		void DrawPoints();
	};
}
#endif