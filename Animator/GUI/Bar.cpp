
#include "stdafx.h"
#include "Bar.h"

namespace GUI
{
	Bar::Bar(Box window, Box box, Texture2D *textureFull, Texture2D *textureEmpty,			// construct the object and defines some defaults
		Vector3 color,
		float minValue , float maxValue)
	{
		this->window = window;
		this->textureFull = textureFull;
		this->textureEmpty = textureEmpty;
		this->box = CalculatePercent(this->window, box);

		this->minValue = minValue;
		this->maxValue = maxValue;
		this->currValue = this->minValue;
		this->percent = 0.0f;

		this->type = INTERNAL_VALUE;

		this->color = color;

		this->iconTexture = NULL;
	}

	void Bar::SetIcon(Texture2D *iconTexure, Box iconBox)
	{
		this->iconTexture = iconTexure;
		this->iconBox = CalculatePercent(this->window, iconBox);
		//this->iconBox.upperLeft.x = this->iconBox.lowerRight.x - (this->iconBox.upperLeft.y - this->iconBox.lowerRight.y);
		this->iconBox.lowerRight.y = this->iconBox.upperLeft.y - (this->iconBox.lowerRight.x - this->iconBox.upperLeft.x);
	}

	void Bar::SetPCurrValue(float *currValuep)
	{
		this->type = EXTERNAL_VALUE;
		this->currValuep = currValuep;
	}

	void Bar::DeletePCurrValue()
	{
		this->type = INTERNAL_VALUE;
	}

	float Bar::GetValue() 
	{
		switch(type)
		{
			case EXTERNAL_VALUE: return *this->currValuep;
			case INTERNAL_VALUE: return this->currValue;
		} 
	}

	void Bar::SetCurrValue(float currValue) 
	{
		if(currValue > this->maxValue)												// Is the new value is beyond the legal interval 
			this->currValue = this->maxValue;										// set the current value to the maximal possible
		else if(currValue < this->minValue)											// If the new value is below the legal interval
			this->currValue = this->minValue;										// set the current value to the minimal possible value
		else																		// else the new value is in the legal interval and everything is OK
				this->currValue =  currValue;

		this->percent = (this->currValue - this->minValue) / (this->maxValue - this->minValue);			// calculate the percent
	}
	
	void Bar::IncreaseCurrValue(float value) 
	{
		if(type == INTERNAL_VALUE)												// Curr Value can be modified only if is internal value
			this->SetCurrValue(this->currValue + value); 
	}

	void Bar::Draw(Vector3 color)
	{
		glPushMatrix();
		glColor3f(color.x, color.y, color.z);
		if(this->percent > 0.01f) this->DrawFullPart();							// Draw the full part if is visible
		if(this->percent < 0.99f) this->DrawEmptyPart();						// Draw the empty part if is visible
		if(this->iconTexture != NULL)
		{
			glColor3f(1.0f, 1.0f, 1.0f);
			this->iconTexture->bind();
			this->iconBox.Draw();
		}
		glPopMatrix();
	}

	void Bar::Update()
	{
		if(this->type == EXTERNAL_VALUE)
			this->SetCurrValue(*this->currValuep);

		this->full = CalculatePercent(this->box, Box(Vector3(0.0f, 0.0f), Vector3(this->percent, 1.0f)));
		this->empty = CalculatePercent(this->box, Box(Vector3(this->percent, 0.0f), Vector3(1.0f, 1.0f)));
	}

	void Bar::Draw()
	{
		this->Draw(this->color);
	}

	void Bar::DrawFullPart()																		// Draws the full part of the bar
	{
		this->textureFull->bind();
	
		glBegin(GL_QUADS);						
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(this->full.upperLeft.x, 
					this->full.lowerRight.y, 
					this->full.upperLeft.z);

		glTexCoord2f(this->percent, 0.0f);
		glVertex3f(this->full.lowerRight.x, 
					this->full.lowerRight.y, 
					this->full.lowerRight.z);

		glTexCoord2f(this->percent, 1.0f);
		glVertex3f(this->full.lowerRight.x, 
					this->full.upperLeft.y, 
					this->full.lowerRight.z);

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(this->full.upperLeft.x, 
					this->full.upperLeft.y, 
					this->full.upperLeft.z);
			
		glEnd();
	}
	
	void Bar::DrawEmptyPart()																		// Draws the empty part of the bar
	{
		this->textureEmpty->bind();
	
		glBegin(GL_QUADS);		
		glTexCoord2f(this->percent, 0.0f);
		glVertex3f(this->empty.upperLeft.x, 
					this->empty.lowerRight.y, 
					this->empty.upperLeft.z);

		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(this->empty.lowerRight.x, 
					this->empty.lowerRight.y, 
					this->empty.lowerRight.z);

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(this->empty.lowerRight.x, 
					this->empty.upperLeft.y, 
					this->empty.lowerRight.z);

		glTexCoord2f(this->percent, 1.0f);
		glVertex3f(this->empty.upperLeft.x, 
					this->empty.upperLeft.y, 
					this->empty.upperLeft.z);
			
		glEnd();
	}
}