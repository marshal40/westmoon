
#include "stdafx.h"
#include "TargetPointer.h"

namespace GUI
{
	TargetPointer::TargetPointer(Box window, Vector3 position, Texture2D* skin, float *tirednessValue, float size)
	{
		this->size = abs(CalculatePercent(window, Vector3(size, size)).x - window.upperLeft.x);		// calculate the real size of the pointer
		this->skin = skin;
		this->window = window;
		this->SetPosition(window, position);
		this->equilibriumPosition = this->position;
		this->targetPosition = Vector3();
		this->tirednessValue = tirednessValue;
		this->centripetalQueff = 0.007f;
		this->timeCounter = 0;
	}

	void TargetPointer::ApplyForce(Vector3 force)
	{ 
		this->SetPosition(this->window, this->position+ force);
	}

	void TargetPointer::Update()
	{
		this->ApplyTirednessEffect();
		this->ApplyCentripetalForce();
	}

	void TargetPointer::ApplyCentripetalForce()
	{
		if((this->equilibriumPosition - this->position).GetSqLength() > 0.0001f)		// if there is a visible difference between the
		{																				// equilibrumPosition and the current position
			Vector3 centripetalForce = (this->equilibriumPosition - this->position).Normal()*this->centripetalQueff;
			this->ApplyForce(centripetalForce);
		}
	}
		
	void TargetPointer::ApplyTirednessEffect()
	{
		if(*this->tirednessValue > 50.0f)
		{	
			this->timeCounter++;

			if(this->timeCounter % 5 == 0)
			{
				float angle = rand() % 360;												// Get a random angle
				float length = ((float)(rand() % (int)*this->tirednessValue)/100.0f);	// Get a random length according to the tiredness of the player		
				length = length * 5 * this->centripetalQueff;
				if(length == 0.0f) length = 5 * (*this->tirednessValue/100.0f) * this->centripetalQueff;;				
				
				Vector3 force = Vector3(cos(angle*PI/180.0f), sin(angle*PI/180.0f))*length;	
				
				this->ApplyForce(force);
			}
		}
	}
}