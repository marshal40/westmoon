
#ifndef TS_COMPASSPOINT_H
#define TS_COMPASSPOINT_H

#ifdef GUI_EXPORTS
#define GUI_API __declspec(dllexport)
#else
#define GUI_API __declspec(dllimport)
#endif

#include "../Geometry/Geometry.h";
using namespace VectorMath;

namespace GUI
{
	class GUI_API CompassPoint
	{
	public:
		char name[128];
		Vector3 *worldPosition;

		CompassPoint(char* name, Vector3 *worldPosition);
	};
}

#endif