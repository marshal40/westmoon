
#include "stdafx.h"
#include "GameMessage.h"

namespace GUI
{
	GameMessage::GameMessage(char *message, unsigned life_frames)
	{
		strcpy(this->message, message);
		this->life_frames = life_frames;
	}

	void GameMessage::Update()
	{
		if(this->life_frames > 0)				// decrement the life ot the message
			this->life_frames--;
	}

	bool GameMessage::IsAlive()
	{
		return this->life_frames != 0;
	}



	GameMessageHandler::GameMessageHandler(Box gui, BitmapFont *font, float textSize, float fullScrMessageSize)
	{ 
		this->messages.reserve(10);
		this->gui = gui;
		this->font = font;
		this->textSize = textSize;

		this->fullScreenMessage.alive = false;
		this->fullScreenMessage.textSize = fullScrMessageSize;
	}

	void GameMessageHandler::PushBack(char *message)						// Add new Game Message
	{
		this->messages.push_back(GameMessage(message));
	}

	void GameMessageHandler::SetFullScreenMessage(char *message)			// Set a full screen message
	{
		this->fullScreenMessage.alive = 30;
		this->fullScreenMessage.message = new TextBox(this->font, 
			this->fullScreenMessage.textSize,
			this->gui, 
			Box(Vector3(0.0f, 0.0f), Vector3(1.0f, 1.0f)), 
			message);
	}

	void GameMessageHandler::Update()
	{
		for(int i = 0; i < this->messages.size(); i++)
		{
			this->messages[i].Update();
			if(!this->messages[i].IsAlive())								// if a message will not being shown any more
			{
				this->messages.erase(this->messages.begin() + i);			// erease is from the vector with messages
				i--;
			}
		}

		if(this->fullScreenMessage.alive) 
			this->fullScreenMessage.alive--;
	}

	void GameMessageHandler::Draw()
	{
		for(int i = 0; i < this->messages.size(); i++)
		{
			TextBox tb(this->font, 
				this->textSize, 
				this->gui, 
				Box(Vector3(0.02f, 0.03f + i*2*this->textSize), Vector3(0.5f, 0.03f + (i*2 + 1)*this->textSize)),
				this->messages[i].GetMessage(), 
				Aligment::LEFT);
			tb.SetColor(Vector3(1.0f, 0.5f, 0.0f));
			tb.Draw();
		}

		if(this->fullScreenMessage.alive)
			this->fullScreenMessage.message->Draw(Vector3(1.0f, 0.5f, 0.0f));
	}
}