
#include "stdafx.h"
#include "Pointer.h"
#if defined(__gl_h_) || defined(__GL_H__) || defined(__X_GL_H)
#else
#include <gl/glew.h>
#endif

namespace GUI
{
	bool Box::Contain(Vector3 point)
	{
		return point.x > this->upperLeft.x && point.x < this->lowerRight.x &&
			point.y > this->upperLeft.y && point.y < this->lowerRight.y;
	}

	Vector3 Box::GetCenter()
	{
		return (this->lowerRight + this->upperLeft)/2;
	}

	Vector3 Box::GetDimensions()
	{
		Vector3 dims =  (this->lowerRight - this->upperLeft)/2;
		//if(dims.x < 0) dims.x *= -1;
		//if(dims.y < 0) dims.y *= -1;
		//if(dims.z < 0) dims.z *= -1;
		return dims;
	}

	void Box::Draw(float rotation_degs)
	{
		glPushMatrix();
		Vector3 center = this->GetCenter();
		Vector3 dims = this->GetDimensions();
		glTranslatef(center.x, center.y, center.z);
		glRotatef(rotation_degs, 0.0f, 0.0f, 1.0f);
		glBegin(GL_QUADS);						
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-dims.x, dims.y, -dims.z);

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(dims.x, dims.y, dims.z);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(dims.x, -dims.y, dims.z);

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(-dims.x, -dims.y, -dims.z);
		
		/*
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-this->upperLeft.x, 
					this->lowerRight.y, 
					this->upperLeft.z);

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(this->lowerRight.x, 
					this->lowerRight.y, 
					this->lowerRight.z);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(this->lowerRight.x, 
					this->upperLeft.y, 
					this->lowerRight.z);

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(this->upperLeft.x, 
					this->upperLeft.y, 
					this->upperLeft.z);
		*/
		glEnd();
		glPopMatrix();
	}

	GUI_API inline Vector3 CalculatePercent(Box box, VectorMath::Vector3 procent)
	{
		return  VectorMath::Vector3((box.upperLeft.x*(1 - procent.x) + 
										box.lowerRight.x*procent.x),
									(box.upperLeft.y*(1 - procent.y) + 
										box.lowerRight.y*procent.y),
									(box.upperLeft.z*(1 - procent.z) + 
										box.lowerRight.z*procent.z));
	}

	GUI_API Box CalculatePercent(Box bigBox, Box littleBox)
	{
		return Box(CalculatePercent(bigBox, littleBox.upperLeft), 
							CalculatePercent(bigBox, littleBox.lowerRight));
	}
}