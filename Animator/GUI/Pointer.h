
#ifndef POINTER_H
#define POINTER_H

#ifdef GUI_EXPORTS
#define GUI_API __declspec(dllexport)
#else
#define GUI_API __declspec(dllimport)
#endif

#include "Box.h"
#include "../Geometry/Geometry.h"
#include "../Md2Model/Texture.h"
#include "../Md2Model/TextureManager.h"
#include "../Md2Model/Image.h"

using namespace VectorMath;

namespace GUI
{
	class GUI_API Pointer
	{
	protected:
		Box box;													// the GUI box in which the pointer will be drawn
		Texture2D* skin;											// the skin of the pointer
		Vector3 position;											// GUI relative coordinates
		Vector3 targetPosition;										// 3d game world - relative coordinates
		float size;													

		Pointer() { }
	public:
		Pointer(Box window, Vector3 position, Texture2D* skin, float size);

		void SetSize(float size) { this->size = size; }
		float GetSize() { return this->size; }

		void SetPosition(Box window, Vector3 position);
		Vector3 GetPosition() { return this-> position; } 

		void Draw();

		void SetTargetPosition(Vector3 targetPosition) { this->targetPosition = targetPosition; }
		Vector3 GetTargetPosition() { return this->targetPosition; }	// Returns the point of the 3D world that is represented by the pointer

		Vector3 GetScreenPosition(int screenX, int screenY);			// Return the position in screen relative coordinates
	};
}
#endif