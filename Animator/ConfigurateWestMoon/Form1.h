#pragma once

#include <fstream>
using namespace std;

namespace ConfigurateWestMoon {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  resolutionLabel;
	private: System::Windows::Forms::Label^  title;
	private: System::Windows::Forms::GroupBox^  windowSettingsGroup;

	protected: 

	private: System::Windows::Forms::CheckBox^  fullscreen;
	private: System::Windows::Forms::Button^  okButton;


	private: System::Windows::Forms::TextBox^  resolutionX;
	private: System::Windows::Forms::TextBox^  resolutionY;
	private: System::Windows::Forms::Button^  cancel;



	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			fstream file("window.ini", ios::in);
			if(!file.is_open()) exit(0);				// Check if the file could be opened

			char resX_cstr[16], resY_cstr[16];			// char* of the X and Y coordinated of the resulution
			bool fscr;									// is fullscreen

			char name_cstr [128];
			file.getline(name_cstr, 128);

			file >> resX_cstr >> resY_cstr;
			int pX, pY;
			file >> pX >> pY;
			file >> fscr;

			this->resolutionLabel = (gcnew System::Windows::Forms::Label());
			this->title = (gcnew System::Windows::Forms::Label());
			this->windowSettingsGroup = (gcnew System::Windows::Forms::GroupBox());
			this->resolutionY = (gcnew System::Windows::Forms::TextBox());
			this->resolutionX = (gcnew System::Windows::Forms::TextBox());
			this->fullscreen = (gcnew System::Windows::Forms::CheckBox());
			this->okButton = (gcnew System::Windows::Forms::Button());
			this->cancel = (gcnew System::Windows::Forms::Button());
			this->windowSettingsGroup->SuspendLayout();
			this->SuspendLayout();
			// 
			// resolutionLabel
			// 
			this->resolutionLabel->AutoSize = true;
			this->resolutionLabel->Location = System::Drawing::Point(6, 16);
			this->resolutionLabel->Name = L"resolutionLabel";
			this->resolutionLabel->Size = System::Drawing::Size(63, 13);
			this->resolutionLabel->TabIndex = 2;
			this->resolutionLabel->Text = L"Resolution: ";
			// 
			// title
			// 
			this->title->Anchor = System::Windows::Forms::AnchorStyles::None;
			this->title->AutoSize = true;
			this->title->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->title->Location = System::Drawing::Point(11, 9);
			this->title->Name = L"title";
			this->title->Size = System::Drawing::Size(204, 20);
			this->title->TabIndex = 4;
			this->title->Text = L"West Moon Configurator";
			// 
			// windowSettingsGroup
			// 
			this->windowSettingsGroup->Controls->Add(this->resolutionY);
			this->windowSettingsGroup->Controls->Add(this->resolutionX);
			this->windowSettingsGroup->Controls->Add(this->fullscreen);
			this->windowSettingsGroup->Controls->Add(this->resolutionLabel);
			this->windowSettingsGroup->Location = System::Drawing::Point(12, 42);
			this->windowSettingsGroup->Name = L"windowSettingsGroup";
			this->windowSettingsGroup->Size = System::Drawing::Size(208, 70);
			this->windowSettingsGroup->TabIndex = 5;
			this->windowSettingsGroup->TabStop = false;
			this->windowSettingsGroup->Text = L"Window settings";
			// 
			// resolutionY
			// 
			this->resolutionY->Location = System::Drawing::Point(140, 13);
			this->resolutionY->Name = L"resolutionY";
			this->resolutionY->Size = System::Drawing::Size(59, 20);
			this->resolutionY->TabIndex = 4;
			this->resolutionY->Text = gcnew String(resY_cstr); //L"800";
			// 
			// resolutionX
			// 
			this->resolutionX->Location = System::Drawing::Point(75, 13);
			this->resolutionX->Name = L"resolutionX";
			this->resolutionX->Size = System::Drawing::Size(59, 20);
			this->resolutionX->TabIndex = 4;
			this->resolutionX->Text = gcnew String(resX_cstr); //L"1280";
			// 
			// fullscreen
			// 
			this->fullscreen->AutoSize = true;
			this->fullscreen->Location = System::Drawing::Point(9, 40);
			this->fullscreen->Name = L"fullscreen";
			this->fullscreen->Size = System::Drawing::Size(74, 17);
			this->fullscreen->TabIndex = 3;
			this->fullscreen->Text = L"Fullscreen";
			this->fullscreen->Checked = fscr;
			this->fullscreen->UseVisualStyleBackColor = true;
			// 
			// okButton
			// 
			this->okButton->Location = System::Drawing::Point(45, 118);
			this->okButton->Name = L"okButton";
			this->okButton->Size = System::Drawing::Size(74, 20);
			this->okButton->TabIndex = 6;
			this->okButton->Text = L"OK";
			this->okButton->UseVisualStyleBackColor = true;
			this->okButton->Click += gcnew System::EventHandler(this, &Form1::okButton_Click);
			// 
			// cancel
			// 
			this->cancel->Location = System::Drawing::Point(125, 118);
			this->cancel->Name = L"cancel";
			this->cancel->Size = System::Drawing::Size(74, 20);
			this->cancel->TabIndex = 6;
			this->cancel->Text = L"Cancel";
			this->cancel->UseVisualStyleBackColor = true;
			this->cancel->Click += gcnew System::EventHandler(this, &Form1::cancel_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(231, 145);
			this->Controls->Add(this->cancel);
			this->Controls->Add(this->okButton);
			this->Controls->Add(this->windowSettingsGroup);
			this->Controls->Add(this->title);
			this->Name = L"Form1";
			this->Text = L"West Moon Configurator";
			this->windowSettingsGroup->ResumeLayout(false);
			this->windowSettingsGroup->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	
private: System::Void okButton_Click(System::Object^  sender, System::EventArgs^  e) {
			String^ resX = this->resolutionX->Text;
			for(int i = 0; i < resX->Length; i++)
				if(resX[i] < '0' || resX[i] > '9')
					return;

			String^ resY = this->resolutionY->Text;
			for(int i = 0; i < resY->Length; i++)
				if(resY[i] < '0' || resY[i] > '9')
					return;

			bool fscr = this->fullscreen->Checked;

			fstream file("window.ini", ios::out);
			file << "WestMoon\n";

			char ch;

			for(int i = 0; i < resX->Length; i++)
				if(resX[i] >= '0' && resX[i] <= '9')
				{
					ch = resX[i];
					file << ch;
				}
				else break;

			file << " ";

			for(int i = 0; i < resY->Length; i++)
				if(resY[i] >= '0' && resY[i] <= '9')
				{
					ch = resY[i];
					file << ch;
				}
				else break;
			file << endl;

			file << "10 10\n";
			file << fscr << endl;
			file.close();
			system("WestMoon.exe");
			exit(0);


		 }

private: System::Void cancel_Click(System::Object^  sender, System::EventArgs^  e) {
			exit(0);
		 }
};
}

