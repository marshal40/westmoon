
#ifndef LEVEL01_H
#define LEVEL01_H

#define PRESENT

#include "../Environment/EnvironmentData.h"
#include "../Environment/Environment.h"
#include "../Environment/LiveObject.h"
#include "Player.h"
#include "Horse.h"
#include "Level.h"
#include "MissionsLogic.h"
#include "../Mouse/Mouse.h"
#include "../Keyboard/Keyboard.h"

#include <sstream>

class Level01 : public Level			
{
private:
	int timeLastShotLeft, timeLastShotRight;					// When was performed the last shot
public:
	Environment environment;
	Player *player;
	//Horse *horse;
	GUI::GUI *gui;
	GUI::GameMessageHandler *messageHandler;
	GUI::Bar *lifeBar, *tirednessBar;
	GUI::TargetPointer *targetPointer;
	GUI::Compass *compass;

	GUI::Bar *leftGunBullets, *rightGunBullets;

	LiveObject *hurtMan;

	Level01(GUI::GameMessageHandler *messageHandler, GUI::GUI *gui);
	~Level01();
	void Init(GUI::GameMessageHandler *messageHandler, GUI::GUI *gui);
	Camera* GetPlayerCamera(Player::CameraMode mode); 
	void UpdatePlayerCamera();
	void Update(Keyboard* keyboard, Mouse *mouse);
	void UpdateGameLogics();
	void UpdateGameLogicsFromFile();
	void Draw(Camera *camera);
	void Draw2D(Camera *camera);

	void SaveToFile(const char* filename);
	void LoadFromFile(const char* filename);

	Environment* GetEnvironment();
	
	void InitMissionFromFile();
	void LoadMissonLogicsCode();

private:
	stringstream missoinLogicsCode;
	int manStartedTalking_time;
};

Environment* Level01::GetEnvironment()
{
	return &this->environment;
}

Level01::Level01(GUI::GameMessageHandler *messageHandler, GUI::GUI *gui = NULL)
{
	this->Init(messageHandler, gui);
}

Level01::~Level01()
{
	delete player;
}

void Level01::Init(GUI::GameMessageHandler *messageHandler, GUI::GUI *gui = NULL)
{
	this->gui = gui;
	this->messageHandler = messageHandler;
	this->environment.LoadEnvironment("Data/WestMoon.rci");
	this->player = new Player(&this->environment);
	
	this->hurtMan = new LiveObject(new Md2Player("Data/Models/HurtMan/hurt_man.md2", "Data/Models/HurtMan/hurt_man.pcx"),
		Vector3(200.0f, 0.0f, -400.0f),
		Vector3(),
		5.0f,
		5.0f,
		100.0f,
		BoundingBox(Vector3(0.0f, 25.0f, 0.0f), Vector3(10.0f, 25.0f, 10.0f)),
		&this->environment, 0.2f, 1.0f);


	Texture2DManager *texMgr;
	texMgr = Texture2DManager::getInstance();

	if(this->gui != NULL)
	{
		this->lifeBar = new GUI::Bar(this->gui->box, 
			GUI::Box(Vector3(0.8f, 0.05f), Vector3(0.97f, 0.07f)),
			texMgr->load("Data/Interface/progressBarFull.tga"),
			texMgr->load("Data/Interface/progressBarEmpty.tga"));
		this->lifeBar->SetColor(Vector3(1.0f, 0.0f, 0.0f));
		this->lifeBar->SetPCurrValue(&this->player->life);
		this->lifeBar->SetIcon(texMgr->load("Data/Interface/heart_icon.tga"), 
			GUI::Box(Vector3(0.768f, 0.045f), Vector3(0.79f, 0.065f)));
	
		this->tirednessBar = new GUI::Bar(this->gui->box, 
			GUI::Box(Vector3(0.8f, 0.08f), Vector3(0.97f, 0.1f)),
			texMgr->load("Data/Interface/progressBarFull.tga"),
			texMgr->load("Data/Interface/progressBarEmpty.tga"));
		this->tirednessBar->SetColor(Vector3(1.0f, 0.0f, 0.0f));
		this->tirednessBar->SetPCurrValue(&this->player->tiredness);
		this->tirednessBar->SetIcon(texMgr->load("Data/Interface/tired_icon.tga"), 
			GUI::Box(Vector3(0.768f, 0.075f), Vector3(0.79f, 0.095f)));

		this->leftGunBullets = new GUI::Bar(this->gui->box,
			GUI::Box(Vector3(0.05f, 0.95f), Vector3(0.22f, 0.97f)),
			texMgr->load("Data/Interface/progressBarFull.tga"),
			texMgr->load("Data/Interface/progressBarEmpty.tga"),
			Vector3(0.0f, 1.0f, 0.0f),
			0.0f, this->player->leftGun->GetMaxBullets());
		this->leftGunBullets->SetColor(Vector3(1.0f, 0.0f, 0.0f));
		this->leftGunBullets->SetPCurrValue(this->player->leftGun->GetCurrBulletsPtr());
		this->leftGunBullets->SetIcon(texMgr->load("Data/Interface/bullets_icon.tga"), 
			GUI::Box(Vector3(0.018f, 0.945f), Vector3(0.04f, 0.975f)));

		this->rightGunBullets = new GUI::Bar(this->gui->box,
			GUI::Box(Vector3(0.8f, 0.95f), Vector3(0.97f, 0.97f)),
			texMgr->load("Data/Interface/progressBarFull.tga"),
			texMgr->load("Data/Interface/progressBarEmpty.tga"),
			Vector3(0.0f, 1.0f, 0.0f),
			0.0f, this->player->rightGun->GetMaxBullets());
		this->rightGunBullets->SetColor(Vector3(1.0f, 0.0f, 0.0f));
		this->rightGunBullets->SetPCurrValue(this->player->rightGun->GetCurrBulletsPtr());
		this->rightGunBullets->SetIcon(texMgr->load("Data/Interface/bullets_icon.tga"), 
			GUI::Box(Vector3(0.768f, 0.945f), Vector3(0.79f, 0.975f)));

		this->targetPointer = new GUI::TargetPointer(this->gui->box, 
			Vector3(0.6f, 0.4f), 
			texMgr->load("Data/Interface/pointerTarget.tga"), 
			&this->player->tiredness);

		this->compass = new GUI::Compass(gui->box, GUI::Box(Vector3(0.8f, 0.66f), Vector3(0.98f, 0.94f)),
			texMgr->load("Data/Interface/compass_dynamic.tga"),
			texMgr->load("Data/Interface/compass_static.tga"),
			texMgr->load("Data/Interface/point.tga"),
			this->player->GetPositionPtr(),
			this->player->GetRotationPtr(),
			Vector3(0.0f, 0.0f, -1.0f));
	}
	else
	{
		this->tirednessBar = NULL;
		this->leftGunBullets = NULL;
		this->rightGunBullets = NULL;
		this->lifeBar = NULL;
		this->targetPointer = NULL;
		this->compass = NULL;
	}

	currMission = new Mission(messageHandler);
	this->InitMissionFromFile();
	this->LoadMissonLogicsCode();
	
	this->currMission->StartMission();
	this->currMission->SetCurrObjective(0, true);

	this->timeLastShotLeft = 0;
	this->timeLastShotRight = 0;
}

void Level01::InitMissionFromFile()
{
	fstream file("Data/WestMoon.rci", ios::in);
	if(!file.is_open())
	{
		ThrowError(L"Cannot open mission initialization file.");
		return;
	}
	if(reCoIl::GoToBlock(&file, "GAMELOGIC") && reCoIl::GoToBlock(&file, "INIT"))
	{
		char _operator[128];
		while(file.good())
		{
			strcpy(_operator, reCoIl::GetNextOperator(&file));
			if(strcmp(_operator, "SETOBJECTIVES") == 0)
			{
				char objectiveList[256];
				file >> objectiveList;
				currMission->LoadObjectiveList(objectiveList);
			}
			if(strcmp(_operator, "END") == 0)
			{
				break;
			}
		}
	}
	else
	{
		ThrowError(L"reCoIl: Cannot find GAMELOGIC INIT block.");
		return;
	}
	file.close();
}

void Level01::LoadMissonLogicsCode()
{
	fstream file("Data/WestMoon.rci", ios::in);
	if(!file.is_open())
	{
		ThrowError(L"Cannot open mission initialization file.");
		return;
	}
	if(reCoIl::GoToBlock(&file, "GAMELOGIC") && reCoIl::GoToBlock(&file, "OBJECTIVECASES"))
	{
		char lineCode[512];
		file.getline(lineCode, 512);
		this->missoinLogicsCode << lineCode << " ";
	}
	else
	{
		ThrowError(L"reCoIl: Cannot find GAMELOGIC OBJECTIVECASES block.");
		return;
	}
	file.close();
}

Camera* Level01::GetPlayerCamera(Player::CameraMode mode)
{
	return this->player->GetCamera(mode);
}

void Level01::UpdatePlayerCamera()
{
	this->player->UpdateCamera();
}

void Level01::UpdateGameLogics()
{
	if(currMission != NULL && currMission->IsStarted()) 
	{
		for(unsigned i = 0; i < currMission->GetCurrObjectivesNum(); i++)
		{
			switch(this->currMission->GetCurrObjectiveIndex(i))
			{
			case 0: 
				if((this->player->GetPosition() - this->hurtMan->GetPosition()).GetLength() < 70) 
				{
					this->currMission->RemoveCurrObjective(0, false);
					this->currMission->SetCurrObjective(1, false);
					if(!PlaySound(TEXT("Data/Sounds/hurtMenSpeach.wav"), 0, 1))
						ThrowError(TEXT("Cannot play hurt men speach wave!"));
					this->manStartedTalking_time = timeGetTime();
				}
				break;
			case 1:
				if(timeGetTime() - this->manStartedTalking_time > 8000)
				{
					this->currMission->RemoveCurrObjective(1, false);
					this->currMission->SetCurrObjective(2, true);
				}
				break;
			case 2:
				if(this->environment.CheckEvent(Environment::BIRD_IS_DEAD))
				{
					this->currMission->RemoveCurrObjective(2, true);
					this->currMission->Accomplish();
					messageHandler->SetFullScreenMessage("Mission Accomplished!");
				}
				break;
			}
		}
	}
}

void Level01::Update(Keyboard *keyboard, Mouse *mouse)
{
	this->environment.Update();

	this->player->Update(keyboard, mouse);
	//this->horse->Update();

#ifdef PRESENT
	if(keyboard->GetButtonState('L'))
	{
		Sun* sunObject = this->environment.GetSun();
		sunObject->SetRotation(0.0f);
	}
#endif

	float topo = this->environment.Topo(this->player->GetPosition());
	if(topo != Environment::UNDEFINED && topo < 0.0f)
	{
		if(this->player->tiredness > 0.1f) 
			this->player->tiredness -= 0.1f;
		
		if(topo < -30.0f)
		{
			if(this->player->life > 0.5f) 
				this->player->life -= 0.5f;
		}
	}

	if(this->lifeBar != NULL) this->lifeBar->Update();
	if(this->tirednessBar != NULL) this->tirednessBar->Update();
	if(this->targetPointer != NULL) this->targetPointer->Update();
	if(this->leftGunBullets != NULL) this->leftGunBullets->Update();
	if(this->rightGunBullets != NULL) this->rightGunBullets->Update();

	if(mouse->IsLeftButtonDown() && !this->player->leftGun->IsEmpty())
	{
		int currentTime = timeGetTime();
		if(currentTime - this->timeLastShotLeft > 500)					// 500 miliseconds - the minimal interval between the shots
		{
			this->timeLastShotLeft = currentTime;

			this->player->FireLeft();

			if(!PlaySound(TEXT("Data/Sounds/MachineGun.wav"), 0, 1))
				ThrowError(TEXT("Cannot play MachineGun.wav"));

			targetPointer->ApplyForce(Vector3(0.0f, -0.03f, 0.0f));		// recoil

			Vector3 targetPosition = targetPointer->GetTargetPosition();
			if((targetPosition - currCamera->GetPosition()).GetLength() < 1000)					// dalekoboinost
				this->environment.Damage(targetPosition, 100.0f);
		}
	}
	else
	{
		this->player->DontFire();
	}

	if(mouse->IsRightButtonDown() && !this->player->rightGun->IsEmpty())
	{
		int currentTime = timeGetTime();
		if(currentTime - this->timeLastShotRight > 500)					// 500 miliseconds - the minimal interval between the shots
		{
			this->timeLastShotRight = currentTime;
			
			this->player->FireRight();

			if(!PlaySound(TEXT("Data/Sounds/MachineGun.wav"), 0, 1))
				ThrowError(TEXT("Cannot play MachineGun.wav"));

			targetPointer->ApplyForce(Vector3(0.0f, -0.03f, 0.0f));		// recoil

			Vector3 targetPosition = targetPointer->GetTargetPosition();
			if((targetPosition - currCamera->GetPosition()).GetLength() < 1000)					// dalekoboinost
				this->environment.Damage(targetPosition, 100.0f);
		}
	}
	else
	{
		this->player->DontFire();
	}

	this->UpdateGameLogics();

	if(player->life < 3.0f) messageHandler->SetFullScreenMessage("Umre");
}

void Level01::Draw(Camera *camera)
{
	this->environment.Draw(camera);
	this->player->Draw(camera, true);

	this->hurtMan->Draw(camera, false);

	this->environment.DrawSky(camera);

	//Reset the target position of the target pointer
	Vector3 targetScreenCoords = targetPointer->GetScreenPosition(mainWindow.sizeW, mainWindow.sizeH);
	targetPointer->SetTargetPosition(ScreenToOGLCoordinates((int)targetScreenCoords.x, (int)targetScreenCoords.y));
}

void Level01::Draw2D(Camera *camera)
{
	this->lifeBar->Draw(Vector3(1 - lifeBar->GetPercent(), lifeBar->GetPercent(), 0.0f));
	this->tirednessBar->Draw(Vector3(this->tirednessBar->GetPercent(), 1.0f, 0.0f));
	this->leftGunBullets->Draw(Vector3(1 - leftGunBullets->GetPercent(), leftGunBullets->GetPercent(), 0.0f));
	this->rightGunBullets->Draw(Vector3(1 - rightGunBullets->GetPercent(), rightGunBullets->GetPercent(), 0.0f));
	this->compass->Draw(camera);

	if(camera == this->GetPlayerCamera(Player::FIRST_PERSON) || 
		camera == this->GetPlayerCamera(Player::THIRD_PERSON))
		this->targetPointer->Draw();
}

void Level01::SaveToFile(const char* filename)
{
	fstream file;
	file.open(filename, ios::out | ios::binary);
	this->player->SaveToFile(&file);
	this->currMission->SaveToFile(&file);

	file.close();
}

void Level01::LoadFromFile(const char* filename)
{
	fstream file;
	file.open(filename, ios::in | ios::binary);
	if(!file.is_open())
	{
		ThrowError(TEXT("Cannot open the save file to load it."));
	}
	else
	{
		this->player->LoadFromFile(&file);
		this->currMission->LoadFromFile(&file);
	}
	file.close();
}

#endif