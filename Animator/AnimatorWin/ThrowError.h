
#ifndef TROWERROR_H
#define TROWERROR_H

#include <Windows.h>

void ThrowError(LPCWSTR errorDescribtion)
{
	MessageBox(NULL, errorDescribtion, TEXT("ERROR"), MB_OK | MB_ICONERROR);
}

#endif