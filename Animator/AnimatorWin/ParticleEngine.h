
#ifndef PARTICLE_ENGINE_H
#define PARTICLE_ENGINE_H

#include <stdlib.h>
#include <vector>
#include "..\Geometry\Geometry.h"
#include "..\Camera\Camera.h"
#include "..\Md2Model\TextureManager.h"
#include "..\Md2Model\Texture.h"
#include "Particle.h"

using namespace VectorMath;

namespace Physics
{
	class ParticleFountain
	{
	private:
		VectorMath::Vector3 position;

		Texture2D* particleTexture;
		
		vector<Particle> particles;
		VectorMath::Vector3 gravity;

		VectorMath::Vector3 direction;

		int newParticlesPerSec,
			particleLife_sec;

		float singleParticleMass;

		int widthAngle_degs;							// for faster performance and same graphics

		Vector3 color;
	public:
		ParticleFountain(VectorMath::Vector3 position, 
			VectorMath::Vector3 direction,
			int widthAngle_degs,
			Texture2D* particleTexture, 
			int newParticlesPerSec, 
			int particleLife_sec,
			Vector3 color = Vector3(1.0f, 1.0f, 1.0f),
			float singleParticleMass = 0.01,
			VectorMath::Vector3 gravity = VectorMath::Vector3(0.0f, -50.0f, 0.0f))
		{
			this->color = color;
			this->position = position;
			this->direction = direction;
			this->widthAngle_degs = widthAngle_degs;
			this->particleTexture = particleTexture;
			this->newParticlesPerSec = newParticlesPerSec;
			this->particleLife_sec = particleLife_sec;
			this->singleParticleMass = singleParticleMass;
			this->gravity = gravity;
		}

		void Update()
		{
			for(int i = this->particles.size() - 1; i >= 0; i--)
			{
				if(!this->particles[i].isAlive())
				{
					this->particles.erase(this->particles.begin() + i); 
					i++;
					continue;
				}
				else
				{
					this->particles[i].Update();
				}
			}
			
			for(int i = 0; i < this->newParticlesPerSec; i++)
			{
				this->particles.push_back(Particle(this->GetNearPosition(), this->particleLife_sec));
				this->particles[this->particles.size() - 1].SetAcceleration(this->gravity * this->singleParticleMass);
				this->particles[this->particles.size() - 1].AddVelocity(this->GetRandomDirection());
			}
		}

		void Draw(Camera *camera)
		{
			if((camera->GetPosition()-this->position).GetSqLength() < 1000000.0f)		// do not draw is further than 1000 units
			{
				glDisable(GL_LIGHTING);
				this->particleTexture->bind();
				glColor3f(this->color.x, this->color.y, this->color.z);
				for(unsigned i = 0; i < this->particles.size(); i++)
					this->particles[i].Draw(camera);
				glColor3f(1.0f, 1.0f, 1.0f);
				glEnable(GL_LIGHTING);
			}
		}

		inline VectorMath::Vector3 GetRandomDirection()
		{
			//generate three random angles between -widthAngle_degs/2 and + -widthAngle_degs/2 :float number with 3 digits precision
			float angleX = (rand() % (this->widthAngle_degs * 1000))/1000.0f - this->widthAngle_degs/2,	
				angleY = (rand() % (this->widthAngle_degs * 1000))/1000.0f - this->widthAngle_degs/2,
				angleZ = (rand() % (this->widthAngle_degs * 1000))/1000.0f - this->widthAngle_degs/2;
			
			VectorMath::Vector3 randomDirection = this->direction;
			randomDirection.RotateAroundXAxis(angleX);
			randomDirection.RotateAroundYAxis(angleY);
			randomDirection.RotateAroundZAxis(angleZ);

			return randomDirection;
		}

		inline VectorMath::Vector3 GetNearPosition()
		{
			return VectorMath::Vector3(this->position.x + (rand()%((int)(PSIZE*20)))/10.0f - PSIZE, 
										this->position.y + (rand()%((int)(PSIZE*20)))/10.0f - PSIZE,
										this->position.z + (rand()%((int)(PSIZE*20)))/10.0f - PSIZE);
		}

		inline void ChangeDirection(VectorMath::Vector3 newDirection)
		{
			this->direction = newDirection;
		}

		VectorMath::Vector3 GetDirection() { return this->direction; }

	};
}

#endif
