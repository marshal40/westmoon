#ifdef TS_WINDOWSBASE_H
#error 'WindowBase.h" declared twice
#else

#define TS_WINDOWSBASE_H

Vector3 guiDimensions;

bool pauseMode;

Mouse mouse;
Camera *currCamera;
Camera staticCamera,
	staticCamera2;
Keyboard keyboard;

#define MOUSE_SPEED 5

#include "Timer.h"

float glob_AngleOfView_degs = 45.0f,			//for Reshape func
	glob_NearPlaneDist = 10.0f,
	glob_FarPlaneDist = 5000.0f;

#include "MainWindow.h"
#include "ThrowError.h"

MainWindow mainWindow;

//----------------------------Window-

//Graphics funcs
int InitGL(GLvoid);
GLvoid ReSizeGLScene(GLsizei width, GLsizei height);


void UpdateCamera();
void ApplyCamera();

//Window funcs
VectorMath::Vector3 ScreenToOGLCoordinates(int x, int y);
void SetWindowTitle(const char *formated, ...);

BOOL CreateGLWindow(LPCWSTR title, int width, int height, int bits, bool fullscreenflag);
void KillGLWindow();
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

void MainLoop();

//------------------------------------------------------------------------------------------


// Window funcs' declarations
inline void WarpPointer(int posX_window, int posY_window)						//Window relative coordinates
{
	POINT point;
	point.x = posX_window;
	point.y = posY_window;

	ClientToScreen(mainWindow.hWnd, &point);
	SetCursorPos(point.x, point.y);

	mouse.SetCurrentPosition(VectorMath::Vector3((float)posX_window, (float)posY_window));
}
inline void ShowPointer()
{
	while(ShowCursor(TRUE) <= 1) ;
}
inline void HidePointer()
{
	while(ShowCursor(FALSE) >= 0) ;
}

void UpdateStaticCamera()
{
	if (mouse.IsLeftButtonDown())
		staticCamera.MoveForward(MOUSE_SPEED);

	if (mouse.IsRightButtonDown())
		staticCamera.MoveForward(-MOUSE_SPEED);

	if(keyboard.GetKeyState('q') || keyboard.GetKeyState('Q'))
		staticCamera.MoveRight(-MOUSE_SPEED);

	if(keyboard.GetKeyState('e') || keyboard.GetKeyState('E'))
		staticCamera.MoveRight(MOUSE_SPEED);


	float mouseDeltaX = mouse.GetCurrentPosition().x - mouse.GetLastPosition().x;
	float mouseDeltaY = -(mouse.GetCurrentPosition().y - mouse.GetLastPosition().y);

	if(mouseDeltaX > 0)
		staticCamera.LookRight (mouseDeltaX);
	else
		staticCamera.LookLeft (-mouseDeltaX);

	if(mouseDeltaY > 0)
		staticCamera.LookUp (mouseDeltaY);
	else
		staticCamera.LookDown (-mouseDeltaY);	
}

void UpdateCamera ()
{
	if(currCamera == &staticCamera)
		UpdateStaticCamera();	

	if(currCamera != &staticCamera2)
	{
		WarpPointer(mainWindow.sizeW/2, mainWindow.sizeH/2);
		mouse.OverrideLastPosition (VectorMath::Vector3((float)(mainWindow.sizeW/2), (float)(mainWindow.sizeH/2)));
	}
}

void InitializeObjects();
void Update();
void Draw();															
void Draw2D();

Vector3 nextClearColor = Vector3(0.75f, 1.0f, 1.0f);

void Display()
{
	glClearColor (nextClearColor.x, nextClearColor.y, nextClearColor.z, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	
	glPushMatrix();
		
	currCamera->ApplyLook();
	Draw();
	mouse.SetCurrent3DPosition(ScreenToOGLCoordinates((int)mouse.GetCurrentPosition().x,
														(int)mouse.GetCurrentPosition().y));
	glPopMatrix();

	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	currCamera->ApplyLook();
	glPopMatrix();
	
	glDisable(GL_DEPTH_TEST);
	Draw2D();
	glEnable(GL_DEPTH_TEST);

	mouse.SetCurrent2DPosition(
		Vector3( (mouse.GetCurrentPosition().x * guiDimensions.x * 2 ) / mainWindow.sizeW - guiDimensions.x,
					- ((mouse.GetCurrentPosition().y * guiDimensions.y * 2 ) / mainWindow.sizeH - guiDimensions.y)));
	SwapBuffers(mainWindow.hDC);
}

void InitializeCamera()
{
	staticCamera.SetPosition(VectorMath::Vector3(0.0, 30.0, 100.0));

	staticCamera2.SetPosition(VectorMath::Vector3(800.0, 250.0, -100.0));
	staticCamera2.SetRotation(VectorMath::Vector3(-10.0, 60.0, 0.0));
	
	currCamera = &staticCamera2;
}

VectorMath::Vector3 ScreenToOGLCoordinates(int x, int y)
{
    GLint viewport[4];
    GLdouble modelview[16];
    GLdouble projection[16];
    GLfloat winX, winY, winZ;
    double posX, posY, posZ;
 
    glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
    glGetDoublev( GL_PROJECTION_MATRIX, projection );
    glGetIntegerv( GL_VIEWPORT, viewport );
 
    winX = (float)x;
    winY = (float)viewport[3] - (float)y;

	glReadPixels( x, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ );
 
    gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);
	
    return VectorMath::Vector3((float)posX, (float)posY, (float)posZ);
}

void SetWindowTitle(const char *formated, ...)
{
	char text[256];
	va_list ap;

	if(formated == NULL) return;

	va_start(ap, formated);	
	    vsprintf_s(text, formated, ap);
	va_end(ap);

	size_t textSize = strlen(text) + 1;
    const size_t newsize = 100;
    size_t convertedChars = 0;
    wchar_t wcstring[newsize];
    mbstowcs_s(&convertedChars, wcstring, textSize, text, _TRUNCATE);
    //wcscat_s(wcstring, L" (wchar_t *)");

	SetWindowText(mainWindow.hWnd, wcstring);
}


void KillGLWindow()																		// Properly Kill The Window
{
	if (mainWindow.fullscreen)															// Are We In Fullscreen Mode?
		ChangeDisplaySettings(NULL,0);													// If So Switch Back To The Desktop

	if (mainWindow.hRC)																	// Do We Have A Rendering Context?
	{
		if (!wglMakeCurrent(NULL,NULL))													// Are We Able To Release The DC And RC Contexts?
			MessageBox(NULL,TEXT("Release Of DC And RC Failed."), TEXT("SHUTDOWN ERROR"),MB_OK | MB_ICONINFORMATION);

		if (!wglDeleteContext(mainWindow.hRC))											// Are We Able To Delete The RC?
			MessageBox(NULL,TEXT("Release Rendering Context Failed."),TEXT("SHUTDOWN ERROR"),MB_OK | MB_ICONINFORMATION);
		
		mainWindow.hRC=NULL;															
	}

	if (mainWindow.hDC && !ReleaseDC(mainWindow.hWnd, mainWindow.hDC))					// Are We Able To Release The DC
	{
		MessageBox(NULL,TEXT("Release Device Context Failed."),TEXT("SHUTDOWN ERROR"),MB_OK | MB_ICONINFORMATION);
		mainWindow.hDC=NULL;													
	}

	if (mainWindow.hWnd && !DestroyWindow(mainWindow.hWnd))								// Are We Able To Destroy The Window?
	{
		MessageBox(NULL,TEXT("Could Not Release hWnd."),TEXT("SHUTDOWN ERROR"),MB_OK | MB_ICONINFORMATION);
		mainWindow.hWnd=NULL;															
	}

	if (!UnregisterClass(TEXT("OpenGL"),mainWindow.hInstance))							// Are We Able To Unregister Class
	{
		MessageBox(NULL,TEXT("Could Not Unregister Class."),TEXT("SHUTDOWN ERROR"),MB_OK | MB_ICONINFORMATION);
		mainWindow.hInstance=NULL;														
	}
}
 
BOOL CreateGLWindow(LPCWSTR title, int width, int height, int bits, bool fullscreenflag)
{
	unsigned	PixelFormat;									// Holds The Results After Searching For A Match
	WNDCLASS	wc;												// Windows Class Structure
	DWORD		dwExStyle;										// Window Extended Style
	DWORD		dwStyle;										// Window Style
	RECT		WindowRect;										// Grabs Rectangle Upper Left / Lower Right Values
	WindowRect.left=(long)0;									// Set Left Value To 0
	WindowRect.right=(long)width;								// Set Right Value To Requested Width
	WindowRect.top=(long)0;										// Set Top Value To 0
	WindowRect.bottom=(long)height;								// Set Bottom Value To Requested Height

	mainWindow.fullscreen=fullscreenflag;						// Set The Global Fullscreen Flag

	mainWindow.hInstance= GetModuleHandle(NULL);				// Grab An Instance For Our Window
	wc.style			= CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// Redraw On Size, And Own DC For Window.
	wc.lpfnWndProc		= (WNDPROC) WndProc;					// WndProc Handles Messages
	wc.cbClsExtra		= 0;									// No Extra Window Data
	wc.cbWndExtra		= 0;									// No Extra Window Data
	wc.hInstance		= mainWindow.hInstance;					// Set The Instance
	wc.hIcon			= LoadIcon(NULL, IDI_WINLOGO);			// Load The Default Icon
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);			// Load The Arrow Pointer
	wc.hbrBackground	= NULL;									// No Background Required For GL
	wc.lpszMenuName		= NULL;									// We Don't Want A Menu
	wc.lpszClassName	= TEXT("OpenGL");						// Set The Class Name

	if (!RegisterClass(&wc))									// Throw error if can't register window class
	{
		MessageBox(NULL,TEXT("Failed To Register The Window Class."),TEXT("ERROR"),MB_OK|MB_ICONEXCLAMATION);
		return FALSE;	
	}
	
	if (mainWindow.fullscreen)
	{
		DEVMODE dmScreenSettings;								// Device Mode
		memset(&dmScreenSettings,0,sizeof(dmScreenSettings));	// Makes Sure Memory's Cleared
		dmScreenSettings.dmSize=sizeof(dmScreenSettings);		// Size Of The Devmode Structure
		dmScreenSettings.dmPelsWidth	= width;				// Selected Screen Width
		dmScreenSettings.dmPelsHeight	= height;				// Selected Screen Height
		dmScreenSettings.dmBitsPerPel	= bits;					// Selected Bits Per Pixel
		dmScreenSettings.dmFields=DM_BITSPERPEL|DM_PELSWIDTH|DM_PELSHEIGHT;

		if (ChangeDisplaySettings(&dmScreenSettings,CDS_FULLSCREEN)!=DISP_CHANGE_SUCCESSFUL)
		{
			// If The Mode Fails, Offer Two Options.  Quit Or Use Windowed Mode.
			MessageBox(NULL,TEXT("The Requested Fullscreen Mode Is Not Supported.\nPossible solutions are:\n1. Restart WestMoon and run in windowed mode. \n2. Use WestMoonCofigurator.exe to sync the rosolution with your system."),
				TEXT("TS WestMoon"),MB_OK|MB_ICONSTOP);
			
			fstream file("window.ini", ios::out);
			file << "WestMoon\n640 400\n10 10\n0\n";
			file.close();
			system("echo WestMoon Configurator is trying to start...");
			system("ConfigurateWestMoon.exe");

			MessageBox(NULL,TEXT("Program Will Now Close."),TEXT("ERROR"),MB_OK|MB_ICONSTOP);
			return FALSE;	
		}
	}

	if (mainWindow.fullscreen)				
	{
		dwExStyle=WS_EX_APPWINDOW;	
		dwStyle=WS_POPUP;		
	}
	else
	{
		dwExStyle=WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;						// Window Extended Style
		dwStyle=WS_OVERLAPPEDWINDOW;										// Windows Style
	}

	AdjustWindowRectEx(&WindowRect, dwStyle, FALSE, dwExStyle);		// Adjust Window To True Requested Size

	// Create The Window
	if (!(mainWindow.hWnd=CreateWindowEx(	dwExStyle,				// Extended Style For The Window
								TEXT("OpenGL"),						// Class Name
								title,								// Window Title
								dwStyle |							// Defined Window Style
								WS_CLIPSIBLINGS |					// Required Window Style
								WS_CLIPCHILDREN,					// Required Window Style
								0, 0,								// Window Position
								WindowRect.right-WindowRect.left,	// Calculate Window Width
								WindowRect.bottom-WindowRect.top,	// Calculate Window Height
								NULL,								// No Parent Window
								NULL,								// No Menu
								mainWindow.hInstance,				// Instance
								NULL)))								// Dont Pass Anything To WM_CREATE
	{
		KillGLWindow();		
		MessageBox(NULL,TEXT("Window Creation Error."),TEXT("ERROR"),MB_OK|MB_ICONEXCLAMATION);
		return FALSE;		
	}

	static	PIXELFORMATDESCRIPTOR pfd=		
	{
		sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
		1,											// Version Number
		PFD_DRAW_TO_WINDOW |						// Format Must Support Window
		PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,							// Must Support Double Buffering
		PFD_TYPE_RGBA,								// Request An RGBA Format
		bits,										// Select Our Color Depth
		0, 0, 0, 0, 0, 0,							// Color Bits Ignored
		0,											// No Alpha Buffer
		0,											// Shift Bit Ignored
		0,											// No Accumulation Buffer
		0, 0, 0, 0,									// Accumulation Bits Ignored
		32,											// 32Bit Depth Buffer		
		0,											// No Stencil Buffer
		0,											// No Auxiliary Buffer
		PFD_MAIN_PLANE,								// Main Drawing Layer
		0,											// Reserved
		0, 0, 0										// Layer Masks Ignored
	};
	
	if (!(mainWindow.hDC=GetDC(mainWindow.hWnd)))				// Throw error if devide context can't be created
	{
		KillGLWindow();						
		MessageBox(NULL, TEXT("Can't Create A GL Device Context."),TEXT("ERROR"),MB_OK|MB_ICONEXCLAMATION);
		return FALSE;
	}

	if (!(PixelFormat=ChoosePixelFormat(mainWindow.hDC,&pfd)))	// Throw error is can't be found pixel format
	{
		KillGLWindow();	
		MessageBox(NULL, TEXT("Can't Find A Suitable PixelFormat."), TEXT("ERROR"),MB_OK|MB_ICONEXCLAMATION);
		return FALSE;
	}

	if(!SetPixelFormat(mainWindow.hDC,PixelFormat,&pfd))		// Throw error if can't set pixelformat
	{
		KillGLWindow();	
		MessageBox(NULL,TEXT("Can't Set The PixelFormat."),TEXT("ERROR"),MB_OK|MB_ICONEXCLAMATION);
		return FALSE;
	}

	if (!(mainWindow.hRC=wglCreateContext(mainWindow.hDC)))		// Throw error if can't create the rendering contex
	{
		KillGLWindow();	
		MessageBox(NULL,TEXT("Can't Create A GL Rendering Context."),TEXT("ERROR"),MB_OK|MB_ICONEXCLAMATION);
		return FALSE;
	}

	if(!wglMakeCurrent(mainWindow.hDC, mainWindow.hRC))			// Throw error if can't activate the rendering contex
	{
		KillGLWindow();
		MessageBox(NULL,TEXT("Can't Activate The GL Rendering Context."),TEXT("ERROR"),MB_OK|MB_ICONEXCLAMATION);
		return FALSE;
	}

	ShowWindow(mainWindow.hWnd, SW_SHOW);
	SetForegroundWindow(mainWindow.hWnd);								
	SetFocus(mainWindow.hWnd);
	ReSizeGLScene(width, height);

	if (!InitGL())
	{
		KillGLWindow();
		MessageBox(NULL,TEXT("Initialization Failed."), TEXT("ERROR"),MB_OK|MB_ICONEXCLAMATION);
		return FALSE;
	}

	HidePointer();
	
	return TRUE;
}

LRESULT CALLBACK WndProc(	HWND	hWnd,			// Handle For This Window
							UINT	uMsg,			// Message For This Window
							WPARAM	wParam,			// Additional Message Information
							LPARAM	lParam)			// Additional Message Information
{
	switch (uMsg)									
	{
		case WM_ACTIVATE:
		{
			if ((LOWORD(wParam) != WA_INACTIVE) && !((BOOL)HIWORD(wParam)))// !HIWORD(wParam))					// Check Minimization State
				mainWindow.active=TRUE;
			else
			{
				mainWindow.active=FALSE;						// Program Is No Longer Active
				currCamera = &staticCamera2;
				pauseMode = true;
				ShowPointer();
			}

			return 0;
		}

		case WM_SYSCOMMAND:							// Intercept System Commands
		{
			switch (wParam)		
			{
				case SC_SCREENSAVE:					// Screensaver trying to start
				case SC_MONITORPOWER:				// Monitor trying to enter powersave mode
				return 0;							
			}
			return 0;								
		}

		case WM_CLOSE:								
		{
			PostQuitMessage(0);						// Send A Quit Message
			exit(0);
			return 0;							
		}

		//Keyboard events
		case WM_KEYDOWN:							
			keyboard.SetState(wParam, true);
			return 0;

		case WM_KEYUP:								
			keyboard.SetState(wParam, false);
			return 0;								
		

		//Mouse events
		case WM_MOUSEMOVE:
			POINT* mousePosition;
			mousePosition= new POINT;
			if(GetCursorPos(mousePosition))
			{
				ScreenToClient(hWnd, mousePosition);
				mouse.SetCurrentPosition(VectorMath::Vector3((float)mousePosition->x, (float)mousePosition->y));
			}
			return 0;

		case WM_LBUTTONDOWN:
			mouse.PressLeftButton();
			return 0;

		case WM_LBUTTONUP:
			mouse.ReleaseLeftButton();
			return 0;

		case WM_RBUTTONDOWN:
			mouse.PressRightButton();
			return 0;

		case WM_RBUTTONUP:
			mouse.ReleaseRightButton();
			return 0;

		case WM_MBUTTONDOWN:
			return 0;

		case WM_MBUTTONUP:
			return 0;

		//Window events
		case WM_SIZE:								// Resize The OpenGL Window
		{
			ReSizeGLScene(LOWORD(lParam),HIWORD(lParam));  // LoWord=Width, HiWord=Height
			return 0;
		}
		
		default: 
			return DefWindowProc(hWnd,uMsg,wParam,lParam);	// Pass All Unhandled Messages To DefWindowProc

	}
}


int WINAPI WinMain(	HINSTANCE	hInstance,			// Instance
					HINSTANCE	hPrevInstance,		// Previous Instance
					LPSTR		lpCmdLine,			// Command Line Parameters
					int			nCmdShow)			// Window Show State
{
	MSG	msg;										// message

	mainWindow.LoadFromFile("window.ini");

	if (!CreateGLWindow(mainWindow.name, mainWindow.sizeW, mainWindow.sizeH, 16, mainWindow.fullscreen))
	{
		ThrowError(TEXT("Cannot create the window"));
		return 0;									
	}

	InitializeCamera();															
	WarpPointer(mainWindow.sizeW/2, mainWindow.sizeH/2);									
	mouse.SetCurrentPosition(VectorMath::Vector3((float)(mainWindow.sizeW/2), (float)(mainWindow.sizeH/2))); 
	mouse.OverrideLastPosition(VectorMath::Vector3(float(mainWindow.sizeW/2), (float)(mainWindow.sizeH/2))); 

	InitializeObjects();

	Timer::beginFrameTime = timeGetTime();

	while(true)									
	{
		if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))	// Is There A Message Waiting?
		{
			if (msg.message==WM_QUIT)
			{
				break;
			}
													// If Not, Deal With Window Messages
			TranslateMessage(&msg);					// Translate The Message
			DispatchMessage(&msg);					// Dispatch The Message
		}

		if(!mainWindow.active) continue;
		Update();

		Display();	
			
		UpdateCamera();

		if(Timer::useConstantFps)
		{
			Timer::endFrameTime = Timer::FRAME_MS + Timer::beginFrameTime - timeGetTime();
			if(Timer::endFrameTime > 0) Sleep(Timer::endFrameTime);
		}

		Timer::frameDuration_sec = timeGetTime() - Timer::beginFrameTime;
		Timer::beginFrameTime = timeGetTime();
	}

	KillGLWindow();										
	return (msg.wParam);
}



//Graphics functions' definitions

GLvoid ReSizeGLScene(GLsizei width, GLsizei height)	
{
	if (height==0)	height = 1;					

	glViewport(0, 0, width, height);
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity();


	double aspectRatio = ((float)width) / ((float)height);

	gluPerspective(glob_AngleOfView_degs, aspectRatio, glob_NearPlaneDist, glob_FarPlaneDist);
	glMatrixMode (GL_MODELVIEW);
}

GLfloat whiteSpecularLight[] = {1.0, 1.0, 1.0}; 
GLfloat blackAmbientLight[] = {0,0, 0}; 
GLfloat whiteDiffuseLight[] = {1.0, 1.0, 1.0}; 
GLfloat lightPosition[]=	{ 50.0f, 1000.0f, 0.0f, 1.0f };

int InitGL(GLvoid)										
{
	glEnable (GL_DEPTH);
	glEnable (GL_DEPTH_TEST);
	glEnable (GL_COLOR_MATERIAL);
	glEnable (GL_LIGHTING);
	glLightfv(GL_LIGHT0, GL_SPECULAR, whiteSpecularLight);
	glLightfv(GL_LIGHT0, GL_AMBIENT, blackAmbientLight);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, whiteDiffuseLight);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);	// Position The Light

	glEnable (GL_LIGHT0);
	glEnable (GL_TEXTURE_2D);
	glEnable (GL_CULL_FACE);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	//glBlendFunc(GL_SRC_ALPHA,GL_ONE);
	//glShadeModel(GL_SMOOTH);							// Enables Smooth Color Shading
	//glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
	//glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	//glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);

	glCullFace(GL_BACK);

	return TRUE;										
}

#endif