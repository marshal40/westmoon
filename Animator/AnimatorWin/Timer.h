
#ifndef TIMER_H
#define TIMER_H

struct Timer
{
	static const bool useConstantFps = true;

	static const short FRAMES_PER_SEC = 30;
	static const short FRAME_MS = 1000/FRAMES_PER_SEC;

	static time_t beginFrameTime;	
	static time_t endFrameTime;

	static const int frameRate = 7;
	static float frameDuration_sec;

}; 

time_t Timer::beginFrameTime;
time_t Timer::endFrameTime;
float Timer::frameDuration_sec;

#endif