
#ifndef TS_GUN_H
#define TS_GUN_H

class Gun
{
private:
	float maxBullets;
	float currBullets;
public:

	Gun(float maxBullets, float currBullets = 0.0f);
	float Reload();						// return the number of the new bullets
	float Fire();						// return the number of the bullets left (before fireing)
	
	float GetMaxBullets() const;
	float* GetCurrBulletsPtr();

	bool IsEmpty();
};

Gun::Gun(float maxBullets, float currBullets)
{
	this->maxBullets = maxBullets;
	this->currBullets = currBullets;
}

float Gun::Reload()
{
	float newBullets = this->maxBullets - this->currBullets;
	this->currBullets = this->maxBullets;

	return newBullets;
}

float Gun::Fire()
{
	if(this->currBullets > 0.0f) return this->currBullets--;
	else return 0.0f;
}

float Gun::GetMaxBullets() const
{
	return this->maxBullets;
}

float* Gun::GetCurrBulletsPtr()
{
	return &this->currBullets;
}

bool Gun::IsEmpty()
{
	return this->currBullets == 0.0f;
}

#endif