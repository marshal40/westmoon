
#include <cstdlib>
#include <fstream>

#include <Windows.h>
#include <gl/glew.h>
#include <gl/freeglut.h>

#include <ctime>
#include <utility>

#include "../reCoIl/reCoIl.h"
reCoIl::VariablesMap reCoIlVars;

#include "../Geometry/Geometry.h"
#include "../Camera/Camera.h"
#include "../Mouse/Mouse.h"
#include "../Keyboard/Keyboard.h"

#include "WindowsBase.h"

#include "../Md2Model/Md2Player.h"
#include "../Md2Model/Texture.h"
#include "../Md2Model/TextureManager.h"

#include "ParticleEngine.h"
#include "../Environment/EnvironmentData.h"
#include "../Environment/Environment.h"

#include "Player.h"

#include "Level.h"

#include "../GUI/GUI.h"						
#include "../GUI/BitmapFont.h"
#include "../GUI/GameMessage.h"
#include "../GUI/TargetPointer.h"

using namespace VectorMath;

#include "Level01.h"
Level *currLevel;

#include "WestMoonGUI.h"
#include "Initializations.h"

void DestructObjects()
{
	delete currLevel;
}

inline void SwitchCamera();

void Update()
{
	if(keyboard.GetKeyState(VK_F2))						// emergency exit
		exit(0);

	if(pauseMode) westMoonGUI->UpdateGUI();
	else
	{
		if(keyboard.GetButtonState(VK_ESCAPE))
		{
			pauseMode = true;
			westMoonGUI->currWindow = westMoonGUI->gui->GetWindow("mainMenu");
			currCamera = &staticCamera2;
			ShowPointer();
		}	

		if(keyboard.GetButtonState('C')) SwitchCamera();
		
		currLevel->Update(&keyboard, &mouse);
		westMoonGUI->messageHandler->Update();
	}
	
	keyboard.Update();
}

void Draw()
{
	if(!pauseMode)
	{
		float sunRotation = currLevel->GetEnvironment()->GetSun()->GetRotationAroundGlobe();

		if(sunRotation > 100.0f && sunRotation < 260.0f) glColor3f(0.0f, 0.0f, 0.0f);
		else glColor3f(1.0f, 1.0f, 1.0f);

		if(sunRotation > 180.0f) sunRotation = 360 - sunRotation;
		if(sunRotation > 150.0f) sunRotation = 150.0f;
		sunRotation = 150 - sunRotation;
		sunRotation /= 150.0f;
		nextClearColor = Vector3(0.7f*sunRotation + 0.075f, 0.95f*sunRotation + 0.1f, 0.95f*sunRotation + 0.1f);

		if(sunRotation > 0.425f) glColor3f(1.0f, 1.0f, 1.0f);
		else glColor3f(sunRotation*2 + 0.15f, sunRotation*2 + 0.15f, sunRotation*2 + 0.15f);

		Vector3 sunPosition_v3 = currLevel->GetEnvironment()->GetSun()->GetPosition();
		GLfloat sunPosition_v4[4] = {sunPosition_v3.x, sunPosition_v3.y, sunPosition_v3.z, 1.0f}; 

		glLightfv(GL_LIGHT0, GL_SPECULAR, whiteSpecularLight);
		glLightfv(GL_LIGHT0, GL_AMBIENT, blackAmbientLight);
		glLightfv(GL_LIGHT0, GL_DIFFUSE, whiteDiffuseLight);
		glLightfv(GL_LIGHT0, GL_POSITION, sunPosition_v4);
		
		glNormal3f(0.0f, 1.0f, 0.0f);

		currLevel->Draw(currCamera);
		westMoonGUI->lucidaConsole->Printf(50.0f, VectorMath::Vector3(0.0f, 200.0f, 0.0f), "Hello cruel world!");
		glDisable(GL_LIGHTING);
	}
}

void Draw2D()
{
	glDisable(GL_LIGHTING);	
	glEnable(GL_TEXTURE_2D);

	if(pauseMode)
	{
		westMoonGUI->currWindow->Draw();
		if(westMoonGUI->currWindow == westMoonGUI->gui->GetWindow("objectiveMenu"))
		{
			if(currLevel->currMission != NULL)
				currLevel->currMission->PrintObjectives(westMoonGUI->gui->box, westMoonGUI->lucidaConsole);
		}
		else if(westMoonGUI->currWindow == westMoonGUI->gui->GetWindow("loadGameMenu") || westMoonGUI->currWindow == westMoonGUI->gui->GetWindow("saveGameMenu"))
		{
			for(int i = 0; i < numberOfSaves; i++)
				saves[i]->Draw();
		}
	}
	else
	{
		westMoonGUI->messageHandler->Draw();
		
		if(currLevel != NULL)
			currLevel->Draw2D(currCamera);
	}
	glEnable(GL_LIGHTING);
}

inline void SwitchCamera()
{
	if(currCamera == &staticCamera) currCamera = &staticCamera2;
	else if(currCamera == currLevel->GetPlayerCamera(Player::THIRD_PERSON)) currCamera = currLevel->GetPlayerCamera(Player::FIRST_PERSON);
	else if(currCamera == currLevel->GetPlayerCamera(Player::FIRST_PERSON)) currCamera = currLevel->GetPlayerCamera(Player::THIRD_PERSON); //currCamera = &staticCamera2;
	else if(currCamera == &staticCamera2)
	{
		currCamera = currLevel->GetPlayerCamera(Player::THIRD_PERSON);
		currLevel->UpdatePlayerCamera();
	}
}