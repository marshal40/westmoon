
#ifndef PARTICLE_H
#define PARTICLE_H

#include <gl/glew.h>
#include "..\Geometry\Geometry.h"

namespace Physics
{
	#define PSIZE 0.5f

	class Particle
	{
	protected:
		VectorMath::Vector3 position;
		VectorMath::Vector3 velocity, 
			acceleration;

		int life_sec;

	public:
		Particle(VectorMath::Vector3 position, int life_sec)
		{
			this->position = position;
			this->velocity = VectorMath::Vector3();
			this->acceleration = VectorMath::Vector3();
			this->life_sec = life_sec;
		}

		void Update()
		{
			this->velocity = this->velocity + this->acceleration;
			this->position = this->position + this->velocity;

			if(this->life_sec > 0) this->life_sec -= 1;
		}

		void TranslateToPosition() { glTranslatef(this->position.x, this->position.y, this->position.z); }
		void DrawParticle()
		{
			glBegin(GL_TRIANGLE_STRIP);
			glTexCoord2d(1,1); glVertex3f(+ PSIZE, + PSIZE, 0.0f); 
			glTexCoord2d(0,1); glVertex3f(- PSIZE, + PSIZE, 0.0f); 
			glTexCoord2d(1,0); glVertex3f(+ PSIZE, - PSIZE, 0.0f); 
			glTexCoord2d(0,0); glVertex3f(- PSIZE, - PSIZE, 0.0f); 
			glEnd();
		}

		void Draw(Camera* camera)
		{	
			glPushMatrix();
			glTranslatef(this->position.x, this->position.y, this->position.z);
			camera->DeapplyOrientation();
			glBegin(GL_TRIANGLE_STRIP);
			glTexCoord2d(1,1); glVertex3f(+ PSIZE, + PSIZE, 0.0f); 
			glTexCoord2d(0,1); glVertex3f(- PSIZE, + PSIZE, 0.0f); 
			glTexCoord2d(1,0); glVertex3f(+ PSIZE, - PSIZE, 0.0f); 
			glTexCoord2d(0,0); glVertex3f(- PSIZE, - PSIZE, 0.0f);
			glEnd();
			glPopMatrix();
		}

		inline bool isAlive()
		{
			return this->life_sec != 0;
		}

		// Set / Get Fucs
		inline void AddAcceleration(VectorMath::Vector3 a)
		{
			this-> acceleration = this->acceleration + a;
		}

		inline void SetAcceleration(VectorMath::Vector3 a)
		{
			this->acceleration = a;
		}

		inline void AddVelocity(VectorMath::Vector3 v)
		{
			this->velocity = this->velocity + v;
		}

		inline void SetVelocity(VectorMath::Vector3 v)
		{
			this->velocity = v;
		}

		inline VectorMath::Vector3 GetVelocity()
		{
			return this->velocity;
		}

		inline VectorMath::Vector3 GetAcceleration()
		{
			return this->acceleration;
		}

	};

}


#endif