// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the KEYBOARD_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// KEYBOARD_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef KEYBOARD_EXPORTS
#define KEYBOARD_API __declspec(dllexport)
#else
#define KEYBOARD_API __declspec(dllimport)
#endif

#define KEY_STATE_PRESSED true
#define KEY_STATE_RELEASED false

#include <iostream>
#pragma once

class KEYBOARD_API Keyboard
{
private:
	bool keyStates[256],							// keyState[i] is true when (char)i is pressed and false when (char)i is released
		buttonStates[256],
		lastKeyStates[256];
	
public:
	Keyboard();

	bool GetKeyState (char key) { return this->keyStates[key]; }
	bool GetLastKetState (char key) { return this->lastKeyStates[key]; }
	bool GetButtonState (char key) { return this->buttonStates[key]; }

	void SetState (char key, bool state);
	void Update();
};
