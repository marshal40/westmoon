
#include "stdafx.h"
#include "Camera.h"

Camera::Camera()
{
	this->currentPos = VectorMath::Vector3();
	this->currentEulerRotation_degs = VectorMath::Vector3();
	this->fwdVector = VectorMath::Vector3(0, 0, -1);
	this->upVector = VectorMath::Vector3(0, 1, 0);
	this->rightVector = VectorMath::Vector3(1, 0, 0);
}

void Camera::SetRotation (VectorMath::Vector3 newEulerRotation_degs)
{
	this->currentEulerRotation_degs = newEulerRotation_degs;
	if(this->currentEulerRotation_degs.x > 90)
	{
		this->currentEulerRotation_degs.x = 90;
	}
	else if(this->currentEulerRotation_degs.x < -90)
	{
		this->currentEulerRotation_degs.x = (-90);
	}

	if(this->currentEulerRotation_degs.x > 180) 
		this->currentEulerRotation_degs.x = 360 - this->currentEulerRotation_degs.x;
	else if (this->currentEulerRotation_degs.x < -180) 
		this->currentEulerRotation_degs.x = 360 + this->currentEulerRotation_degs.x;

	if (this->currentEulerRotation_degs.y < -180.0f) 
		this->currentEulerRotation_degs.y = 360.0f + this->currentEulerRotation_degs.y;
			
	VectorMath::Vector3 fVect (0, 0, -1),
		uVect (0, 1, 0),
		rVect (1, 0, 0);

	fVect.RotateAroundXAxis (this->currentEulerRotation_degs.x);
	fVect.RotateAroundYAxis (this->currentEulerRotation_degs.y);
	fVect.RotateAroundZAxis (this->currentEulerRotation_degs.z);

	uVect.RotateAroundXAxis (this->currentEulerRotation_degs.x);
	uVect.RotateAroundYAxis (this->currentEulerRotation_degs.y);
	uVect.RotateAroundZAxis (this->currentEulerRotation_degs.z);

	rVect.RotateAroundXAxis (this->currentEulerRotation_degs.x);
	rVect.RotateAroundYAxis (this->currentEulerRotation_degs.y);
	rVect.RotateAroundZAxis (this->currentEulerRotation_degs.z);

	fVect.Normalize();
	uVect.Normalize();
	rVect.Normalize();

	this->fwdVector = fVect;
	this->upVector = uVect;
	this->rightVector = rVect;
}

void Camera::ApplyOrientation()
{
	glRotatef (-this->currentEulerRotation_degs.x, 1.0, 0.0, 0.0);
	glRotatef (-this->currentEulerRotation_degs.y, 0.0, 1.0, 0.0);
	glRotatef (-this->currentEulerRotation_degs.z, 0.0, 0.0, 1.0);
}

void Camera::DeapplyOrientation()
{
	glRotatef (this->currentEulerRotation_degs.x, 1.0, 0.0, 0.0);
	glRotatef (this->currentEulerRotation_degs.y, 0.0, 1.0, 0.0);
	glRotatef (this->currentEulerRotation_degs.z, 0.0, 0.0, 1.0);
}

void Camera::ApplyLook()
{
	ApplyOrientation();
	glTranslatef (-this->currentPos.x, -this->currentPos.y, -this->currentPos.z);
}

void Camera::MoveForward (float units)
{
	this->currentPos = this->currentPos + (this->fwdVector*units);
}

void Camera::MoveRight (float units)
{
	this->currentPos = this->currentPos + (this->rightVector*units);
}

void Camera::MoveUp (float units)
{
	this->currentPos = this->currentPos + (this->upVector*units);
}

void Camera::LookRight (float absDegrees)
{
	VectorMath::Vector3 newRotation = this->currentEulerRotation_degs;
	newRotation.y = newRotation.y - absDegrees;

	this->SetRotation ( newRotation );
}

void Camera::LookLeft (float absDegrees)
{	
	VectorMath::Vector3 newRotation = this->currentEulerRotation_degs;
	newRotation.y = newRotation.y + absDegrees;
		
	this->SetRotation ( newRotation );
}

void Camera::LookUp (float absDegrees)
{
	VectorMath::Vector3 newRotation = this->currentEulerRotation_degs;
	newRotation.x =  newRotation.x + fabs(absDegrees) ;

	this->SetRotation ( newRotation );
}

void Camera::LookDown (float absDegrees)
{
	VectorMath::Vector3 newRotation = this->currentEulerRotation_degs;
	newRotation.x =  newRotation.x - fabs(absDegrees);
	this->SetRotation ( newRotation );
}

bool Camera::IsVisible(VectorMath::Vector3 point)
{
	VectorMath::Vector3 vA = point - this->currentPos,
		vB = this->fwdVector;
	float angleCosine = VectorMath::Vector3::DotProduct(vA, vB) / (vA.GetLength() * vB.GetLength());
	return angleCosine >= 0.707f;						// cos 45

}

bool Camera::IsVisible(BoundingBox *box)
{
	return this->IsVisible(Vector3(box->center.x + box->dimensions.x, box->center.y + box->dimensions.y, box->center.z + box->dimensions.z)) ||
		this->IsVisible(Vector3(box->center.x + box->dimensions.x, box->center.y + box->dimensions.y, box->center.z - box->dimensions.z)) ||
		this->IsVisible(Vector3(box->center.x + box->dimensions.x, box->center.y - box->dimensions.y, box->center.z + box->dimensions.z)) ||
		this->IsVisible(Vector3(box->center.x + box->dimensions.x, box->center.y - box->dimensions.y, box->center.z - box->dimensions.z)) ||
		this->IsVisible(Vector3(box->center.x - box->dimensions.x, box->center.y + box->dimensions.y, box->center.z + box->dimensions.z)) ||
		this->IsVisible(Vector3(box->center.x - box->dimensions.x, box->center.y + box->dimensions.y, box->center.z - box->dimensions.z)) ||
		this->IsVisible(Vector3(box->center.x - box->dimensions.x, box->center.y - box->dimensions.y, box->center.z + box->dimensions.z)) ||
		this->IsVisible(Vector3(box->center.x - box->dimensions.x, box->center.y - box->dimensions.y, box->center.z - box->dimensions.z));
}
	
bool Camera::IsBehind(VectorMath::Vector3 point)
{
	VectorMath::Vector3 vA = point - this->currentPos,
		vB = this->fwdVector;
	float angleCosine = VectorMath::Vector3::DotProduct(vA, vB) / (vA.GetLength() * vB.GetLength());
	return angleCosine <= 0.0f;							// cos 0 <=> if the angle is more than 90 degreees 
}

Vector3 *Camera::GetPositionPtr()
{
	return &this->currentPos;
}

Vector3 *Camera::GetRotationPtr()
{
	return &this->currentEulerRotation_degs;
}