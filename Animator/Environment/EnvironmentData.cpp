
#include "stdafx.h"
#include "EnvironmentData.h"

Texture2D* EnvironmentData::textureWater;
Texture2D* EnvironmentData::textureGrass;
Texture2D* EnvironmentData::textureSoil;
Texture2D* EnvironmentData::textureRock;