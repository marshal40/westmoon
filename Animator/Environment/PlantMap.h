
#ifndef TS_PLANTMAP_H
#define TS_PLANTMAP_H

#ifdef ENVIRONMENT_EXPORTS
#define ENVIRONMENT_API __declspec(dllexport)
#else
#define ENVIRONMENT_API __declspec(dllimport)
#endif

#include "../Geometry/Geometry.h"

#include "../Camera/Camera.h"
#include "BoundingBox.h"

#include "../Md2Model/Md2Model.h"
#include "../Md2Model/Md2Player.h"

using namespace VectorMath;
using namespace std;

/* This class describes a plantation (a lot of equal md2 models situated over the ground ).
 * It is used for many object's like forests or grass fields 
 * but there is no problem to be used for a single object -
 * for example a building
 */
class ENVIRONMENT_API PlantMap
{
private:
	Md2Player *model;					// Md2Model of plant
	int plantsNum;						// numeber of plants
	Vector3 *position;					// plants' positions 

	int displayList;					// display list with compiled rendering data 

	BoundingBox *bbs;					// pointer to array with the bounding bodies of the plants
	BoundingBox _tempBB;				// template for making the bounding boxes

public:
	PlantMap(char* filename, vector<BoundingBox*> *bbContainer);
	~PlantMap();
	
	void Load(char* filename, vector<BoundingBox*> *bbContainer);			// Load the plant map object from file 
	void LoadMap(char* mapFilename);										// Load the map from file
	void LoadPlantModel(char* meshFilename, char* textureFilename);

	int GetPlantsNum() { return this->plantsNum; }
	Vector3 GetPlantPosition(int index) { return this->position[index]; }
	
	void SetPlantPosition(int index, Vector3 position);
	void SetPlantHeight(int index, float height);

	void Render();							// Compile the display list
	void Draw(Camera *camera);				// Draw the display list

private:
	void ComputeBoundingBox(int index);
};

#endif