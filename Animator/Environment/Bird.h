
#ifndef TS_BIRD_H
#define TS_BIRD_H

#ifdef ENVIRONMENT_EXPORTS
#define ENVIRONMENT_API __declspec(dllexport)
#else
#define ENVIRONMENT_API __declspec(dllimport)
#endif

#include <fstream>

#include "LiveObjectInfo.h"
#include "../Geometry/Geometry.h"
#include "../Camera/Camera.h"
#include "../Md2Model/Md2Model.h"
#include "../Md2Model/Md2Player.h"

using namespace VectorMath;
using namespace std;

/*
 * Bird class - defines a bird with typical bird motions
 */
class ENVIRONMENT_API Bird
{
private:
	Md2Player *model;				// Md2Model of the bird
	float frameRate;

	Vector3 center;					// The center and
	float radius;					// the radius of the area where the bird is supposed to fly
	
	Vector3 position;				// position
	float rotationY;				// and rotation in the space

	Vector3 velocity;
	float angle;					// the current d(rotation) 
	float maxAngle;					// the maximal angle which the bird can take when flying

	bool isInside, lastIsInside;	// is inside the area in the current moment and in the last moment

	float life, armor;
	BoundingBox bb, _templateBB;
public:
	Bird(char* filename, vector<LiveObjectInfo>* liveObjectsInfos);
	void Load(char* filename, vector<LiveObjectInfo>* liveObjectsInfos);
	void LoadModel(char* meshFilename, char* textureFilename);
	
	bool IsAlive();

	void Draw(Camera *camera);
	void Update();

private:
	void MoveForward();					// Move the bird forward
	void Rotate(float angle);			// Rotate the bird 
	void UpdateBB();					// Update the BoundingBox
};

#endif