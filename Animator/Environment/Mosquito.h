
#ifndef TS_MOSQUITO_H
#define TS_MOSQUITO_H

#ifdef ENVIRONMENT_EXPORTS
#define ENVIRONMENT_API __declspec(dllexport)
#else
#define ENVIRONMENT_API __declspec(dllimport)
#endif

#include <Windows.h>
#include <MMSystem.h>
#include <fstream>
#include "ThrowError.h"

using namespace std;

/* This class descibes a mosquito.
 * Mosquito's task is only to play it sound
 * by random period of time simulating a mosquito 
 * comed to the player
 */
class ENVIRONMENT_API Mosquito
{
private:
	LPCWSTR waveFilename;				// mosquito sound
	unsigned probability;				// probability mosquito to come

public:
	Mosquito(LPCWSTR waveFilename);
	void SetProbability(unsigned probability);
	void Update();
};

#endif