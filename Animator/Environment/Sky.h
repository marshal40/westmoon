

#ifndef TS_SKY_H
#define TS_SKY_H

#ifdef ENVIRONMENT_EXPORTS
#define ENVIRONMENT_API __declspec(dllexport)
#else
#define ENVIRONMENT_API __declspec(dllimport)
#endif

#include "SkyObject.h"
#include "Sun.h"

#include "../Camera/Camera.h"

/* This object describes the sky over the environment
 */
class ENVIRONMENT_API Sky
{
private:
	Sun *sun;								// pointer to sun object

	unsigned skyObjectsNum;
	SkyObject **skyObjects;					// array with pointer to skyObjects
	int lastSkyObjIndex;
public:
	Sky();
	void SetSun(Sun *sun) { this->sun = sun; }
	Sun* GetSun() const { return this->sun; }

	void SetNumberOfSkyObjects(int number);
	void PushSkyObject(SkyObject* skyObject);

	void Update();
	void Draw(Camera *camera);
};

#endif