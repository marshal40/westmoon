// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the ENVIRONMENT_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// ENVIRONMENT_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#ifdef ENVIRONMENT_EXPORTS
#define ENVIRONMENT_API __declspec(dllexport)
#else
#define ENVIRONMENT_API __declspec(dllimport)
#endif

#include <vector>
#include <fstream>

#include "../Geometry/Geometry.h"
#include "../Camera/Camera.h"

using namespace VectorMath;
using namespace std;

#include "BoundingBox.h"
#include "Water.h"
#include "Terrain.h"
#include "Sun.h"
#include "Sky.h"
#include "PlantMap.h"
#include "Bird.h"
#include "Mosquito.h"
#include "LiveObjectInfo.h"

#include "../Md2Model/Texture.h"
#include "../Md2Model/TextureManager.h"
#include "../Md2Model/Md2Model.h"
#include "../Md2Model/Md2Player.h"
#include "../SmartArray/SmartArray.h"
#include <utility>

/* Main class for Environment DLL. 
 * It describes the whole environment.
 */
class ENVIRONMENT_API Environment
{
private:
	SmartArray<Terrain*> terrains;
	SmartArray<PlantMap*> plants;
	SmartArray<Bird*> birds;

	vector<BoundingBox*> bodies;						// vector of the boundig boxes of the static objects

	Sky sky;											// Describtion of the sky

	Mosquito *mosquito;									// a mosquito

	long long eventsMask;								// bit mask of the events 

public:
	enum Events { BIRD_IS_DEAD = 1 };					// Events

	vector<LiveObjectInfo> liveObjectsInfos;			// Info ( bounding box and pointer to object) of the live objects
	static const float UNDEFINED;   // = -12345.0f;
	Environment();
	~Environment();

	void PushBack(Terrain *terrain);					// Push new terrain

	void LoadEnvironment(char* filename);				// Load environment from file
	void LoadPlantMap(char* filename);					// Load plant map from file
	void LoadBird(char* filename);						// Load bird from file

	void SetSun(Sun *sun);
	Sun* GetSun();
	void SetMosquito(Mosquito *mosquito);

	void Update();
	void Compile();
	void Draw(Camera *camera);
	void DrawSky(Camera *camera);

	void ReserveTerrains(int numTerrains);
	float Topo(Vector3 point);							// Get the highest point of the environment at specific point
	bool isLegal(BoundingBox box);						// If the box is not colliding with any of the objects in the environment

	void Damage(Vector3 point, float damage);			// Damage the object in which the point is located
	bool CheckEvent(Events _event);						// Check if event is happened
};

#endif