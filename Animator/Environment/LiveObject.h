
#ifndef TS_LIVEOBJECT_H
#define TS_LIVEOBJECT_H

#ifdef ENVIRONMENT_EXPORTS
#define ENVIRONMENT_API __declspec(dllexport)
#else
#define ENVIRONMENT_API __declspec(dllimport)
#endif

#include "LiveObjectInfo.h"
#include "../Geometry/Geometry.h"
#include "../Environment/BoundingBox.h"
#include "../Environment/Environment.h"
#include "../Camera/Camera.h"
#include "../Md2Model/Md2Player.h"

/* Defines a life object - a object which
 * can take damage and move in the world
 */
class ENVIRONMENT_API LiveObject
{
protected:
	Vector3 position;					// Position and
	Vector3 rotation;					// rotation (ouler coordinates) in the 3D world

	Md2Player *character;				// Md2Model of the object
	float frameRate;					// frame rate of the animation

	float angularSpeed;					// Angular velocity
	float linearSpeed;					// Linear velocity

	BoundingBox boundingBox;
	BoundingBox _templateBB;			// Template which we use when the bounding box is updated
	Environment *environment;			// Pointer to the environment where the live object belongs to

	bool unlim;	
	virtual void AnimStoped() {}
public:
	float armor;						// Armor - 0 - no armor; 100 - max armor
	float life;							// Life 0 - the object is dead; 100 - the object is "full of energy"

	LiveObject(Md2Player *character, 
		Vector3 position, 
		Vector3 rotation, 
		float linearSpeed, 
		float angularSpeed,
		float armor,
		BoundingBox boundingBox,
		Environment* environment,
		float frameRate,
		float scale);

	void SetPosition(Vector3 position);
	Vector3 GetPosition() { return this->position; }
	Vector3* GetPositionPtr() { return &this->position; }

	float GetLive() { return this->life; }
	void Heal(float value);							// life += value

	BoundingBox* GetBoundingBox() { return &this->boundingBox; }

	void UpdateBoundingBox();

	void SetAnimation(char* animation);				// Change the animation of the model
	void Draw(Camera *camera, bool animated);

	// Rotation functons
	void TurnLeft(float angle);						
	void TurnRight(float angle);

	// Movements
	void MoveForward();
	void MoveLeft();
	void MoveRight();
	void MoveBack();

	void Move(Vector3 newPosition);	
};


#endif