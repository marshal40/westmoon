
#ifndef TS_SKYOBJECT_H
#define TS_SKYOBJECT_H

#ifdef ENVIRONMENT_EXPORTS
#define ENVIRONMENT_API __declspec(dllexport)
#else
#define ENVIRONMENT_API __declspec(dllimport)
#endif

#include "../Geometry/Geometry.h"
#include "../Camera/Camera.h"
#include "../Md2Model/Texture.h"
using namespace VectorMath;


/* This class describes a object in the sky. 
 * These object has only texture which must be 
 * oriented to the camera
 */
class ENVIRONMENT_API SkyObject
{
protected:
	Texture2D* texture;			
	Vector3 position;					// Position in the 3d word
	float size;							// size of the size

	Vector3 rotationV;					// rotation axis
	float angle;						// angle of rotation
					
public:
	SkyObject(Texture2D *texture, Vector3 position, float size = 50.0f);

	void SetPosition(Vector3 position);
	void SetTexture(Texture2D *texture) { this->texture = texture; }
	void SetSize(float size) { this->size = size; }
	float GetSize() { return this->size; }
	virtual Vector3 GetPosition();
	
	virtual void Draw(Camera *camera);
	virtual void Update() { }
	
protected:
	void CalculateRotation();			// calculates the rotation 
};

#endif