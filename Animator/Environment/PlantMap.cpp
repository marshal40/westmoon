
#include "stdafx.h"
#include "PlantMap.h"
#include "ThrowError.h"

PlantMap::PlantMap(char* filename, vector<BoundingBox*> *bbContainer)
{
	this->Load(filename, bbContainer);
}

PlantMap::~PlantMap() 
{
	delete model;
	delete position;
}

void PlantMap::LoadMap(char* mapFilename)					// Load the plant map from file
{
	this->bbs = NULL;

	fstream file;
	file.open(mapFilename, ios::in | ios::binary);
	if(file.is_open())
	{
		file.read((char*)&this->plantsNum, sizeof(this->plantsNum));		// read the number of plant
		this->position = new Vector3[this->plantsNum];
		for(unsigned i = 0; i < this->plantsNum; i++)						// read the plants info
		{
			file.read((char*)&this->position[i].x, sizeof(this->position[i].x));
			file.read((char*)&this->position[i].z, sizeof(this->position[i].z));
			this->position[i].y = 0.0f;
		}
	}
	else		//throw error if the file do not exist 
	{
		ThrowError(TEXT("Cannot open treeMap"));
	}
	file.close();
}

void PlantMap::Load(char* filename, vector<BoundingBox*> *bbContainer)				// Read the plant map obect info from file
{
	fstream file;
	file.open(filename, ios::in);
	if(file.is_open())
	{
		char meshFilename[256], textureFilename[256], mapFilename[256];
			
		//Load the model info
		file.getline(meshFilename, 256);
		file.getline(textureFilename, 256);
		this->LoadPlantModel(meshFilename, textureFilename);

		//Load the map info
		file.getline(mapFilename, 256);
		this->LoadMap(mapFilename);

		//Load bounding box info
		int isBB;
		file >> isBB;
		if(isBB)
		{
			this->bbs = new BoundingBox[this->plantsNum];
			file >> this->_tempBB.center.x >> this->_tempBB.center.y >> this->_tempBB.center.z;					// Read the template bonding box
			file >> this->_tempBB.dimensions.x >> this->_tempBB.dimensions.y >> this->_tempBB.dimensions.z;
			for(unsigned i = 0; i < this->plantsNum; i++)
			{
				this->ComputeBoundingBox(i);
				this->bbs[i] = this->_tempBB;																	// Initialize the bounding boxes
				this->bbs[i].center += this->position[i];
				bbContainer->push_back(&this->bbs[i]);															// Register the BBs
			}
		}
	}
	else		// throw error if the file do not exists
	{
		ThrowError(TEXT("Cannot open the file to load TreeMap!"));
	}

	file.close();
}
	
void PlantMap::SetPlantPosition(int index, Vector3 position) 
{ 
	this->position[index] = position; 
	this->ComputeBoundingBox(index);
}
void PlantMap::SetPlantHeight(int index, float height) 
{ 
	this->position[index].y = height; 
	this->ComputeBoundingBox(index);
}

void PlantMap::LoadPlantModel(char* meshFilename, char* textureFilename)			// Load the plant's MD2 model
{
	this->model = new Md2Player(meshFilename, textureFilename); 
	this->model->setScale(1.5f);

	this->displayList = glGenLists(1);
	glNewList(this->displayList, GL_COMPILE);
	this->Render();
	glEndList();
}

void PlantMap::Render()				// Render the plant maps
{
	this->model->drawPlayerItp(false, static_cast<Md2Object::Md2RenderMode>(1));
}
	
void PlantMap::Draw(Camera *camera)
{
	for(unsigned int i = 0; i < this->plantsNum; i++)
	{
		//if the plant is not visible or if it is too far from the camera, do not draw it
		if((camera->GetPosition() - this->position[i]).GetLength() < 1500.0f && camera->IsVisible(&this->bbs[i])) 
		{
			glPushMatrix();
			glTranslatef(this->position[i].x, this->position[i].y, this->position[i].z);
			glCallList(this->displayList);
			glPopMatrix();
		}
	}
}

void PlantMap::ComputeBoundingBox(int index)
{
	if(this->bbs != NULL)
	{
		this->bbs[index] = this->_tempBB;																	// Initialize the bounding boxes
		this->bbs[index].center += this->position[index];
	}
}