

#ifndef TERRAIN_H
#define TERRAIN_H

#ifdef ENVIRONMENT_EXPORTS
#define ENVIRONMENT_API __declspec(dllexport)
#else
#define ENVIRONMENT_API __declspec(dllimport)
#endif

#include <vector>
#include "Water.h"
#include "../Camera/Camera.h"
#include "../Geometry/Geometry.h"

using namespace VectorMath;
using namespace std;

/* This class describes a Terrain as a Mesh
 * Also Terrain class computes where to put water pooles (all regions below zero)
 */
class ENVIRONMENT_API Terrain
{
private:
	int mapX, mapZ;
	int** vertexHeights;				// array with the height of the points

	Vector3 position;					// position of the terrain
	int precision;						// grid precision

	Vector3 **vertices;

	struct Triangle
	{
		unsigned vertices[3][2];
		void Set(unsigned vertexA_x, unsigned vertexA_z, 
			unsigned vertexB_x, unsigned vertexB_z, 
			unsigned vertexC_x, unsigned vertexC_z)
		{
			this->vertices[0][0] = vertexA_x;
			this->vertices[0][1] = vertexA_z;
			this->vertices[1][0] = vertexB_x;
			this->vertices[1][1] = vertexB_z;
			this->vertices[2][0] = vertexC_x;
			this->vertices[2][1] = vertexC_z;
		}
	} ***triangles;

	unsigned smoothLevel;
	Vector3 ***normals, ***smoothNormals;	// normals and smooth normals of the faces

	int displayList;	
	vector<Water> waterPools;			// vector containing the water pools

	unsigned triangleCount;				// number of triangles
	unsigned vertexCount;				// number of vertices

	int _ID;
public:
	Terrain() { }
	Terrain(int _ID, char* fileName, unsigned smoothLevel, 
		int mapX, int mapY, int precision,
		Vector3 position = Vector3(0.0f, 0.0f, 0.0f), 
		Vector3 scale = Vector3(1.0f, 1.0f, 1.0f),
		bool comperess = true);
	~Terrain();

	void Draw(Camera *camera);						// Draw the display list
	void Render();									// Render the terrain
	void Compile();									// Compile display list
	float Topo(float x, float z);					// Returns the altitude of a point in the terrain				 

	int GetVertexCount() { return this->vertexCount; }
	int GetTriangleCount() { return this->triangleCount; }
	Vector3 GetPosition() { return this->position; }
	int GetPrecision() { return this->precision; }
	int GetMapX() { return this->mapX; }
	int GetMapZ() { return this->mapZ; }
	bool IsPointInside( VectorMath::Vector3 point);	// Is the point belongs to this terrain

private:
	void ManageWithFaceNormals();
	void CalculateFaceNormals();					// Calculate the normals of the faces of the terrain
	void PrintFaceNormalsToFile();
	void GetPool(int i, int j, bool** visited, float* maxX, float*maxZ, float* minX, float* minZ);		// DFS implementation
	void CalculateWaterPools();						// Calculate the water pools

};



#endif