
#ifndef TROWERROR_H
#define TROWERROR_H

#ifdef ENVIRONMENT_EXPORTS
#define ENVIRONMENT_API __declspec(dllexport)
#else
#define ENVIRONMENT_API __declspec(dllimport)
#endif

#include <Windows.h>

ENVIRONMENT_API void ThrowError(LPCWSTR errorDescribtion);

#endif