
#include "stdafx.h"
#include "Water.h"
#include "EnvironmentData.h"

void Water::Grid()						// Calculate the grids width and height
{
	float x = fabs(boxA.x - boxB.x);
	float z = fabs(boxA.z - boxB.z);

	this->gridWidth = x/(int)(x/100.0f) + 0.1f;
	this->gridHeight = z/(int)(z/100.0f) + 0.1f;
}

Water::Water(VectorMath::Vector3 boxA, VectorMath::Vector3 boxB)
{
	this->boxA = boxA;
	this->boxB = boxB;
	Grid();

	this->displayList = glGenLists(1);
	glNewList(this->displayList, GL_COMPILE);
		this->Render();
	glEndList();
}

Water::~Water()
{
	glDeleteLists(this->displayList, 1);
}

void Water::Update()
{

}

void Water::Render()			// render the water
{
	EnvironmentData::textureWater->bind();
	//glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_QUADS);
	for(float i = this->boxA.x; i < this->boxB.x; i += this->gridWidth)
	{
		for(float j = this->boxA.z; j < this->boxB.z; j += this->gridHeight)
		{
			glNormal3f(0.0f, 1.0f, 0.0f);
			glTexCoord2f(0.0f, 1.0f); glVertex3f(i, this->boxA.y, j+this->gridHeight);
			glTexCoord2f(1.0f, 1.0f); glVertex3f(i+this->gridWidth, this->boxA.y, j+this->gridHeight);
			glTexCoord2f(1.0f, 0.0f); glVertex3f(i+this->gridWidth, this->boxA.y, j);
			glTexCoord2f(0.0f, 0.0f); glVertex3f(i, this->boxA.y, j);		
		}
	}
	glEnd();
}

void Water::Draw()				// draw the display list
{
	glCallList(this->displayList);
}
