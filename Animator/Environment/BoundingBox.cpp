
#include "stdafx.h"
#include "BoundingBox.h"

BoundingBox::BoundingBox(Vector3 center, Vector3 dimensions)
{
	this->center = center;
	this->dimensions = dimensions;
}

void BoundingBox::Set(Vector3 center, Vector3 dimensions)
{
	this->center = center;
	this->dimensions = dimensions;
}

bool BoundingBox::isColliding(BoundingBox* box)						// If the boxes are colliding
{
	// if the distance between the centers is less than the sum of the dimensions => true
	return abs(this->center.x - box->center.x) < (this->dimensions.x + box->dimensions.x) &&	
			abs(this->center.y - box->center.y) < (this->dimensions.y + box->dimensions.y) &&
			abs(this->center.z - box->center.z) < (this->dimensions.z + box->dimensions.z);
}

bool BoundingBox::IsEqual(BoundingBox* box)						// If the boxes are equal
{
	return (this->center.x == box->center.x) && 
		(this->center.y == box->center.y) && 
		(this->center.z == box->center.z) &&
		(this->dimensions.x == box->dimensions.x) &&
		(this->dimensions.y == box->dimensions.y) &&
		(this->dimensions.z == box->dimensions.z);
}

void BoundingBox::Draw()
{
	glPushMatrix();
	glTranslatef(this->center.x, this->center.y, this->center.z);
	glScalef(this->dimensions.x, this->dimensions.y, this->dimensions.z);
	glBegin(GL_QUADS);
		glColor3f(0.0f,1.0f,0.0f);						// Set The Color To Green
		glTexCoord2f(0.0f, 0.0f);	glVertex3f( 1.0f, 1.0f, 1.0f);	
		glTexCoord2f(0.0f, 0.0f);	glVertex3f(-1.0f, 1.0f, 1.0f);	
		glTexCoord2f(0.0f, 0.0f);	glVertex3f(-1.0f, 1.0f,-1.0f);	
		glTexCoord2f(0.0f, 0.0f);	glVertex3f( 1.0f, 1.0f,-1.0f);	
				
		glColor3f(1.0f,0.5f,0.0f);						// Set The Color To Orange
		glTexCoord2f(0.0f, 0.0f);	glVertex3f( 1.0f,-1.0f, 1.0f);					// Bottom side
		glTexCoord2f(0.0f, 0.0f);	glVertex3f(-1.0f,-1.0f, 1.0f);	
		glTexCoord2f(0.0f, 0.0f);	glVertex3f(-1.0f,-1.0f,-1.0f);	
		glTexCoord2f(0.0f, 0.0f);	glVertex3f( 1.0f,-1.0f,-1.0f);	

		glColor3f(1.0f,0.0f,0.0f);						// Set The Color To Red
		glTexCoord2f(0.0f, 0.0f);	glVertex3f( 1.0f, 1.0f, 1.0f);					// Front side
		glTexCoord2f(0.0f, 0.0f);	glVertex3f(-1.0f, 1.0f, 1.0f);	
		glTexCoord2f(0.0f, 0.0f);	glVertex3f(-1.0f,-1.0f, 1.0f);	
		glTexCoord2f(0.0f, 0.0f);	glVertex3f( 1.0f,-1.0f, 1.0f);	

		glColor3f(1.0f,1.0f,0.0f);						// Set The Color To Yellow
		glTexCoord2f(0.0f, 0.0f);	glVertex3f( 1.0f,-1.0f,-1.0f);					// Back side
		glTexCoord2f(0.0f, 0.0f);	glVertex3f(-1.0f,-1.0f,-1.0f);		
		glTexCoord2f(0.0f, 0.0f);	glVertex3f(-1.0f, 1.0f,-1.0f);		
		glTexCoord2f(0.0f, 0.0f);	glVertex3f( 1.0f, 1.0f,-1.0f);		

		glColor3f(0.0f,0.0f,1.0f);						// Set The Color To Blue
		glTexCoord2f(0.0f, 0.0f);	glVertex3f(-1.0f, 1.0f, 1.0f);					// Left side
		glTexCoord2f(0.0f, 0.0f);	glVertex3f(-1.0f, 1.0f,-1.0f);
		glTexCoord2f(0.0f, 0.0f);	glVertex3f(-1.0f,-1.0f,-1.0f);		
		glTexCoord2f(0.0f, 0.0f);	glVertex3f(-1.0f,-1.0f, 1.0f);		

		glColor3f(1.0f,0.0f,1.0f);						// Set The Color To Violet
		glTexCoord2f(0.0f, 0.0f);	glVertex3f( 1.0f, 1.0f,-1.0f);					// Right side
		glTexCoord2f(0.0f, 0.0f);	glVertex3f( 1.0f, 1.0f, 1.0f);					
		glTexCoord2f(0.0f, 0.0f);	glVertex3f( 1.0f,-1.0f, 1.0f);		
		glTexCoord2f(0.0f, 0.0f);	glVertex3f( 1.0f,-1.0f,-1.0f);				
		glColor3f(1.0f, 1.0f, 1.0f);
	glEnd();
	glPopMatrix();
}

void BoundingBox::Translate(Vector3 translation)
{
	this->center+=translation;
}

bool BoundingBox::IsContaining(Vector3 point)						// If the point is inside the box
{
	return abs(point.x - this->center.x) <= this->dimensions.x &&
		abs(point.y - this->center.y) <= this->dimensions.y &&
		abs(point.z - this->center.z) <= this->dimensions.z;
}

bool BoundingBox::IsReal()											// If the box has positive dimensions
{
	return !((this->dimensions.x < 0.1f) && 
		(this->dimensions.y < 0.1f) && 
		(this->dimensions.z < 0.1f));
}