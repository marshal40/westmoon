
#include "stdafx.h"
#include "Mosquito.h"

Mosquito::Mosquito(LPCWSTR waveFilename)
{
	this->waveFilename = waveFilename;
	this->probability = 10001;
}
	
void Mosquito::SetProbability(unsigned probability) { this->probability = probability; }

void Mosquito::Update()
{
	if(rand()%this->probability == 0) 
	{
		if(!PlaySound(this->waveFilename, 0, 1))
			ThrowError(TEXT("Cannot play mosquito wave!"));
	}
}