
#ifndef WATER_H
#define WATER_H

#ifdef ENVIRONMENT_EXPORTS
#define ENVIRONMENT_API __declspec(dllexport)
#else
#define ENVIRONMENT_API __declspec(dllimport)
#endif

#include "../Geometry/Geometry.h"

/* This class describes a water pool
 */ 
class ENVIRONMENT_API Water
{
private:
	VectorMath::Vector3 boxA, boxB;			// corners ot the water pool
	float gridWidth, gridHeight;			// number of grids on the grid

	int displayList;
public:
	Water(VectorMath::Vector3 boxA, VectorMath::Vector3 boxB);
	~Water();
	void Update();
	void Draw();
	void Render();
private:
	void Grid();
};


#endif