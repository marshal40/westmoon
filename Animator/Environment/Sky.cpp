
#include "stdafx.h"
#include "Sky.h"

Sky::Sky() 
{ 
	this->sun = NULL; 
	this->skyObjects = NULL;
	this->skyObjectsNum = 0;
	this->lastSkyObjIndex = -1;
}

void Sky::Update()
{
	this->sun->Update();
}
	  
void Sky::Draw(Camera *camera)
{
	if(this->sun != NULL)
		this->sun->Draw(camera);

	for(int i = 0; i <= this->lastSkyObjIndex; i++)
		this->skyObjects[i]->Draw(camera);

}

void Sky::SetNumberOfSkyObjects(int number)
{
	if(this->skyObjects == NULL)
	{
		this->skyObjectsNum = number;
		this->skyObjects = new SkyObject*[this->skyObjectsNum]; 
	}
}

void Sky::PushSkyObject(SkyObject* skyObject)
{
	if(this->skyObjects != NULL && (this->lastSkyObjIndex+1 < this->skyObjectsNum))	 //check for out of bounds and uninitialized array
		this->skyObjects[++this->lastSkyObjIndex] = skyObject;
}
